const path = require('path');

module.exports = function override(config, env) {
  // Add the fallback for http
  config.resolve.fallback = {
    ...config.resolve.fallback,
    http: require.resolve('stream-http'),
    // Add other necessary fallbacks here
    https: require.resolve('https-browserify'),
    buffer: require.resolve('buffer'),
    crypto: require.resolve('crypto-browserify'),
    stream: require.resolve('stream-browserify'),
    url: require.resolve('url'),
    assert: require.resolve('assert/'),
    os: require.resolve('os-browserify/browser'),
    path: require.resolve('path-browserify'),
    fs: false, // fs cannot be polyfilled in the browser
  };

  return config;
};
