const { ethers } = require('ethers');


const {FACTORY_V3_ABI, POOL_ABI, ERC20_ABI, QUOTER2_ABI} = require("./constants.js")


const provider = new ethers.providers.JsonRpcProvider("https://mainnet.base.org");

const FACTORY_ADDRESS = "0x33128a8fC17869897dcE68Ed026d694621f6FDfD"
const QUOTER2_ADDRESS = "0x3d4e44Eb1374240CE5F1B871ab261CD16335B76a"

const WETH_ADDRESS = "0x4200000000000000000000000000000000000006"
const USDC_ADDRESS = "0x833589fCD6eDb6E08f4c7C32D4f71b54bdA02913"

function sqrtToPrice(sqrt, decimals0, decimals1, token0IsInput = true){
    const numerator = sqrt**2;
    const denominator = 2 **192;
    var ratio = numerator/ denominator;
    const shiftDecimals = Math.pow(10, decimals0 - decimals1);
    if(!token0IsInput){
        ratio = 1/ratio;
    }
    return ratio;
}

async function main(tokenIn, tokenOut, fee, amountIn){
    const factory = new ethers.Contract(FACTORY_ADDRESS, FACTORY_V3_ABI, provider);
    const quoter = new ethers.Contract( QUOTER2_ADDRESS, QUOTER2_ABI, provider);

    const poolAddress = await factory.getPool(tokenIn, tokenOut, fee);
    const poolContract = new ethers.Contract(poolAddress, POOL_ABI, provider);
    const slot0 = await poolContract.slot0()
    const sqrtPriceX96 = slot0.sqrtPriceX96


    const tokenInContract = new ethers.Contract(tokenIn, ERC20_ABI, provider);
    const tokenOutContract = new ethers.Contract(tokenOut, ERC20_ABI, provider);

    const decimalsIn = await tokenInContract.decimals()
    const decimalsOut = await tokenOutContract.decimals()

    const params = {
        tokenIn: tokenIn,
        tokenOut: tokenOut,
        fee: fee,
        amountIn: amountIn,
        sqrtPriceLimitX96: '0'
    }

    const quote = await quoter.callStatic.quoteExactInputSingle(params)
    const sqrtPriceX96After = quote.sqrtPriceX96After

    const price = sqrtToPrice(sqrtPriceX96, decimalsIn, decimalsOut)
    const priceAfter = sqrtToPrice(sqrtPriceX96After, decimalsIn, decimalsOut)

    console.log("price", price)
    console.log("priceAfter", priceAfter)

    const absoluteChange = price - priceAfter
    const percentChange = absoluteChange/price
    console.log('percent Change, ', (percentChange * 100).toFixed(2))
}

main(WETH_ADDRESS, USDC_ADDRESS, 100, ethers.utils.parseEther('10'));