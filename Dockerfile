# Sử dụng hình ảnh Node.js chính thức làm base image
FROM node:18-alpine

# Thiết lập thư mục làm việc
WORKDIR /app

# Sao chép package.json và package-lock.json (nếu có) vào container
COPY package*.json ./

# Cài đặt dependencies
RUN npm install

# Sao chép mã nguồn của ứng dụng vào container
COPY . .

# Build ứng dụng
RUN npm run build

# Sử dụng một server tĩnh để serve ứng dụng, như serve
RUN npm install -g serve

# Command để khởi động ứng dụng
CMD ["serve", "-s", "build"]

# Expose port
EXPOSE 3000
