import React from "react";
import "./DropdownMenu.css"; // Ensure this contains your dropdown styles
import { LIST_CHAIN, WEB3_CHAIN_SUPPORT } from "../../constants";
import useDropdownMenu from "hooks/useDropdownMenu";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import useWeb3 from "hooks/useWeb3";

const DropdownMenu = ({ containerRef }) => {
  const {
    isOpen,
    selectedItem,
    toggleDropdown,
    selectItem,
    dropdownRef,
    menuRef,
  } = useDropdownMenu(containerRef);

  const {
    switchChain,
    // disconnectWeb3,
  } = useWeb3();

  const handleSwitchChain = async (chain) => {
    const result = await switchChain(WEB3_CHAIN_SUPPORT[chain.value]);
    alert(result?.chainName);
    if (!result) return;
    selectItem(chain);
  };

  return (
    <div className="dropdown-container" ref={dropdownRef}>
      <div className="dropdown-button" onClick={toggleDropdown}>
        {selectedItem ? (
          <img
            src={selectedItem.logo}
            alt={selectedItem.name}
            className="dropdown-logo"
          />
        ) : (
          <img
            src="path-to-default-icon.png"
            alt="Dropdown Icon"
            className="dropdown-logo"
          />
        )}
        {/* <span className={`dropdown-arrow ${isOpen ? "open" : ""}`}>
          &#9660;
        </span> */}
      </div>
      {isOpen && (
        <ul className="dropdown-menu-list"  ref={menuRef}>
          {Object.values(LIST_CHAIN).map((chain) => (
            <li
              key={chain.value}
              className="dropdown-item"
              onClick={() => handleSwitchChain(chain)}
            >
              <div>
                <img
                  src={chain.logo}
                  alt={chain.name}
                  className="dropdown-item-logo"
                />
                <span className="dropdown-item-name">{chain.name}</span>
              </div>
              {selectedItem?.value === chain.value && (
                <FontAwesomeIcon
                  icon={faCheck}
                  className="dropdown-item-check"
                />
              )}
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default DropdownMenu;
