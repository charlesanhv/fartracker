import { useEffect, useRef, useState } from "react";
import { createPortal } from "react-dom";
import { TYPE_NOTI } from "constants";
import useShowNoti from "hooks/useShowNoti";
import useOpen from "hooks/useOpen";
import {
  SearchIcon,
  WalletIcon,
  BackIcon,
  IconResultSearch,
  FireIcon,
  UnionIcon,
  ClearIcon,
  InputSearchIcon,
  SearchIconMb,
} from "assets/icons";
import DropdownMenu from "./DropdownMenu/DropdownMenu";
import useScreenType from "hooks/useScreenType";
import { NotiContainer } from "./Home";
import { useAccount } from "wagmi";
import { ConnectKitButton } from "connectkit";
import { Link, useLocation, useNavigate } from "react-router-dom";
import SearchContent from "./SearchContent";
import { DEFAULT_RESULT_HEADER } from "constants";
import { searchContract } from "lib/rarible";
import { useDebounce } from "hooks/useDebounce";
import { useDispatch } from "react-redux";
import { actionSetFilterByUser, setFilterType } from "../redux/homeReducer";

function Header({ result = DEFAULT_RESULT_HEADER }) {
  const location = useLocation();
  const navigate = useNavigate();

  const pathSegments = location.pathname
    .split("/")
    .filter((segment) => segment);
  const pathValue = pathSegments[pathSegments.length - 1];

  const screenWidth = window.innerWidth;

  const [inputValue, setInputValue] = useState("");
  const containerRef = useRef(null);
  const account = useAccount();

  const {
    isShowing: showSearchMb,
    open: openSearchMb,
    close: closeSearchMb,
  } = useOpen();

  const { isShowing, openNoti } = useShowNoti(false);

  const { isTablet, isDesktopOrLaptop } = useScreenType();

  const dispatch = useDispatch();

  // const handleKeyDown = (event) => {
  //   if (event.key === "Enter" && inputValue.trim() === "") {
  //     openNoti();
  //   }
  // };

  const [showPopupSearch, setShowPopupSearch] = useState(false);

  const [resultSearch, setResultSearch] = useState(result);

  const inputDebounce = useDebounce(inputValue, 500);

  const handleOnchange = (e) => {
    setInputValue(e.target.value);
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      // dispatch(actionSetFilterByUser({username: inputValue}));
      const currentPath = window.location.pathname;
      if (!currentPath.includes('mint')) {
        navigate(`/${inputValue}`);
      } else {
        navigate(`/mint/${inputValue}`);
        // dispatch(actionSetFilterByUser({ username: inputValue }));
      }
      // dispatch(setFilterType("user"));
      closeSearchMb();
    }
  };

  useEffect(() => {
    const handle = async () => {
      if (inputDebounce) {
        if (pathValue === "mint") {
          const dataSearch = await searchContract(inputDebounce);
          console.log("dataSearch:::", dataSearch);
          setResultSearch(dataSearch);
        }
      }
    };
    handle();
  }, [inputDebounce]);

  useEffect(() => {
    if (screenWidth > 768) {
      closeSearchMb();
    }
  }, [screenWidth]);

  return (
    <>
      {showSearchMb ? (
        <div className="showSearchHeader">
          <div className="containerInputSearchMB">
            <div className="boxInputSearchMB">
              <div
                onClick={() => {
                  closeSearchMb();
                }}
              >
                <BackIcon />
              </div>
              <div className="inputSearchHeaderMB">
                <div style={{ display: "flex", height: "20px", width: "20px" }}>
                  <SearchIcon />
                </div>
                <input
                  className="inputSearchMb"
                  placeholder="Search token, trader, transaction,..."
                  type="text"
                  value={inputValue}
                  onChange={handleOnchange}
                  onKeyDown={handleKeyDown}
                />
                {inputValue === "" ? (
                  <></>
                ) : (
                  <div
                    onClick={() => {
                      setInputValue("");
                    }}
                    style={{ cursor: "pointer" }}
                  >
                    <ClearIcon />
                  </div>
                )}
              </div>
            </div>
          </div>
          <div className="containerListResult">
            {resultSearch.map((item, index) => (
              <div key={index}>
                <div className="itemListResult">
                  <div className="IconResultSearch">
                    <IconResultSearch />
                  </div>
                  <div className="UnionIcon">
                    <UnionIcon />
                  </div>
                  <div className="contentItemList">
                    <img src="assets/icons/Ellipse 629.svg" alt="" />
                    <p className="textItemList">{item.name}</p>
                  </div>
                  <FireIcon />
                </div>
              </div>
            ))}
          </div>
        </div>
      ) : (
        <div className="container containerHeader">
          <Link to="/">
            <img
              alt="logo"
              className="logo"
              src="/assets/images/logo.png"
              style={{
                cursor: "pointer",
              }}
            />
          </Link>
          {pathValue === "mint" && (
            <div
              className="containerInputSearch"
              style={{ position: "relative" }}
              onClick={() => setShowPopupSearch(true)}
            >
              <div className="iconSearch">
                <SearchIcon className="iconSearch" />
              </div>

              <div className="boxInputSearch">
                <input
                  className="inputSearch"
                  placeholder={
                    pathValue === "mint"
                      ? "Search contract..."
                      : "Search token, trader, transaction,..."
                  }
                  type="text"
                />
                {inputValue === "" ? (
                  <div
                    style={{
                      backgroundColor: "rgba(255, 255, 255, 0.06)",
                      borderRadius: "4px",
                      padding: "0.5px 9px",
                    }}
                  >
                    <InputSearchIcon />
                  </div>
                ) : (
                  <div
                    onClick={() => {
                      setInputValue("");
                    }}
                    style={{ cursor: "pointer" }}
                  >
                    <ClearIcon />
                  </div>
                )}
              </div>
            </div>
          )}
          <div className="containerConnectWallet" ref={containerRef}>
            <div
              onClick={() => {
                openSearchMb();
              }}
            >
              <SearchIconMb />
            </div>
            <DropdownMenu containerRef={containerRef} />
            {/* <ConnectKitButton /> */}
            <ConnectKitButton.Custom>
              {({ isConnected, show, truncatedAddress, ensName }) => (
                <div className="boxConnectWallet" onClick={show}>
                  <WalletIcon />
                  {(isTablet || isDesktopOrLaptop) && (
                    <p
                      className="textConnectWallet"
                      style={{
                        fontFamily: "'Archivo'",
                      }}
                    >
                      {isConnected
                        ? account?.address.slice(0, 6) +
                          "..." +
                          account?.address.slice(-4)
                        : "Connect wallet"}
                    </p>
                  )}
                </div>
              )}
            </ConnectKitButton.Custom>
          </div>
          {createPortal(
            <div className={`${isShowing ? "show-noti" : "hide-noti"}`}>
              <NotiContainer
                show_noti={isShowing}
                type={TYPE_NOTI.WARNING}
                message="Please enter number amount"
              />
            </div>,
            document.body
          )}
          {showPopupSearch &&
            createPortal(
              <div className="container-popup-search">
                <SearchContent
                  onClose={() => setShowPopupSearch(false)}
                  openNoti={openNoti}
                  placeholder={"Search contract..."}
                />
              </div>,
              document.body
            )}
        </div>
      )}
    </>
  );
}

export default Header;
