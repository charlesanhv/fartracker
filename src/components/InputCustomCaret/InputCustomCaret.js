/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import "./InputCustomCaret.css";

const InputCustomCaret = ({ value=0, id, readOnly = false, showSwap }) => {
  const [text, setText] = useState(value);

  useEffect(() => {
    const input = document.getElementById(id);
    if (!input) return;

    const handleInput = () => {
      if (isNaN(input.value)) return;
      setText(input.value);
    };

    input.addEventListener("input", handleInput);

    return () => {
      input.removeEventListener("input", handleInput);
    };
  }, [id]);

  useEffect(() => {
    setText(value);
  }, [value]);

  useEffect(() => {
    setText(value);
  }, [showSwap]);

  return (
    <>
      <p id={`${id}-shell`} className="text-custom-caret">
        {text}
      </p>
      <input
        id={id}
        type="text"
        className="input-custom-caret"
        value={text}
        readOnly={readOnly}
        onChange={(event) => {}}
        maxLength={7}
        autoComplete="false"
      />
    </>
  );
};

export default InputCustomCaret;
