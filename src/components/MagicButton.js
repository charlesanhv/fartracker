import React from "react";

const MagicButton = ({
  title,
  icon,
  position,
  handleClick,
  otherClasses,
}) => {
  const buttonStyle = {
    position: 'relative',
    display: 'inline-flex',
    height: '3rem',
    width: '100%',
    marginTop: '2.5rem', // md:mt-10
    overflow: 'hidden',
    borderRadius: '0.5rem', // rounded-lg
    padding: '1px',
    outline: 'none',
  };

  const spinnerStyle = {
    position: 'absolute',
    inset: '-1000%',
    animation: 'spin 2s linear infinite',
    background: 'conic-gradient(from 90deg at 50% 50%, #E2CBFF 0%, #393BB2 50%, #E2CBFF 100%)',
  };

  const contentStyle = {
    display: 'inline-flex',
    height: '100%',
    width: '100%',
    cursor: 'pointer',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: '0.5rem', // rounded-lg
    backgroundColor: '#1e293b', // bg-slate-950
    paddingLeft: '1.75rem', // px-7
    paddingRight: '1.75rem', // px-7
    fontSize: '0.875rem', // text-sm
    fontWeight: 500, // font-medium
    color: '#ffffff', // text-white
    backdropFilter: 'blur(12px)', // backdrop-blur-3xl
    gap: '0.5rem', // gap-2
  };

  return (
    <>
      <button style={buttonStyle} onClick={handleClick}>
        <span style={spinnerStyle} />
        <span style={contentStyle}>
          {position === "left" && icon}
          {title}
          {position === "right" && icon}
        </span>
      </button>
      <style>
        {`
          @keyframes spin {
            to {
              transform: rotate(360deg);
            }
          }
        `}
      </style>
    </>
  );
};

export default MagicButton;
