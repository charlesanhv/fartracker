import useDelay from "hooks/useDelay";
import React, { Suspense } from "react";

const LazyLoadWithDelay = ({
  children,
  delay = 0,
  loading = false,
  fallback,
}) => {
  const isDelayed = useDelay(delay);

  if (isDelayed || loading) {
    return fallback;
  }

  return (
    <div className="wrapper-layout">
      <Suspense fallback={fallback}>{children}</Suspense>
    </div>
  );
};

export default LazyLoadWithDelay;
