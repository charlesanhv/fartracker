import React, { useEffect, useState } from "react";
import "./CustomInput.css"; // Ensure this contains your styles
import LoadingIcon from "assets/icons/LoadingIcon";

const CustomInput = ({
  prefix,
  suffix,
  value,
  onChange,
  disabled = false,
  className = "CustomInput",
  loading = false,
  type = "text",
  onClick = function () { },
  input = "true",
  readOnly = "false",
  ...props
}) => {
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (value && value.length > 0) {
      setIsLoading(true);
      const timer = setTimeout(() => {
        setIsLoading(false);
      }, 500); // Simulate loading for 500ms

      return () => clearTimeout(timer);
    } else {
      setIsLoading(false);
    }
  }, [value]);

  const displayValue =
    value && value.trim() !== "" ? `${Number(value)}` : value;
  return (
    <div
      className="custom-input-container"
      onClick={() => onClick && onClick()}
    >
      {prefix && <div className="custom-input-prefix">{prefix}</div>}
      {input ? (
        <input
          type={type}
          value={displayValue}
          // disabled={disabled}
          // onChange={onChange}
          readOnly={readOnly}
          className="custom-input"
          onClick={() => onClick && onClick()}
          {...props}
        />
      ) : (
        <div className="custom-input" {...props}>
          {displayValue}
        </div>
      )}
      <div className="custom-input-suffix-container">
        {isLoading ? (
          <div className="loading-quick-action-2">
            <LoadingIcon />
          </div>
        ) : (
          suffix && <div className="custom-input-suffix">{suffix}</div>
        )}
        {((displayValue && displayValue.trim() !== "" && !isLoading) ||
          displayValue === 0) && <span className="custom-input-dollar">$</span>}
      </div>
    </div>
  );
};

export default CustomInput;
