import React, { forwardRef, useImperativeHandle, useState } from "react";
import PropTypes from "prop-types";
import "./Modal.css";

const Modal = forwardRef(
  (
    {
      title,
      children,
      onClose,
      footer,
      className = "",
      id,
      ariaLabel,
      header,
      ...props
    },
    ref
  ) => {
    const [isOpen, setIsOpen] = useState(false);

    useImperativeHandle(ref, () => ({
      open: () => setIsOpen(true),
      close: () => setIsOpen(false)
    }));


    const handleOverlayClick = (e) => {
      if (e.target === e.currentTarget) {
        setIsOpen(false);
        onClose();
      }
    };
  
    const handleCloseClick = () => {
      setIsOpen(false);
      onClose();
    };

    return (
      isOpen && (
        <div className="modal-overlay" onClick={handleOverlayClick} {...props}>
          <div className={`modal ${className}`} id={id} aria-label={ariaLabel}>
            {header && (
              <div className="modal-header">
                <h2>{title}</h2>
                <button onClick={handleCloseClick} className="modal-close-button">
                  &times;
                </button>
              </div>
            )}
            <div className="modal-body">{children}</div>
            {footer && <div className="modal-footer">{footer}</div>}
          </div>
        </div>
      )
    );
  }
);

Modal.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node.isRequired,
  onClose: PropTypes.func.isRequired,
  footer: PropTypes.node,
  className: PropTypes.string,
  id: PropTypes.string,
  ariaLabel: PropTypes.string,
};

export default Modal;

// import React, { useRef } from 'react';
// import './App.css';
// import Modal from './components/Modal/Modal';

// function App() {
//   const modalRef = useRef();

//   const openModal = () => {
//     modalRef.current.open();
//   };

//   const closeModal = () => {
//     modalRef.current.close();
//   };

//   return (
//     <div className="App">
//       <header className="App-header">
//         <h1>Reusable Modal Component</h1>
//         <button onClick={openModal}>Open Modal</button>
//         <Modal
//           ref={modalRef}
//           title="Example Modal"
//           onClose={closeModal}
//           footer={<button onClick={closeModal}>Close</button>}
//         >
//           <p>This is a reusable modal component in React.</p>
//         </Modal>
//       </header>
//     </div>
//   );
// }

// export default App;
