import React, { useEffect, useState } from "react";
import { SEARCH_ITEMS } from "constants/trending_action_constants";
import { useDispatch } from "react-redux";
import { ClearIcon, UnionIcon } from "assets/icons";
import {
  actionSetFilterByUser,
  actionSetModalFilter,
  setFilterType,
} from "../../../redux/homeReducer";
import { useNavigate } from "react-router-dom";
const SearchItem = ({ avatar, name, icon }) => (
  <div className="d-flex align-items-center searchbox_items">
    <img
      className="iconSearchModal"
      src="/assets/icons/Icon-search.svg"
      alt=""
    />

    <div className="addIconModal">
      {" "}
      <UnionIcon />
    </div>
    <img src={`/assets/icons/${avatar}`} alt="" width="24px" height="24px" />
    <span className="searchbox_text">{name}</span>
    {icon && (
      <img src={`/assets/icons/${icon}`} alt="img" width="24px" height="24px" />
    )}
  </div>
);

const ModalFilter = () => {
  const navigate = useNavigate();

  const modal_filter_type = localStorage.getItem("modal_filter_type");
  const dispatch = useDispatch();
  const [searchValue, setSearchValue] = useState("");
  const handleClickOutSideModal = (e) => {
    const modalElement = document.getElementById("modal_search");
    if (modalElement && !modalElement.contains(e.target)) {
      dispatch(actionSetModalFilter(false));
    }
  };

  const handleKeyDown = (event) => {
    if (event.key === "Enter") {


      // dispatch(setFilterType("user"));
      dispatch(actionSetModalFilter(false));
      const currentPath = window.location.pathname;
       
      if (!currentPath.includes('mint')) {
        navigate(`/${searchValue}`);
        
       } else {
        navigate(`/mint/${searchValue}`);
      }
      // dispatch(actionSetFilterByUser({username: searchValue}));
    }
  };

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutSideModal);
    return () => {
      document.removeEventListener("mousedown", handleClickOutSideModal);
    };
  }, []);
  return (
    <>
      <div className="show-overlay_modal"></div>
      <div id="modal_search" className="modalSearch">
        <div className="d-flex flex-column modal_content_container">
          <div
            className="containerInputSearch containerInputSearchModal"
            style={{ width: "100%", marginBottom: "32px" }}
          >
            <img
              className="iconSearch"
              src="/assets/icons/Icon-search.svg"
              alt=""
            />
            <div className="boxInputSearch">
              <input
                className="inputSearch input_modal_search"
                placeholder={`Search ${
                  modal_filter_type ? modal_filter_type.toLowerCase() : ""
                }s`}
                type="text"
                onChange={(e) => {
                  setSearchValue(e.target.value);
                }}
                onKeyDown={handleKeyDown}
                value={searchValue}
              />
              {!searchValue ? (
                <img
                  style={{
                    padding: "8px 10px",
                    backgroundColor: "rgba(255, 255, 255, 0.06)",
                    borderRadius: "4px",
                  }}
                  src="/assets/icons/Icon-input-search.svg"
                  alt=""
                />
              ) : (
                <div
                  onClick={() => setSearchValue("")}
                  style={{ cursor: "pointer" }}
                >
                  <ClearIcon />{" "}
                </div>
              )}
            </div>
          </div>
          <div
            className="d-flex flex-wrap justify-content-center search_box"
            style={{
              gap: "8px",
              padding: "12px",
              maxHeight: "300px",
              overflowY: "auto",
            }}
          >
            {SEARCH_ITEMS.map((item, index) => (
              <SearchItem key={index} {...item} />
            ))}
          </div>
        </div>
      </div>
    </>
  );
};

export default ModalFilter;
