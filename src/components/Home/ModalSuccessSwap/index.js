import React, { useRef } from 'react';
import Modal from 'components/Modal/Modal';
import './ModalSuccessSwap.css';

export const ModalSuccessSwap = () => {
    const modalRef = useRef();

    // const openModal = () => {
    //     modalRef.current.open();
    // };

    const closeModal = () => {
        modalRef.current.close();
    };

    return (
        <div className="App">
            <header className="App-header">
                <Modal
                    ref={modalRef}
                    onClose={closeModal}
                >
                    <div className='header d-flex align-items-center' style={{ justifyContent: 'flex-end', gap: '12px' }}>
                        <span style={{ cursor: 'pointer', marginTop: '-2px' }} onClick={closeModal}>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#F5F5F5" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" class="lucide lucide-x"><path d="M18 6 6 18" /><path d="m6 6 12 12" /></svg>
                        </span>
                    </div>
                    <div className='content-container'>
                        <span className='icon_success'>
                            <svg xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 24 24" fill="none" stroke="#ffffff" strokeWidth="3" strokeLinecap="round" strokeLinejoin="round" class="lucide lucide-check"><path d="M20 6 9 17l-5-5" /></svg>
                        </span>
                        <div className='content-container-desc'>
                            <span className='content-container-desc-title'>Swap Success!</span>
                            <div className='d-flex align-items-center' style={{ gap: '12px' }}>
                                <div className='item'>
                                    <img src='/assets/images/swap_supported_token_eth.svg' alt="" width="16px" height="16px" />
                                    <span>100 BRETT</span>
                                </div>
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24" fill="none" stroke="#F5F5F5" stroke-width="2.75" stroke-linecap="round" stroke-linejoin="round" class="lucide lucide-arrow-right"><path d="M5 12h14" /><path d="m12 5 7 7-7 7" /></svg>
                                <div className='item'>
                                    <img src='/assets/images/swap_supported_token_eth.svg' alt="" width="16px" height="16px" />
                                    <span>100 ETH</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span class='view-explore'>View on explore</span>
                </Modal>
            </header>
        </div>
    );
}