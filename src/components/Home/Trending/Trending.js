import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { actionSetTrending } from "../../../redux/homeReducer";
import LazyLoadWithDelay from "components/LazyLoadWithDelay";
import { LoadingTransaction } from "../Transactions";

const RenderTrending = ({ TRENDING, current_trending, onChooseTrending }) => {
  const handleChooseTrending = (trending) => {
    onChooseTrending && onChooseTrending(trending);
    window.open(
      `https://dexscreener.com/base/${String(trending?.address).toLowerCase()}`,
      "_blank"
    );
  };
  return TRENDING.map((el, key) => {
    const item = el;
    const isActive = current_trending?.id === item?.coin_id;
    return (
      <div
        key={item?.coin_id}
        className={`d-flex align-items-center fadeIn  ${
          isActive ? "trending_content_item_active" : "trending_content_item"
        } `}
        onClick={() => handleChooseTrending(item)}
      >
        <span
          className="trending_action_text"
          style={{
            color: "#ffffff99",
            fontSize: "18px",
            lineHeight: "25.2px",
          }}
        >
          #{key + 1}
        </span>
        <div
          className="d-flex align-items-center"
          style={{
            gap: "8px",
          }}
        >
          <img
            height="24px"
            src={item?.image}
            width="24px"
            style={{ borderRadius: 24 / 2 }}
            alt="img"
          />
          <span
            className="trending_action_text"
            style={{
              fontSize: "18px",
              lineHeight: "25.2px",
            }}
          >
            {item?.name}
          </span>
        </div>
        {/* {item?.score < 8 ? (
          <span className="Fomo_tag">
            <p className="Fomo_tag_text mb-0">Fomo</p>
          </span>
        ) : null} */}
      </div>
    );
  });
};

export const Trending = () => {
  const current_trending = useSelector((state) => state.home.trending);

  const dispatch = useDispatch();

  const [coins, setCoins] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  // useEffect(() => {
  //   const fetchTrendingCoins = async () => {
  //     try {
  //       const response = await fetch(
  //         "https://api.coingecko.com/api/v3/search/trending"
  //       );
  //       const data = await response.json();
  //       setCoins(data?.coins || []);
  //     } catch (error) {
  //       console.error("Error fetching trending coins", error);
  //     }
  //   };

  //   fetchTrendingCoins();
  // }, []);

  useEffect(() => {
    const fetchTrendingCoins = async () => {
      setIsLoading(true);
      try {
        const response = await fetch(
          "https://api.coingecko.com/api/v3/exchanges/uniswap-v2-base/tickers"
        );
        const data = await response.json();
        const trendingCoins = data.tickers.slice(0, 10);
        const infoRes = await fetch(
          `https://api.coingecko.com/api/v3/coins/markets?${new URLSearchParams(
            {
              vs_currency: "usd",
              ids: trendingCoins.map((coin) => coin["coin_id"]),
            }
          )}`
        );
        const infoData = await infoRes.json();
        const convertInfoData = infoData.reduce((result, currentCoin) => {
          const { id, image, name, symbol } = currentCoin;
          result[id] = {
            id,
            image,
            name,
            symbol,
          };
          return result;
        }, {});
        const resultTrendingCoins = trendingCoins.map((itemCoins) => ({
          coin_id: itemCoins.coin_id,
          address: itemCoins.base,
          ...convertInfoData[itemCoins.coin_id],
        }));

        setCoins(resultTrendingCoins);
      } catch (error) {
        console.error("Error fetching trending coins", error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchTrendingCoins();
  }, []);

  const handleChooseTrending = (trending) => {
    dispatch(
      actionSetTrending({
        id: trending?.coin_id,
        name: trending?.name,
        icon: trending?.image,
      })
    );
  };

  return (
    <div className="d-flex w-100 justify-content-center align-items-center">
      <div className="container d-flex flex-column trending_container">
        <span className="d-flex align-items-center trending_header">
          <img
            alt="img"
            height="24px"
            src="/assets/icons/fire_icon.svg"
            width="24px"
          />
          <p className="trending_action_text trending_header_text mb-0">
            Tokens being traded by Warpcast Users
          </p>
        </span>
        <LazyLoadWithDelay
          loading={isLoading || coins.length === 0}
          fallback={<LoadingTransaction height="auto" />}
        >
          <div className="marquee">
            <div className="track">
              <div className="d-flex align-items-center trending_content">
                <RenderTrending
                  TRENDING={coins}
                  current_trending={current_trending}
                  onChooseTrending={(trending) =>
                    handleChooseTrending(trending)
                  }
                />
              </div>
              <div className="d-flex align-items-center trending_content">
                <RenderTrending
                  TRENDING={coins}
                  current_trending={current_trending}
                  onChooseTrending={(trending) =>
                    handleChooseTrending(trending)
                  }
                />
              </div>
            </div>
          </div>
          {/* <Marquee
            direction="left"
            speed={50}
            // pauseOnHover
          >
            <div className="d-flex align-items-center trending_content">
              <RenderTrending
                TRENDING={coins}
                current_trending={current_trending}
                onChooseTrending={(trending) =>
                  handleChooseTrending(trending)
                }
              />
            </div>
          </Marquee> */}
        </LazyLoadWithDelay>
      </div>
    </div>
  );
};
