import { useDispatch, useSelector } from "react-redux";
import { ACTIVATE_MODAL_FILTER_TYPE } from "constants/trending_action_constants";
import { useRef, useState } from "react";
import {
  actionSetBuySellValue,
  actionSetModalFilter,
  selectType,
  setFilterType,
} from "../../../redux/homeReducer";
import { FILTER_ACTION } from "constants";
import { filterByDataType } from "lib/util";
import { useLocation, useNavigate } from "react-router-dom";
import KolFilter from "components/KolFilter/KolFilter";
import useClickOutSide from "hooks/useClickOutSite";

const FilterItem = ({
  type,
  currentType,
  handleChooseType,
  icon,
  position,
  label,
  disabled
}) =>
  position === "left" ? (
    <>
      <div
        className={`action_item left_action_item d-none d-xl-flex ${
          type === currentType && "action_item_active"
        }`}
        onClick={() => handleChooseType && handleChooseType(type)}
      >
        {icon && <div className="action_item_icon">{icon}</div>}
        <span
          className="trending_action_text action_item_text"
          // style={{
          //   zIndex: "100",
          // }}
        >
          {label}
        </span>
      </div>
    </>
  ) : (
    <>
      <div
        className={`action_item right_action_item ${disabled ? "action_item_disabled" : ""} ${
          type === currentType && "action_item_active"
        }`}
        onClick={() => handleChooseType && !disabled ? handleChooseType(type) : null}
      >
        {icon && <div className="action_item_icon">{icon}</div>}
        <span
          className="trending_action_text action_item_text"
          // style={{
          //   color: "#ffffff",
          //   zIndex: "100",
          // }}
        >
          {label}
        </span>
      </div>
    </>
  );

export const FilterConfirmation = ({ dataType = "Default" }) => {
  const [isOpenActionBox, setIsOpenActionBox] = useState(false);
  const type = useSelector(selectType);  
  const buy_sell_value = useSelector((state) => state.home.buy_sell_value);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();

  const actionBoxRef = useRef(null);
  const openActionBoxRef = useRef(null);

  const handleSelectFilter = (name) => {
    if (name === FILTER_ACTION.TYPE.MINT) {
      const url = "/mint";
      window.open(url, "_blank");
    } else {
      isOpenActionBox && setIsOpenActionBox(false);
      ACTIVATE_MODAL_FILTER_TYPE.includes(name)
        ? dispatch(actionSetModalFilter(true)) &&
          localStorage.setItem("modal_filter_type", name)
        : dispatch(setFilterType(name)) && navigate(`${location.pathname.includes('mint') ? '/mint' : '/'}`);
    }
  };

  const handleSetBuySellValue = (value) => {
    if (isNaN(value)) return;
    dispatch(
      actionSetBuySellValue({
        ...buy_sell_value,
        amount: value,
      })
    );
  };
  const filteredTypes = filterByDataType(dataType);

  useClickOutSide(actionBoxRef,openActionBoxRef ,() => setIsOpenActionBox(false));

  return (
    <div className="container d-flex align-items-center">
      <div className="d-flex align-items-center justify-content-between w-100 action_container position-relative z1">
        <div className="d-flex flex-column-reverse flex-sm-row align-items-center ">
          <div className="action_table_left_container d-flex align-items-center gap-2">
            <div
              className={`action_item left_action_item d-none d-xl-flex ${
                type === FILTER_ACTION.TYPE.NORMAL && "action_item_active"
              }`}
              onClick={() => handleSelectFilter(FILTER_ACTION.TYPE.NORMAL)}
            >
              <span className="trending_action_text action_item_text">
                Normal
              </span>
            </div>
            <div
              className={`action_item 
                 action_item_active
               d-flex d-xl-none justify-content-between`}
              id="open_action"
              onClick={() => setIsOpenActionBox(!isOpenActionBox)}
              ref={openActionBoxRef}
            >
              {type === FILTER_ACTION.TYPE.NORMAL ? (
                <span className="trending_action_text action_item_text">
                  Normal
                </span>
              ) : (
                <>
                  {/* <img
          height="18px"
          src={`assets/icons/${filterTypes.find((filter_type) => filter_type?.type === type)?.icon}`}
      
          width="18px"
          alt={type}
        /> */}
                  <span
                    className="trending_action_text action_item_text"
                    // style={{
                    //   zIndex: "100",
                    // }}
                  >
                    {
                      filteredTypes.find(
                        (filter_type) => filter_type?.type === type
                      )?.label
                    }
                  </span>
                </>
              )}
              <img
                className="d-block d-xl-none"
                height="18px"
                src="/assets/icons/CaretDown.svg"
                width="18px"
                alt="img"
                style={isOpenActionBox ? { transform: "rotate(180deg)" } : {}}
              />
            </div>
            {filteredTypes
              .filter((filter_type) => filter_type?.position === "left")
              .map((filterType, index) => {
                return (
                  <FilterItem
                    key={index}
                    type={filterType.type}
                    currentType={type}
                    handleChooseType={(name) => handleSelectFilter(name)}
                    icon={filterType.icon}
                    label={filterType.label}
                    position={filterType.position}
                  />
                );
              })}
          </div>
          <div className="d-none d-xl-flex action_divider" />
          <div
            className="d-none d-xl-flex align-content-center"
            style={{
              gap: "12px",
            }}
          >
            {filteredTypes
              .filter((filter_type) => filter_type?.position === "right")
              .map((filterType, index) => {
                return (
                  <FilterItem
                    key={index}
                    type={filterType.type}
                    currentType={type}
                    handleChooseType={(name) => handleSelectFilter(name)}
                    icon={filterType.icon}
                    label={filterType.label}
                    position={filterType.position}
                    disabled={filterType.disabled}
                  />
                );
              })}
          </div>
          <KolFilter />
        </div>
        {dataType !== "Mint" && (
          <div className="buy_sell_box_container">
            <div
              className="d-flex align-items-center"
              style={{
                gap: "8px",
                width: "fit-content",
              }}
            >
              <img
                height="18px"
                src="/assets/icons/Lightning.svg"
                width="18px"
                alt="img"
              />
              <div
                className="d-flex align-items-center"
                style={{
                  gap: "12px",
                }}
              >
                <span
                  className={`trending_action_text  ${
                    buy_sell_value.type === "Buy"
                      ? "buy_sell_text_active"
                      : "buy_sell_text"
                  }`}
                  id="buy_action"
                  onClick={() => {
                    dispatch(
                      actionSetBuySellValue({ ...buy_sell_value, type: "Buy" })
                    );
                  }}
                  style={{
                    cursor: "pointer",
                    fontSize: "14px",
                    fontWeight: "600",
                    lineHeight: "19.6px",
                  }}
                >
                  BUY
                </span>
                <span
                  className={`trending_action_text  ${
                    buy_sell_value.type === "Sell"
                      ? "buy_sell_text_active"
                      : "buy_sell_text"
                  }`}
                  id="sell_action"
                  onClick={() => {
                    dispatch(
                      actionSetBuySellValue({ ...buy_sell_value, type: "Sell" })
                    );
                  }}
                  style={{
                    cursor: "pointer",
                    fontSize: "14px",
                    fontWeight: "600",
                    lineHeight: "19.6px",
                  }}
                >
                  SELL
                </span>
              </div>
            </div>
            <div className="buy_sell_box">
              <input
                className="buy_sell_input"
                // defaultValue="0"
                maxLength="10"
                type="text"
                onChange={(e) => {
                  handleSetBuySellValue(e.target.value);
                }}
                value={buy_sell_value.amount}
              />
              <span
                className="trending_action_text"
                style={{
                  color: "#a3a5ae",
                  fontSize: "14px",
                  fontWeight: "400",
                  lineHeight: "18px",
                  marginLeft: "12px",
                }}
              >
                $
              </span>
            </div>
          </div>
        )}
        {isOpenActionBox ? (
          <div className="position-absolute  action_box_mobile" ref={actionBoxRef}>
            {type !== FILTER_ACTION.TYPE.NORMAL ? (
              <div
                className="action_item d-flex w-100 align-items-center action_box_mobile_item"
                onClick={() => handleSelectFilter(FILTER_ACTION.TYPE.NORMAL)}
              >
                <span className="trending_action_text action_item_text">
                  Normal
                </span>
              </div>
            ) : null}

            {filteredTypes
              .filter((filter_type) => filter_type.type !== type)
              .map((filterType, index) => {
                return (
                  <div
                    className="action_item d-flex w-100 align-items-center action_box_mobile_item"
                    onClick={() => handleSelectFilter(filterType.type)}
                    key={index}
                  >
                    {filterType?.iconSvg ? (
                      <img
                        height="18px"
                        src={`/assets/icons/${filterType.iconSvg}`}
                        width="18px"
                        alt="img"
                      />
                    ) : (
                      filterType.icon && (
                        <div className="action_item_icon">
                          {filterType.icon}
                        </div>
                      )
                    )}

                    <span className="trending_action_text action_item_text">
                      {filterType.label}
                    </span>
                  </div>
                );
              })}
          </div>
        ) : null}
      </div>
    </div>
  );
};
