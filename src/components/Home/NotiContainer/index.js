import { TYPE_NOTI, NOTI_MESSAGE_DEFAULT } from 'constants';
import { useEffect, useRef, useState } from 'react';

const icon_noti = {
  success: '/assets/icons/CheckSquare.svg',
  error: '/assets/icons/error_noti.svg',
  warning: '/assets/icons/Warning.svg',
};

export const NotiContainer = ({
  show_noti = false,
  type = TYPE_NOTI.SUCCESS,
  message = NOTI_MESSAGE_DEFAULT[TYPE_NOTI.SUCCESS],
}) => {
  const renderCount = useRef(0);
  const [showMessage, setShowMessage] = useState(false);

  useEffect(() => {
    if (renderCount.current === 2) return;
    renderCount.current += 1;
    if (renderCount.current > 1) {
      setShowMessage(show_noti);
    }
  }, [show_noti]);

  return (
    <div
      className={`noti-popup ${
        !showMessage ? 'hide-first' : 'd-flex align-items-center'
      }`}
    >
      <img src={icon_noti[type]} alt="noti" width={28} height={28} />
      <span>{message}</span>
    </div>
  );
};
