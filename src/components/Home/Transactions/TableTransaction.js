import SortIcon from "assets/icons/SortIcon";
import useClickOutside from "hooks/useClickOutSite";
import useDragToScroll from "hooks/useDragToScroll";
import useGetDataMint from "hooks/useGetDataMint";
import React, { memo, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";

function addUniqueFields(items, newFieldName, callbackFnCondition) {
  const nameCount = {};

  return items.map(item => {
    let newValue = callbackFnCondition(item);
    if (nameCount[newValue] !== undefined) {
      nameCount[newValue]++;
      newValue = `${newValue}-${nameCount[newValue]}`;
    } else {
      nameCount[newValue] = 0;
    }

    return {
      ...item,
      [newFieldName]: newValue
    };
  });
}

const RowItem = memo(({ record, columns, showSwap }) => {
  const dataLayoutSwap = useSelector((state) => state?.home?.dataSwap);
  const dataLayoutMint = useSelector((state) => state?.home?.dataMint);
  //   const [triggerAnimation, setTriggerAnimation] = useState(true);

  //   useEffect(() => {
  //     const timeId = setTimeout(() => {
  //       setTriggerAnimation(false);
  //       clearTimeout(timeId);
  //     }, 1000);
  //   }, [record]);
  // Ensure record?.price is a valid number or set it to an empty string
  //   const formattedPrice = isNaN(record?.price) ? "" : String(record?.price);
  return (
    <tr
      className={`body-table-row ${(dataLayoutSwap?.tx === record?.tx && showSwap) || dataLayoutMint?.unikey === record?.unikey
          ? "body-table-row-active"
          : ""
        }`}
    //   data-time={formattedPrice}
    >
      {columns?.map((item, index) => (
        <td
          className={`body-table ${item?.hideSwap && showSwap ? "hide-swap" : ""
            }`}
          key={`${item?.key}-${index}`}
        >
          {item?.render ? item?.render(record) : record[item?.key]}
        </td>
      ))}
    </tr>
  );
});

const TableTransaction = memo(({ columns, dataByPage, showSwap }) => {
  const tableRef = useDragToScroll();
  const tableLayoutRef = useRef();
  // const dataWithUnikey = addUniqueFields(dataByPage, 'unikey', (record) => `${record?.tx}-${record?.farcasterInfo?.fid}`)
  // console.log('---> dataWithUnikey', dataWithUnikey)
  const dispatch = useDispatch()

  const { getDataMint } = useGetDataMint();

  useClickOutside(tableLayoutRef, () => getDataMint({}));
  return (
    <div
      className={`container-table fadeIn`}
      id="tableContainer"
      ref={tableRef}
    >
      <table
        className={showSwap ? "hide-row-table" : "show-full-table"}
        style={{
          backgroundColor: "#11111A",
          width: "100%",
          // minWidth: 1300,
          // overflowX: 'scroll'
        }}
        ref={tableLayoutRef}
      >
        <thead className="head-table">
          <tr>
            {columns?.map((item, index) => (
              <th
                className={`header-table ${item?.hideSwap && showSwap ? "hide-swap" : ""
                  } ${item?.align === "left"
                    ? "align-left"
                    : item?.align === "center"
                      ? "align-center"
                      : item?.align === "right"
                        ? "align-right"
                        : ""
                  }`}
                key={`${item?.key}-${index}`}
              >
                {item?.sort ? (
                  <div
                    className="d-flex align-items-center"
                    style={{
                      cursor: "pointer",
                      gap: "6px",
                      width: "100%",
                      padding: "0",
                    }}
                  >
                    <span>{item?.title}</span>
                    <SortIcon />
                  </div>
                ) : (
                  item?.title
                )}
              </th>
            ))}
          </tr>
        </thead>

        <tbody
          style={{
            position: "relative",
          }}
        >
          {addUniqueFields(dataByPage, 'unikey', (record) => `${record?.tx}-${record?.farcasterInfo?.fid}`).map((record) => {
            return (
              <RowItem
                // key={`${record?.tx}-${record?.farcasterInfo?.fid}`}
                key={`${record.unikey}`}
                record={record}
                columns={columns}
                showSwap={showSwap}
              />
            );
          })}
        </tbody>
      </table>
    </div>
  );
});
TableTransaction.displayName = "TableTransaction";
export default TableTransaction;
