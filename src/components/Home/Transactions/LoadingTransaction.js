import React from "react";
export const LoadingTransaction = ({ height = "100vh", className }) => {
  return (
    <div
      className={`d-flex justify-content-center align-items-center ${className}`}
      style={{
        height,
      }}
    >
      <span className="loader">
        <img alt="loading" src="/assets/icons/loading.svg" />
      </span>
    </div>
  );
};
