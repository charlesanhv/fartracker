import { VipIcon, WhaleIcon } from "assets/icons";
import CopyIconTable from "assets/icons/copyIconTable";
import { IconKOL } from "assets/icons/iconKOL";
import ImageCommon from "components/ImageCommon/ImageCommon";
import { imageTier } from "constants/Transactions.constants";
import { Link } from "react-router-dom";

export default function LayoutForm({ record, handleCopy, openNoti }) {
  return (
    <div
      className="d-flex"
      style={{
        alignItems: "center",
        gap: "12px",
      }}
    >
      <span className="hide-respon">
        <ImageCommon
          alt="coin"
          src={record?.farcasterInfo?.avatar}
          width={30}
          height={30}
          borderRadius={50}
        />
      </span>
      <div className="d-flex flex-column">
   <div className="d-flex align-items-center" style={{ gap: "4px" }}>      
    <Link
          to={`https://warpcast.com/${record?.farcasterInfo?.username}`}
          target="_blank"
          style={{ textDecoration: "none" }}
        >
          <span title={record?.farcasterInfo?.displayName} className="name">
            @
            {record?.farcasterInfo?.username?.length > 12
              ? `${record?.farcasterInfo?.username.slice(0, 12)}...`
              : record?.farcasterInfo?.username}
          </span>
        </Link>

        {record?.vip && <VipIcon />}     
        {record?.whale && <WhaleIcon />}
        {record?.tier && <IconKOL />}
        {record?.tier && imageTier[record?.tier]}        
        </div>
        <div
          className="d-flex align-items-center address-container"
          onClick={() => handleCopy(record?.address, openNoti)}
          style={{
            gap: "4px",
            // marginTop: "-6px",
            cursor: "pointer",
          }}
        >
          <span
            className="address"
            style={{
              marginTop: "2px",
            }}
          >
            {record?.address?.slice(0, 5) +
              "....." +
              record?.address?.slice(-3)}
          </span>
          <div
            style={{
              position: "relative",
            }}
          >
            <div className="icon">
              <CopyIconTable />
            </div>
            <div
              style={{ position: "absolute", top: 0 }}
              className="hover-icon"
            >
              <CopyIconTable color={"#e1e2e5"} />
            </div>
          </div>
        </div>
      </div>
      <div
        className="d-flex align-items-center"
        style={{
          gap: "6px",
        }}
      >
        {/* {record?.vip && <VipIcon />} */}
        {/* <div className="dot" />
                <span
                    className="time"
                    style={{
                        fontWeight: '500',
                    }}
                >
                    {record?.time}
                </span> */}
      </div>
      {/* {record?.whale && <WhaleIcon />} */}
    </div>
  );
}
