import React from "react";
import { Link } from "react-router-dom";
import LayoutForm from "./LayoutForm";
import SourceIcon from "assets/icons/SourceIcon";
import ImageCommon from "components/ImageCommon/ImageCommon";
import { formatTimestamp, truncateString } from "lib/util";
import CopyIconTable from "assets/icons/copyIconTable";
import Tooltip from "components/Tooltip/Tooltip";
import QuickMintIcon from "assets/icons/QuickMintIcon";
import { useDispatch } from "react-redux";
import { ConnectKitButton } from "connectkit";

const createColumnsMint = (
  handleCopy,
  openNoti,
  mintNFT,
  getDataMint,
  priceQuickAction
) => {
  let isLoading = false;

  const handleQuickMintClick = async (record) => {
    getDataMint(record);
    if (isLoading) return;

    isLoading = true;
    try {
      const result = await mintNFT(record?.tx);
      console.log("result", result);
    } catch (error) {
      console.log("error", error);
    } finally {
      isLoading = false;
    }
  };

  return [
    {
      key: "from",
      title: "From",
      render: (record) => {
        return (
          <LayoutForm
            record={record}
            handleCopy={handleCopy}
            openNoti={openNoti}
          />
        );
      },
    },
    {
      key: "block",
      title: "Block",
      render: (record) => {
        return <span className="text-primary">{record?.block}</span>;
      },
    },
    {
      key: "time",
      title: "Time",
      render: (record) => {
        const formattedTime = formatTimestamp(record?.time);
        return <span className="time">{formattedTime}</span>;
      },
    },
    {
      key: "maker",
      title: "Contract",
      render: (record) => {
        return (
          <div
            className="d-flex align-items-center address-container"
            style={{
              gap: "4px",
              // marginTop: "-6px",
              cursor: "pointer",
            }}
          >
            <Link
              to={`https://basescan.org/address/${record?.maker}`}
              target="_blank"
              className="content-td"
              style={{ textDecoration: "none" }}
            >
              <Tooltip message={record?.maker}>
                <span
                  className="address-mint"
                  style={{
                    marginTop: "2px",
                  }}
                >
                  {truncateString(record?.maker, 20, "center")}
                </span>
              </Tooltip>
            </Link>

            <div
              style={{
                position: "relative",
              }}
              onClick={() => handleCopy(record?.maker, openNoti)}
            >
              <div className="icon">
                <CopyIconTable />
              </div>
              <div
                style={{ position: "absolute", top: 0 }}
                className="hover-icon"
              >
                <CopyIconTable color={"#e1e2e5"} />
              </div>
            </div>
          </div>
        );
      },
    },
    {
      key: "action",
      title: "Action",
      render: (record) => (
        <div
          className="d-flex align-items-center action-content content-td"
          style={{
            gap: "4px",
          }}
        >
          <span
            className="text-green"
            style={{
              marginTop: "2px",
            }}
          >
            {record?.action}
          </span>
        </div>
      ),
    },
    {
      key: "source",
      title: "Source",
      render: (record) => (
        <Link
          to={`https://basescan.org/tx/${record?.tx}`}
          target="_blank"
          className="content-td address-container"
          style={{ position: "relative" }}
        >
          <span style={{ display: "flex" }} className="icon">
            <SourceIcon />
          </span>
          <span
            style={{ display: "flex", position: "absolute", left: 0 }}
            className="hover-icon"
          >
            <SourceIcon color="#5083e3" />
          </span>
        </Link>
      ),
    },

    {
      key: "type",
      title: "Type",
      render: (record) => <span className="badge">{record?.type}</span>,
    },
    {
      key: "token",
      title: "Item",
      hideSwap: true,
      align: "left",
      render: (record) => {
        const stringItem = `${String(record?.nft?.name)}#${String(
          record?.nft?.tokenId
        )}`;
        return (
          <Link
            to={`https://rarible.com/collection/base/${record?.maker}/items`}
            target="_blank"
            className="d-flex align-items-center token-content content-td"
            style={{
              gap: "4px",
              textDecoration: "none",
            }}
          >
            <ImageCommon
              src={record?.logo_token}
              alt="image_token"
              width={27}
              height={27}
              borderRadius="20px"
            />
            <Tooltip message={`${record?.nft?.name}#${record?.nft?.tokenId}`}>
              <span className="address-mint" style={{ fontSize: "16px" }}>
                {truncateString(stringItem, 16)}
              </span>
            </Tooltip>
          </Link>
        );
      },
    },
    {
      key: "quick-mint",
      title: "Quick Mint",
      hideSwap: true,
      align: "center",
      render: (record) => (
        <ConnectKitButton.Custom>
          {({ isConnected, show, truncatedAddress, ensName }) => {
            if (isConnected) {
              return (
                <div
                  className="btn-quickMint"
                  onClick={() => handleQuickMintClick(record)}
                  style={{
                    display: "flex",
                    margin: "0 auto",
                    textAlign: "center",
                    justifyContent: "center",
                  }}
                >
                  <span
                    style={{
                      zIndex: 10,
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <QuickMintIcon />
                  </span>
                </div>
              );
            } else {
              return (
                <div
                  className="btn-quickMint"
                  onClick={show}
                  style={{
                    display: "flex",
                    margin: "0 auto",
                    textAlign: "center",
                    justifyContent: "center",
                  }}
                >
                  <span
                    style={{
                      zIndex: 10,
                      display: "flex",
                      justifyContent: "center",
                      alignItems: "center",
                    }}
                  >
                    <QuickMintIcon />
                  </span>
                </div>
              )
            }

          }}

        </ConnectKitButton.Custom>

      ),
    },
    // {
    //   key: "quick-action",
    //   title: "Quick Action",
    //   hideSwap: true,
    //   render: (record) => (
    //     <div style={{ display: "flex", justifyContent: "flex-end" }}>
    //       <CustomInput
    //         prefix={<QuickActionIcon />}
    //         type="number"
    //         maxLength={10}
    //         disabled
    //         loading={false}
    //         value={priceQuickAction}
    //         readOnly={true}
    //         placeholder=""
    //         onClick={() => {
    //           mintNFT(record?.tx);
    //         }}
    //       />
    //     </div>
    //   ),
    // },
  ];
};

export default createColumnsMint;
