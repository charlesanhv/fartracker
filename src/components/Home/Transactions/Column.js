import React from "react";
import { Link } from "react-router-dom";
import LayoutForm from "./LayoutForm";
import { ActionDump, ActionPump } from "assets/icons";
import SourceIcon from "assets/icons/SourceIcon";
import ImageCommon from "components/ImageCommon/ImageCommon";
import CustomInput from "components/CustomInput/CustomInput";
import QuickActionIcon from "assets/icons/QuickActionIcon";
import { formatTimestamp, parseAmount } from "lib/util";

const createColumns = (
  handleCopy,
  openNoti,
  onOpenSwap,
  getDataSwap,
  priceQuickAction
) => [
  {
    key: "from",
    title: "From",
    render: (record) => {
      return (
        <LayoutForm
          record={record}
          handleCopy={handleCopy}
          openNoti={openNoti}
        />
      );
    },
  },
  {
    key: "time",
    title: "Time",
    render: (record) => {
      const formattedTime = formatTimestamp(record?.time);
      return <span className="time">{formattedTime}</span>;
    },
  },
  {
    key: "action",
    title: "Action",
    render: (record) => (
      <div
        className="d-flex align-items-center action-content content-td"
        style={{
          gap: "4px",
        }}
      >
        <span
          className={`${record?.action === "PUMP" ? "text-green" : "text-red"}`}
          style={{
            marginTop: "2px",
          }}
        >
          {record?.action}
        </span>
        {record?.action === "PUMP" ? <ActionPump /> : <ActionDump />}
      </div>
    ),
  },
  {
    key: "source",
    title: "Source",
    render: (record) => (
      <Link
        to={`https://basescan.org/tx/${record?.tx}`}
        target="_blank"
        className="content-td"
      >
        <SourceIcon />
      </Link>
    ),
  },
  {
    key: "amount",
    title: "Amount",
    render: (record) => {
      const amount = parseAmount(record?.amount, 9);
      return (
        <span className="time" style={{ fontWeight: "500" }} title={amount}>
          {amount}
        </span>
      );
    },
  },
  {
    key: "token",
    title: "Token",
    hideSwap: true,
    render: (record) => {
      return (
        <Link
          to={`https://dexscreener.com/base/${record?.addressToken}`}
          target="_blank"
          className="d-flex align-items-center token-content content-td"
          style={{
            gap: "4px",
            textDecoration: "none",
          }}
        >
          <ImageCommon
            src={record?.logo_token}
            alt="image_token"
            type="logo_token"
            width={20}
            height={20}
            borderRadius="20px"
          />
          <span className="name-token">{record?.token}</span>
        </Link>
      );
    },
  },
  {
    key: "price",
    title: "Price",
    hideSwap: true,
    render: (record) => {
      const price = parseAmount(
        record?.price,
        Number(record?.price) > 1000 ? 0 : 9
      );
      const displayPrice = isNaN(Number(record?.price))
        ? price
        : price.length > 8
        ? `$${price.substring(0, 8)}...`
        : `$${price}`;

      return (
        <div
          className="d-flex align-items-center token-content content-td"
          style={{ gap: "4px" }}
          title={price}
        >
          <span className="time" style={{ fontWeight: "500" }}>
            {displayPrice}
          </span>
        </div>
      );
    },
  },
  {
    key: "value",
    title: "Value",
    hideSwap: true,
    render: (record) => {
      const value = parseAmount(
        record?.value,
        Number(record?.value) > 1000 ? 0 : 1
      );
      const displayValue = isNaN(Number(record?.value))
        ? value
        : value.length > 8
        ? `$${value.substring(0, 8)}...`
        : `$${value}`;

      return (
        <div
          className="token-content content-td"
          style={{ gap: "4px" }}
          title={value}
        >
          <span>{displayValue}</span>
        </div>
      );
    },
  },
  {
    key: "quick-action",
    title: "Quick Action",
    hideSwap: true,
    render: (record) => (
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <CustomInput
          prefix={<QuickActionIcon />}
          type="number"
          maxLength={10}
          disabled
          loading={false}
          value={priceQuickAction}
          readOnly={true}
          placeholder=""
          onClick={() => {
            onOpenSwap();
            getDataSwap(record);
          }}
        />
      </div>
    ),
  },
];

export default createColumns;
