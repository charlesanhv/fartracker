import QuickActionIcon from "assets/icons/QuickActionIcon";
import LoadingIcon from "assets/icons/LoadingIcon";
import { useEffect, useState } from "react";

export default function BtnQuickAction({ onClick, price }) {
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setLoading(true)
        const timeOut = setTimeout(() => {
            setLoading(false)
        }, 1000)
        return () => {
            clearTimeout(timeOut)
        }
    }, [price])
    return (
        <div className="content-td">
            <div
                className="d-flex align-items-center btn-quick-action"
                style={{
                    gap: '4px',
                    width: '95px',
                    position: 'relative',
                }}
                onClick={() => {
                    onClick && onClick();
                }}
            >
                <span style={{width:16, height:26}}><QuickActionIcon /></span>
                <span className="title-quick-action">{price}$</span>
                {loading && (
                    <div className="loading-quick-action">
                    <LoadingIcon />
                </div>
                )}
            </div>
        </div>
    )
}