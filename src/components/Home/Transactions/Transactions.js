import { createPortal } from "react-dom";
import { TYPE_NOTI } from "constants";
import useShowNoti from "hooks/useShowNoti";
import { useDispatch, useSelector } from "react-redux";
import useGetDataByPage from "hooks/useGetDataByPage";
import useGetDataSwap from "hooks/useGetDataSwap";
import useCopyAddress from "hooks/useCopyAddress";
import TableTransaction from "./TableTransaction";
import { memo, useMemo } from "react";
import { NotiContainer } from "../NotiContainer";
import { actionSetShowSwap } from "../../../redux/hookReducer";
import createColumns from "./Column";
import createColumnsMint from "./ColumnMint";
import useWeb3 from "hooks/useWeb3";
import useGetDataMint from "hooks/useGetDataMint";
import { selectType } from "../../../redux/homeReducer";
import { FILTER_ACTION } from "constants";

export const Transactions = memo(
  ({ DataTransactions = [], dataType = "Default" }) => {
    const dispatch = useDispatch();
    const showSwap = useSelector((state) => state?.hook?.showSwap);
    const priceQuickAction = useSelector(
      (state) => state?.home?.buy_sell_value?.amount
    );
    const filterType = useSelector(selectType);
    const { isShowing, openNoti } = useShowNoti(false);
    const { handleCopy } = useCopyAddress();
    const { getDataSwap } = useGetDataSwap();
    const { getDataMint } = useGetDataMint();
    const { mintNFT } = useWeb3();
    // Memoizing column creation to avoid recalculating on every render
    const columns = useMemo(
      () =>
        dataType === "Default"
          ? createColumns(
              handleCopy,
              openNoti,
              () => dispatch(actionSetShowSwap(true)),
              getDataSwap,
              priceQuickAction
            )
          : createColumnsMint(
              handleCopy,
              openNoti,
              mintNFT,
              getDataMint,
              priceQuickAction
            ),
      [
        handleCopy,
        openNoti,
        dispatch,
        getDataSwap,
        getDataMint,
        priceQuickAction,
        dataType,
      ]
    );
    const dataByPage = useGetDataByPage(DataTransactions);

    return (
      <>
        <TableTransaction
          columns={columns}
          dataByPage={
            filterType === FILTER_ACTION.TYPE.VIP ||
            filterType === FILTER_ACTION.TYPE.WHALE || 
            filterType === FILTER_ACTION.TYPE.USER
              ? DataTransactions
              : dataByPage
          }
          showSwap={showSwap}
        />
        {createPortal(
          <div className={`${isShowing ? "show-noti" : "hide-noti"}`}>
            <NotiContainer
              show_noti={isShowing}
              type={TYPE_NOTI.SUCCESS}
              message="Copied to clipboard"
            />
          </div>,
          document.body
        )}
      </>
    );
  }
);
