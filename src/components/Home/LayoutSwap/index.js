import ChangeIcon from 'assets/icons/ChangeIcon';
import CloseIcon from 'assets/icons/CloseIcon';
import SwapIcon from 'assets/icons/SwapIcon';
import { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import SwapItem from './SwapItem';
import { TYPE_SWAP } from 'constants';
import { TYPE_BUY_SELL } from 'constants';
import useWeb3 from 'hooks/useWeb3';
import { actionSetShowSwap } from '../../../redux/hookReducer';
import { dataTokenWallet } from 'constants/Transactions.constants';
import { useAccount } from 'wagmi';
import { ConnectKitButton } from 'connectkit';

export const LayoutSwap = ({ valuePrice = 0, typeSwap }) => {
  const dispatch = useDispatch();
  const { web3Info, disconnectWeb3 } = useWeb3();
  const open = useSelector((state) => state?.hook?.showSwap);
  const dataLayoutSwap = useSelector((state) => state?.home?.dataSwap);
  const account = useAccount();

  const [type, setType] = useState(typeSwap);
  const [valueMax, setValueMax] = useState(120);

  const handleMaxValue = (type) => {
    setValueMax(1000);
  };

  useEffect(() => {
    if (!open) {
      setValueMax(120);
    }
  }, [open]);

  useEffect(() => {
    setType(typeSwap);
  }, [typeSwap]);

  // Determine connection status with memoization for better performance
  // const isConnectWallet = useMemo(
  //   () => web3Info?.address && web3Info?.status !== 0,
  //   [web3Info]
  // );
  const isConnectWallet = account.isConnected;

  const onClose = () => {
    dispatch(actionSetShowSwap(false));
  };

  return (
    <>
      {open && (
        <>
          <div
            className={`overlay ${open ? 'show-overlay' : ''}`}
            id="overlay"
          />
          <div
            className={`swap-token ${open ? 'show-swap' : ''}`}
            id="swap-element"
            style={{ height: '665px', position: 'relative', width: '480px' }}
          >
            <img
              alt="swap-bg"
              src="assets/images/Swap_token_bg.svg"
              style={{
                position: 'absolute',
                width: 'calc(100% + 10px)',
                height: '100%',
                bottom: '0',
                left: '-4px',
                right: '0px',
                top: '0',
              }}
            />
            <div
              className="btn-close-swap"
              style={{
                cursor: 'pointer',
                position: 'absolute',
                right: '7px',
                top: '9px',
                zIndex: '100',
              }}
              onClick={onClose}
            >
              <CloseIcon />
            </div>
            <div
              className="container-swap"
              style={{ position: 'relative', zIndex: '100' }}
            >
              <span className="title-swap">Swap Token</span>
              <div
                className="d-flex content-swap"
                style={{
                  alignItems: 'center',
                  flexDirection:
                    type === TYPE_BUY_SELL.BUY ? 'column-reverse' : 'column',
                  gap: '12px',
                  width: '100%',
                }}
              >
                <SwapItem
                  data={dataLayoutSwap}
                  type={TYPE_SWAP.TOKEN}
                  valuePrice={valuePrice}
                  onClickMax={handleMaxValue}
                  valueMax={valueMax}
                  showSwap={open}
                />
                <div
                  className="btn-change-swap"
                  style={{
                    border: '1px solid rgba(255, 255, 255, 0.08)',
                    borderRadius: '4px',
                    cursor: 'pointer',
                    padding: '6px',
                  }}
                  onClick={() =>
                    setType(
                      type === TYPE_BUY_SELL.BUY
                        ? TYPE_BUY_SELL.SELL
                        : TYPE_BUY_SELL.BUY
                    )
                  }
                >
                  <ChangeIcon />
                </div>
                <SwapItem
                  data={dataTokenWallet}
                  type={TYPE_SWAP.WALLET}
                  valuePrice={valuePrice}
                  showSwap={open}
                />
              </div>
              {/* <LayoutConnect
                isConnectWallet={isConnectWallet}
                disconnectWeb3={disconnectWeb3}
                web3Info={web3Info}
                type="swap"
              > */}
                <div className="btn-swap">
                  {!isConnectWallet ? (
                    // <span className="title-btn-swap">Connect wallet</span>
                    <ConnectKitButton />
                  ) : (
                    <>
                      <span className="title-btn-swap">Swap</span>
                      <SwapIcon />
                    </>
                  )}
                </div>
              {/* </LayoutConnect> */}
            </div>
          </div>
        </>
      )}
    </>
  );
};
