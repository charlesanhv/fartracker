import DropDownIcon from "assets/icons/DropDownIcon";
import ImageCommon from "components/ImageCommon/ImageCommon";
import { TYPE_SWAP } from "constants";
import useScreenType from "hooks/useScreenType";
import { truncateString } from "lib/util";

const DropDownSwap = ({ logoToken, NameToken, type }) => {
  const { isMobile } = useScreenType();
  const dataToken = {
    logo_token: logoToken,
    token: NameToken,
  };
  return (
    <div
      className="d-flex align-items-center chossen-token"
      style={{ gap: "4px" }}
    >
      <ImageCommon
        src={dataToken?.logo_token}
        alt="token_simpol_1"
        type={type === TYPE_SWAP.TOKEN ? "logo_token" : ""}
        width={28}
        height={28}
        borderRadius={28}
      />
      <span
        className="title-swap"
        style={{
          fontSize: "15px",
        }}
        title={dataToken?.token}
      >
        {isMobile ? truncateString(dataToken?.token, 6) : dataToken?.token}
      </span>
      {type === TYPE_SWAP.TOKEN && <DropDownIcon />}
    </div>
  );
};
export default DropDownSwap;
