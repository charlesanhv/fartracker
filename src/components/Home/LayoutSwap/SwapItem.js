import InputCustomCaret from "components/InputCustomCaret/InputCustomCaret"
import { TYPE_SWAP } from "constants"
import DropDownSwap from "./DropDownSwap"
import { TYPE_BUY_SELL } from "constants"
import { useSelector } from "react-redux"

const SwapItem = ({ data, type, valuePrice, onClickMax, valueMax = 120, showSwap }) => {
    const typeBuySell = useSelector((state) => state?.home?.buy_sell_value?.type);
    return (
        <div
            className="content-swap-item d-flex"
            style={{
                flexDirection: "column",
                gap: "6px",
            }}
        >
            <div className="d-flex"
                style={{
                    justifyContent: "space-between",
                    marginBottom: "7px",
                }}
            >
                <div
                    className="d-flex align-items-center" style={{ gap: "6px", position: "relative" }}
                >
                    <InputCustomCaret value={valuePrice} id="type" showSwap={showSwap}/>
                </div>
                <DropDownSwap logoToken={data?.logo_token} NameToken={data?.token} type={type} />
            </div>
            <div
                className="d-flex"
                style={{
                    justifyContent: "space-between",
                }}
            >
                <div className="d-flex align-items-center" style={{ gap: "6px" }}>
                    <span className="price-swap">${valueMax}</span>
                </div>
                <div className="d-flex align-items-center" style={{ gap: "6px" }}>
                    <span
                        className="price-swap"
                        style={{
                            color: "rgba(255, 255, 255, 0.50)",
                            fontSize: "15px",
                            fontWeight: "500",
                        }}
                    >
                        Balance
                    </span>
                    <span className="price-swap">${valueMax}</span>
                    {(typeBuySell === TYPE_BUY_SELL.BUY && TYPE_SWAP.WALLET === type) || (typeBuySell === TYPE_BUY_SELL.SELL && TYPE_SWAP.TOKEN === type) ? <span className="max-swap" onClick={() => onClickMax && onClickMax()}>Max</span> : null}
                </div>
            </div>
        </div>
    )
}

export default SwapItem