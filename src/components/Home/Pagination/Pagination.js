import React, { memo, useEffect, useState } from "react";
import _ from "lodash";
import { useDispatch, useSelector } from "react-redux";
import {
  ArowLeftEnabled,
  ArowLeftDisabled,
  ArowRightEnabled,
  ArowRightDisabled,
} from "assets/icons";
import {
  getPageNumber,
  selectType,
  setPageNumber,
} from "../../../redux/homeReducer";
import { FILTER_ACTION } from "constants";

const Pagination = memo((props) => {
  const { size, step } = props;
  const pageNumber = useSelector(getPageNumber);
  const filterType = useSelector(selectType);
  const dataFilter = useSelector((state) => state.home.dataFilter);

  // const [active, setActive] = useState(pageNumber);

  const [selectedPage, setSelectedPage] = useState(pageNumber);
  const dispatch = useDispatch();

  const showingNumbers = step * 2 + 1;
  let startNumber = 2;
  let startArrayNumber = step;

  let needStartDots = false;
  let needEndDots = false;

  const handleClickPage = (page) => {
    setSelectedPage(page);
    dispatch(setPageNumber(page));
  };

  if (selectedPage > step) {
    startArrayNumber = selectedPage - step;
    needStartDots = selectedPage > step + startNumber;
  }

  if (size > showingNumbers) {
    needEndDots = size > selectedPage + step + 1;
    if (size < selectedPage + step + 1) {
      startArrayNumber = size - showingNumbers;
    }
  }

  let contentNumber;

  useEffect(() => {
    if (pageNumber === 1) {
      setSelectedPage(pageNumber);
    }
  }, [pageNumber]);

  return (
    <ul className="container containerPagination">
      {selectedPage > 1 ? (
        <li
          className="page-item prev arrow-icon"
          onClick={() => handleClickPage(selectedPage - 1)}
        >
          {/* <ArowLeftEnabled /> */}
          <div className="icon">
            <ArowLeftEnabled />
          </div>
          <div style={{ position: "absolute" }} className="hover-icon">
            <ArowLeftEnabled color={"#8a66d8"} />
          </div>
        </li>
      ) : (
        <li
          className="page-item prev arrow-icon"
          style={{ cursor: "not-allowed" }}
        >
          <ArowLeftDisabled />
        </li>
      )}
      {size > showingNumbers + startNumber ? (
        <>
          <li
            onClick={(e) =>
              handleClickPage(Number(e.currentTarget.textContent))
            }
            className={`page-item ${
              selectedPage === 1 ? "page-item-select" : ""
            }`}
          >
            <p
              className={`page_link ${
                selectedPage === 1 ? "page_link-select" : ""
              }`}
            >
              1
            </p>
          </li>
          {needStartDots && (
            <div className="page-item">
              <span className="page_link">...</span>
            </div>
          )}
          {_.times(showingNumbers, (i) => {
            contentNumber = needStartDots ? startArrayNumber : startNumber;
            startNumber++;
            startArrayNumber++;
            return (
              <li
                key={i}
                className={`page-item ${
                  selectedPage === contentNumber ? "page-item-select" : ""
                }`}
                onClick={(e) =>
                  handleClickPage(Number(e.currentTarget.textContent))
                }
                // onMouseEnter={()=>{console.log('enter',contentNumber)}}
                // onMouseLeave={()=>{console.log('leave' ,contentNumber)}}
              >
                <p
                  className={`page_link ${
                    selectedPage === contentNumber ? "page_link-select" : ""
                  }`}
                >
                  {contentNumber}
                </p>
              </li>
            );
          })}
          {needEndDots && (
            <div className="page-item">
              <span className="page_link">...</span>
            </div>
          )}
          <li
            className={`page-item ${
              selectedPage === size ? "page-item-select" : ""
            }`}
            onClick={(e) =>
              handleClickPage(Number(e.currentTarget.textContent))
            }
          >
            <p
              className={`page_link ${
                selectedPage === size ? "page_link-select" : ""
              }`}
            >
              {size}
            </p>
          </li>
        </>
      ) : (
        ((startArrayNumber = 1),
        _.times(size, (i) => (
          <li
            key={i}
            className={`page-item ${
              selectedPage === startArrayNumber ||
              filterType === FILTER_ACTION.TYPE.VIP ||
              filterType === FILTER_ACTION.TYPE.WHALE ||
              filterType === FILTER_ACTION.TYPE.USER
                ? "page-item-select"
                : ""
            }`}
            onClick={(e) =>
              handleClickPage(Number(e.currentTarget.textContent))
            }
          >
            <p
              className={`page_link ${
                selectedPage === startArrayNumber ||
                filterType === FILTER_ACTION.TYPE.VIP ||
                filterType === FILTER_ACTION.TYPE.WHALE ||
                filterType === FILTER_ACTION.TYPE.USER
                  ? "page_link-select"
                  : ""
              }`}
            >
              {filterType === FILTER_ACTION.TYPE.VIP ||
              filterType === FILTER_ACTION.TYPE.WHALE ||
              filterType === FILTER_ACTION.TYPE.USER
                ? selectedPage
                : startArrayNumber++}
            </p>
          </li>
        )))
      )}
      {filterType === FILTER_ACTION.TYPE.VIP ||
      filterType === FILTER_ACTION.TYPE.WHALE ||
      filterType === FILTER_ACTION.TYPE.USER ? (
        dataFilter?.length > 20 ? (
          <li
            className="page-item next arrow-icon"
            onClick={() => handleClickPage(selectedPage + 1)}
          >
            <div className="icon">
              <ArowRightEnabled />
            </div>
            <div style={{ position: "absolute" }} className="hover-icon">
              <ArowRightEnabled color={"#8a66d8"} />
            </div>
          </li>
        ) : (
          <li
            className="page-item next arrow-icon"
            style={{ cursor: "not-allowed" }}
          >
            <ArowRightDisabled />
          </li>
        )
      ) : selectedPage < size ? (
        <li
          className="page-item next arrow-icon"
          onClick={() => handleClickPage(selectedPage + 1)}
        >
          <div className="icon">
            <ArowRightEnabled />
          </div>
          <div style={{ position: "absolute" }} className="hover-icon">
            <ArowRightEnabled color={"#8a66d8"} />
          </div>
        </li>
      ) : (
        <li
          className="page-item next arrow-icon"
          style={{ cursor: "not-allowed" }}
        >
          <ArowRightDisabled />
        </li>
      )}
    </ul>
  );
});
Pagination.displayName = "Pagination";

export default Pagination;
