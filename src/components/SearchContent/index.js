import React, { useEffect, useRef, useState } from "react";
import "./SearchContent.css";
import {
  ClearIcon,
  FireIcon,
  IconResultSearch,
  InputSearchIcon,
  SearchIcon,
} from "assets/icons";
import { useDebounce } from "hooks/useDebounce";
import { searchContract } from "lib/rarible";
import useClickOutside from "hooks/useClickOutSite";
import ImageCommon from "components/ImageCommon/ImageCommon";
import { DEFAULT_RESULT_HEADER } from "constants";
import { LoadingTransaction } from "components/Home";
import { useDispatch } from "react-redux";
import { actionSetFilterByUser, setFilterType } from "../../redux/homeReducer";
import { useNavigate } from "react-router-dom";
const SearchContent = ({
  onClose,
  placeholder = "Search token, trader, transaction,...",
}) => {
  const navigate = useNavigate();

  const [inputValue, setInputValue] = useState("");
  const [resultSearch, setResultSearch] = useState(DEFAULT_RESULT_HEADER);
  const [loading, setLoading] = useState(false);

  const layoutRef = useRef();
  const inputRef = useRef(null);

  const dispatch = useDispatch();
  const inputDebounce = useDebounce(inputValue, 500);

  const handleOnchange = async (e) => {
    setInputValue(e.target.value);
  };

  const handleChooseSearchResult = (name) => {
    // dispatch(actionSetFilterByUser({username: name}));
    const currentPath = window.location.pathname;
    if (!currentPath.includes('mint')) {
      navigate(`/${name}`);
    } else {
      navigate(`/mint/${name}`);
    //   dispatch(actionSetFilterByUser({ username: name }));
    }
    // dispatch(setFilterType("user"));
    onClose && onClose();
  };

  useClickOutside(layoutRef, () => onClose && onClose());

  useEffect(() => {
    const handle = async () => {
      if (inputDebounce) {
        setLoading(true);
        const dataSearch = await searchContract(inputDebounce);
        setResultSearch(dataSearch);
        setLoading(false);
      }
    };
    handle();
  }, [inputDebounce]);

  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.focus();
    }
  }, []);

  return (
    <>
      <div className="overlay-popup-search fade-in">
        <div className="content-popup-search fade-in" ref={layoutRef}>
          <div
            className="containerInputSearch"
            style={{ position: "relative", width: "100%" }}
          >
            <div className="iconSearch">
              <SearchIcon className="iconSearch" />
            </div>
            <div className="boxInputSearch">
              <input
                placeholder={placeholder}
                type="text"
                value={inputValue}
                onChange={handleOnchange}
                ref={inputRef}
                style={{
                  width: "100%",
                  background: "transparent",
                  border: "none",
                  outline: "none",
                  color: "#fff",
                }}
              />
              {inputValue === "" ? (
                <div className="btn-input-search">
                  <InputSearchIcon />
                </div>
              ) : (
                <div
                  onClick={() => {
                    setInputValue("");
                  }}
                  style={{ cursor: "pointer" }}
                >
                  <ClearIcon />
                </div>
              )}
            </div>
          </div>
          {loading ? (
            <div style={{ width: "100%" }}>
              <LoadingTransaction height="210px" />
            </div>
          ) : (
            <div
              className="containerListResult fade-in"
              style={{ gap: 12, padding: "12px 0 0 0", minHeight: "100px" }}
            >
              {resultSearch?.length > 0 ? (
                <>
                  {resultSearch.map((item) => (
                    <div
                      key={item?.id}
                      onClick={() => handleChooseSearchResult(item.name)}
                    >
                      <div className="itemListResult">
                        <div className="IconResultSearch">
                          <IconResultSearch />
                        </div>
                        <div className="UnionIcon"></div>
                        <div className="contentItemList">
                          <ImageCommon
                            src={item?.image}
                            alt=""
                            imageError="/assets/images/nft.svg"
                            width={22}
                            height={22}
                            borderRadius={"50%"}
                          />
                          <p className="textItemList">
                            {item.name?.length > 10
                              ? item.name.slice(0, 10) + "..."
                              : item.name}
                          </p>
                        </div>
                        <FireIcon />
                      </div>
                    </div>
                  ))}
                </>
              ) : (
                <span
                  style={{
                    color: "#fff",
                    fontSize: 16,
                    fontWeight: 500,
                    textAlign: "center",
                  }}
                >
                  No result
                </span>
              )}
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default SearchContent;
