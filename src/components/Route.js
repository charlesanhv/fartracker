import { ROUTE } from '../constants';
import React from 'react';
import { Navigate, Outlet } from 'react-router-dom';
// import { useSelector } from 'react-redux'

function PublicRoute({ logged }) {
  return !logged ? <Outlet /> : <Navigate to={ROUTE.NAVIGATE.DASHBOARD} />;
}

function PrivateRoute({ logged }) {
  return logged ? <Outlet /> : <Navigate to={ROUTE.NAVIGATE.LOGIN} />;
}

function RankRoute({ rankRequire, client_rank }) {
  return client_rank >= rankRequire ? (
    <Outlet />
  ) : (
    <Navigate to={ROUTE.NAVIGATE.DASHBOARD} />
  );
}

export { PublicRoute, PrivateRoute, RankRoute };
