import { actionSetShowSwap } from "../redux/hookReducer";
import { useDispatch, useSelector } from "react-redux";
import useWeb3 from "hooks/useWeb3";
import { useState, useEffect, useMemo, useRef } from "react";
import { GAS_SWAP_TOKEN_SUPPORTED, ROUTER_ADDRESS, REDUX_ACTION } from "../constants";
import SwapIcon from "assets/icons/SwapIcon";
import { WalletIcon } from "assets/icons";
import useScreenType from "hooks/useScreenType";
import { truncateString } from "lib/util";
import CloseIcon from "assets/icons/CloseIcon";
// import Modal from "./Modal/Modal";
// import { Link } from "react-router-dom";
import { ConnectKitButton } from "connectkit";
import { useAccount } from "wagmi";


export default function InhouseWidget() {
  const dispatch = useDispatch();
  const { tokenFunction, kyberswapFunction } =
    useWeb3();
  const account = useAccount();
  const stableCryptoPrice = useSelector(
    (state) => state.reducer.stable_crypto_price
  );
  const open = useSelector((state) => state?.hook?.showSwap);
  const dataLayoutSwap = useSelector((state) => state?.home?.dataSwap);
  const { type: typeBuySell, amount: priceQuickAction } = useSelector(
    (state) => state?.home?.buy_sell_value
  );
  const [
    [gasTokenSelected, setGasTokenSelected],
    [gasInputAmount, setGasInputAmount],
    [maxGasAmount, setMaxGasAmount],
    [gasPrice, setGasPrice],
    [gasAllowance, setGasAllowance],
    [isSelectGas, setIsSelectGas],
  ] = [
      useState(GAS_SWAP_TOKEN_SUPPORTED[0]),
      useState(0),
      useState(0),
      useState(0),
      useState(false),
      useState(false),
    ];
  const [
    [targetTokenSelected, setTargetTokenSelected],
    [targetInputAmount, setTargetInputAmount],
    [maxTargetAmount, setMaxTargetAmount],
    [targetPrice, setTargetPrice],
    [targetAllowance, setTargetAllowance],
  ] = [
      useState(GAS_SWAP_TOKEN_SUPPORTED[1]),
      useState(0),
      useState(0),
      useState(0),
      useState(false),
    ];

  const [waitingApproveTarget, setWaitingApproveTarget] = useState(false);
  const [waitingApproveGas, setWaitingApproveGas] = useState(false);
  const [confirmSwap, setConfirmSwap] = useState(false);

  const [toTargetToken, setToTargetToken] = useState(true);

  const stopUsePriceQuickAction = useRef(false)

  // const modalRef = useRef();

  const { isMobile } = useScreenType();

  const isConnectWallet = account.isConnected;

  const onClose = () => {
    dispatch(actionSetShowSwap(false));
    // console.log(dataLayoutSwap);
    // console.log(GAS_SWAP_TOKEN_SUPPORTED);
  };
  function toFixedDown(number, decimals) {
    const factor = Math.pow(10, decimals);
    return (Math.floor(number * factor) / factor).toFixed(decimals);
  }
  const setInputAmount = (value, from) => {
    if (from == "gas") {
      setGasInputAmount(value);
      setTargetInputAmount(
        (value * gasPrice * (toTargetToken ? 1 - 0.15 / 100 : 1 + 0.15 / 100)) /
        targetPrice
      );
    } else {
      setTargetInputAmount(value);
      setGasInputAmount(
        (value *
          targetPrice *
          (toTargetToken ? 1 + 0.15 / 100 : 1 - 0.15 / 100)) /
        gasPrice
      );
    }
  };

  const swapToken = async () => {
    try {
      setConfirmSwap(true);
      //swap gas to target
      //check approve
      // checkApproveToken

      const swap = async (fromToken, toToken, inputAmount, decimals) => {
        const resultSwap = await kyberswapFunction.swapToken(
          fromToken.address,
          toToken.address,
          inputAmount.toString(),
          decimals
        );
        if (resultSwap?.hash) {
          setGasInputAmount(0);
          setTargetInputAmount(0);
        }
      };

      const approveAndWait = async (
        token,
        routerAddress,
        setWaitingApprove
      ) => {
        const approveStatus = await tokenFunction.approveToken(
          token.address,
          routerAddress
        );
        if (approveStatus !== false) {
          setWaitingApprove(true);
        }
      };

      if (toTargetToken && gasInputAmount > 0) {
        if (gasAllowance) {
          await swap(
            gasTokenSelected,
            targetTokenSelected,
            gasInputAmount,
            gasTokenSelected.decimals
          );
        } else {
          await approveAndWait(
            gasTokenSelected,
            ROUTER_ADDRESS.BASE_MAINNET.ROUTER_KYBERSWAP,
            setWaitingApproveGas
          );
        }
      } else if (targetInputAmount > 0) {
        if (targetAllowance) {
          await swap(
            targetTokenSelected,
            gasTokenSelected,
            targetInputAmount,
            targetTokenSelected.decimals
          );
        } else {
          await approveAndWait(
            targetTokenSelected,
            ROUTER_ADDRESS.BASE_MAINNET.ROUTER_KYBERSWAP,
            setWaitingApproveTarget
          );
        }
      }
    } catch (error) {
      console.log("Error Swap token:", error);
    } finally {
      setConfirmSwap(false);
    }
  };

  const updateMaxAmount = async () => {
    if (!open || !isConnectWallet) {
      return;
    }
    // console.log(
    //   gasTokenSelected.address,
    //   gasTokenSelected.symbol,
    //   targetTokenSelected.address
    // );
    if (
      gasTokenSelected.address?.length > 0 &&
      targetTokenSelected.address?.length > 0
    ) {
      // console.log(gasTokenSelected.address, targetTokenSelected.address);
      const gasBalance = await tokenFunction.tokenBalance(
        gasTokenSelected.address,
        account.address
      );
      const targetBalance = await tokenFunction.tokenBalance(
        targetTokenSelected.address,
        account.address
      );
      setMaxGasAmount(
        (gasBalance % 10 > 5 && gasTokenSelected.decimals >= 6
          ? gasBalance - 5
          : gasBalance) /
        10 ** gasTokenSelected.decimals
      );
      setMaxTargetAmount(
        (targetBalance % 10 > 5 && targetTokenSelected.decimals >= 6
          ? targetBalance - 5
          : targetBalance) /
        10 ** targetTokenSelected.decimals
      );

      const tempGasAllowance =
        gasTokenSelected.address ==
          ROUTER_ADDRESS.BASE_MAINNET.TOKEN_IN_IS_ETH_KYBERSWAP
          ? true
          : await tokenFunction.checkApproveToken(
            gasTokenSelected.address,
            account.address,
            ROUTER_ADDRESS.BASE_MAINNET.ROUTER_KYBERSWAP,
            gasInputAmount * 10 ** gasTokenSelected.decimals
          );
      setGasAllowance(tempGasAllowance);
      const tempTargetAllowance = await tokenFunction.checkApproveToken(
        targetTokenSelected.address,
        account.address,
        ROUTER_ADDRESS.BASE_MAINNET.ROUTER_KYBERSWAP,
        targetInputAmount * 10 ** targetTokenSelected.decimals
      );
      setTargetAllowance(tempTargetAllowance);
      setWaitingApproveGas(false);
      setWaitingApproveTarget(false);
    }
  };

  // const closeModal = () => {
  //   modalRef.current.close();
  //   setGasInputAmount(0);
  //   setTargetInputAmount(0);
  //   setConfirmSwap(false);
  //   dispatch(actionSetShowSwap(false));
  // };

  useEffect(() => {
    // console.log("isConnectWallet : ", isConnectWallet);
    if (
      dataLayoutSwap?.tokenInInfo?.name?.length > 0 &&
      dataLayoutSwap?.tokenOutInfo?.name?.length > 0 &&
      open
    ) {
      if (
        !GAS_SWAP_TOKEN_SUPPORTED.map((item) =>
          item?.address?.toLowerCase()
        ).includes(dataLayoutSwap.swap?.tokenIn?.address?.toLowerCase())
      ) {
        setTargetTokenSelected({
          name: dataLayoutSwap.tokenInInfo.name,
          symbol: dataLayoutSwap.tokenInInfo.symbol,
          decimals: dataLayoutSwap.tokenInInfo.decimals,
          address: dataLayoutSwap.tokenInInfo.address,
          icon:
            "https://dd.dexscreener.com/ds-data/tokens/base/" +
            dataLayoutSwap.tokenInInfo.address +
            ".png?size=lg&key=e9c33",
        });
      } else {
        setTargetTokenSelected({
          name: dataLayoutSwap.tokenOutInfo.name,
          symbol: dataLayoutSwap.tokenOutInfo.symbol,
          decimals: dataLayoutSwap.tokenOutInfo.decimals,
          address: dataLayoutSwap.tokenOutInfo.address,
          icon:
            "https://dd.dexscreener.com/ds-data/tokens/base/" +
            dataLayoutSwap.tokenOutInfo.address +
            ".png?size=lg&key=e9c33",
        });
      }
    }
    setTargetPrice(dataLayoutSwap.price);
  }, [dataLayoutSwap, open]);

  useEffect(() => {
    if (!isConnectWallet) {
      return;
    }
    setWaitingApproveGas(false);
    setWaitingApproveTarget(false);
    updateMaxAmount();
    const intervalId = setInterval(async () => {
      updateMaxAmount();
    }, 3500);
    return () => {
      clearInterval(intervalId);
    };
  }, [gasTokenSelected, targetTokenSelected, open, account]);

  useEffect(() => {
    if (gasTokenSelected.symbol == "ETH" || gasTokenSelected.symbol == "WETH") {
      setGasPrice(stableCryptoPrice.eth);
    } else if (gasTokenSelected.symbol == "DAI") {
      setGasPrice(stableCryptoPrice.dai);
    } else {
      setGasPrice(stableCryptoPrice.usd);
    }
  }, [stableCryptoPrice]);

  useEffect(() => {
    typeBuySell == "Buy" ? setToTargetToken(true) : setToTargetToken(false);
  }, [typeBuySell]);

  useEffect(() => {
    // if (!(Number(gasInputAmount) > 0 || Number(targetInputAmount) > 0)) {
    //   setGasInputAmount(0);
    //   setTargetInputAmount(0);
    // }
    if (open && Number(priceQuickAction) > 0) {
      setInputAmount(priceQuickAction, "gas");
    }
    if (!open && Number(priceQuickAction) === 0) {
      setGasInputAmount(0);
      setTargetInputAmount(0);
    }
  }, [open, priceQuickAction, gasPrice, targetPrice, toTargetToken]);

  useEffect(() => {
    !isConnectWallet && dispatch(actionSetShowSwap(false));
  }, [isConnectWallet]);

  return (
    <>
      {open && (
        <>
          <div className={`overlay show-overlay`} id="overlay" />
          <div
            className="swap-token show-swap"
            id="swap-element"
            style={{ height: 665, position: "relative", width: 480 }}
          >
            <img
              alt="swap-bg"
              src="assets/images/Swap_token_bg.svg"
              style={{
                position: "absolute",
                width: "calc(100% + 10px)",
                height: "100%",
                inset: "0px 0px 0px -4px",
              }}
            />
            <div
              className="btn-close-swap"
              style={{
                cursor: "pointer",
                position: "absolute",
                right: 7,
                top: 9,
                zIndex: 100,
              }}
              onClick={onClose}
            >
              <CloseIcon />
            </div>
            <div
              className="container-swap"
              style={{ position: "relative", zIndex: 100 }}
            >
              <span className="title-swap">Swap Token</span>
              <div
                className="d-flex content-swap"
                style={{
                  alignItems: "center",
                  flexDirection: toTargetToken ? "column" : "column-reverse",
                  gap: 12,
                  width: "100%",
                  cursor: "pointer",
                }}
              >
                <div
                  className="content-swap-item d-flex"
                  style={{ flexDirection: "column", gap: 6 }}
                >
                  <div
                    className="d-flex"
                    style={{ justifyContent: "space-between", marginBottom: 7 }}
                  >
                    <div
                      className="d-flex align-items-center"
                      style={{ gap: 6, position: "relative" }}
                      title={gasInputAmount}
                    >
                      <p id="type-shell" className="text-custom-caret">
                        {isMobile
                          ? truncateString(gasInputAmount, 6)
                          : truncateString(gasInputAmount, 15)}
                      </p>
                      <input
                        type="text"
                        className="input-custom-caret"
                        maxLength={10}
                        autoComplete="false"
                        // defaultValue=""
                        value={gasInputAmount}
                        onChange={(e) =>
                          !isNaN(e.target.value)
                            ? setInputAmount(e.target.value, "gas")
                            : null
                        }
                      />
                    </div>
                    <div>
                      <div
                        className="d-flex align-items-center chossen-token"
                        style={{ gap: 4 }}
                        onClick={() => setIsSelectGas(!isSelectGas)}
                      >
                        <img
                          alt="token_simpol_1"
                          loading="lazy"
                          className="fadeIn"
                          src={gasTokenSelected.icon}
                          style={{ borderRadius: 28, height: 28, width: 28 }}
                          onError={(e) => {
                            e.target.onerror = null; // Prevent infinite loop if fallback also fails
                            e.target.src = "/assets/images/empty-token.svg";
                          }}
                        />
                        <span
                          className="title-swap"
                          title={gasTokenSelected.symbol}
                          style={{ fontSize: 15 }}
                        >
                          {gasTokenSelected.symbol}
                        </span>
                        <svg
                          fill="none"
                          height={18}
                          viewBox="0 0 18 18"
                          width={18}
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M14.625 6.75L9 12.375L3.375 6.75"
                            stroke="#F1F2F3"
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="1.6"
                          />
                        </svg>
                      </div>
                      <div
                        style={{
                          position: "absolute",
                          zIndex: 1000,
                          display: isSelectGas ? "block" : "none",
                          width: "150px",
                          background: "#111",
                          padding: "10px",
                          marginTop: "10px",
                          borderRadius: "10px",
                        }}
                      >
                        {GAS_SWAP_TOKEN_SUPPORTED.map((item) => (
                          <div
                            key={item.symbol}
                            onClick={() => {
                              setGasTokenSelected(item);
                              setIsSelectGas(false);
                            }}
                            style={{
                              display: "flex",
                              justifyContent: "space-between",
                              padding: "5px",
                              cursor: "pointer",
                            }}
                          >
                            <img
                              src={item.icon}
                              alt=""
                              style={{
                                borderRadius: 28,
                                height: 28,
                                width: 28,
                              }}
                            />
                            <p
                              style={{
                                color: "#fff",
                                width: "75px",
                                fontWeight: "bold",
                              }}
                            >
                              {item.symbol}
                            </p>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                  <div
                    className="d-flex"
                    style={{ justifyContent: "space-between" }}
                  >
                    <div
                      className="d-flex align-items-center"
                      style={{ gap: 6 }}
                    >
                      <span className="price-swap">
                        ${(gasPrice * gasInputAmount).toFixed(2)}
                      </span>
                    </div>
                    <div
                      className="d-flex align-items-center"
                      style={{ gap: 6 }}
                    >
                      <span
                        className="price-swap"
                        style={{
                          color: "rgba(255, 255, 255, 0.5)",
                          fontSize: 15,
                          fontWeight: 500,
                        }}
                      >
                        Balance
                      </span>
                      <span className="price-swap" title={maxGasAmount}>
                        {isMobile
                          ? truncateString(maxGasAmount, 6)
                          : truncateString(maxGasAmount, 16)}
                      </span>
                      {toTargetToken ? (
                        <span
                          className="max-swap"
                          onClick={() => {
                            isConnectWallet &&
                              setInputAmount(
                                gasTokenSelected.symbol == "ETH"
                                  ? maxGasAmount >
                                    20000000 / 10 ** gasTokenSelected.decimals
                                    ? maxGasAmount -
                                    20000000 / 10 ** gasTokenSelected.decimals
                                    : maxGasAmount
                                  : maxGasAmount,
                                "gas"
                              );
                          }}
                        >
                          Max
                        </span>
                      ) : null}
                    </div>
                  </div>
                </div>
                <div
                  className="btn-change-swap"
                  style={{
                    border: "1px solid rgba(255, 255, 255, 0.08)",
                    borderRadius: 4,
                    cursor: "pointer",
                    padding: 6,
                  }}
                  onClick={() => setToTargetToken(!toTargetToken)}
                >
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width={33}
                    height={32}
                    fill="none"
                    viewBox="0 0 33 32"
                  >
                    <path
                      fill="#C4C6CD"
                      d="M26.499 4h-20a2.002 2.002 0 00-2 2v20a2.002 2.002 0 002 2h20a2.002 2.002 0 002-2V6a2.002 2.002 0 00-2-2zM21.45 17.465l-4.243 4.24a1.003 1.003 0 01-1.416 0l-4.241-4.24a1 1 0 011.414-1.414l2.535 2.535V11a1 1 0 012 0v7.587l2.537-2.536a1 1 0 011.414 1.414z"
                    ></path>
                  </svg>
                </div>
                <div
                  className="content-swap-item d-flex"
                  style={{ flexDirection: "column", gap: 6 }}
                >
                  <div
                    className="d-flex"
                    style={{ justifyContent: "space-between", marginBottom: 7 }}
                  >
                    <div
                      className="d-flex align-items-center"
                      style={{ gap: 6, position: "relative" }}
                      title={targetInputAmount}
                    >
                      <p id="type-shell" className="text-custom-caret">
                        {isMobile
                          ? truncateString(targetInputAmount, 6)
                          : truncateString(targetInputAmount, 15)}
                      </p>
                      <input
                        type="text"
                        className="input-custom-caret"
                        maxLength={10}
                        autoComplete="false"
                        // defaultValue=""
                        value={targetInputAmount}
                        onChange={(e) =>
                          !isNaN(e.target.value)
                            ? setInputAmount(e.target.value, "target")
                            : null
                        }
                      />
                    </div>
                    <div
                      className="d-flex align-items-center chossen-token"
                      style={{ gap: 4 }}
                    >
                      <img
                        alt="token_simpol_1"
                        loading="lazy"
                        className="fadeIn"
                        src={targetTokenSelected.icon}
                        style={{ borderRadius: 28, height: 28, width: 28 }}
                        onError={(e) => {
                          e.target.onerror = null; // Prevent infinite loop if fallback also fails
                          e.target.src = "/assets/images/empty-token.svg";
                        }}
                      />
                      <span
                        className="title-swap"
                        title={targetTokenSelected.symbol}
                        style={{ fontSize: 15 }}
                      >
                        {targetTokenSelected.symbol}
                      </span>
                    </div>
                  </div>
                  <div
                    className="d-flex"
                    style={{ justifyContent: "space-between" }}
                  >
                    <div
                      className="d-flex align-items-center"
                      style={{ gap: 6 }}
                    >
                      <span className="price-swap">
                        ${(targetPrice * targetInputAmount).toFixed(2)}
                      </span>
                    </div>
                    <div
                      className="d-flex align-items-center"
                      style={{ gap: 6 }}
                    >
                      <span
                        className="price-swap"
                        style={{
                          color: "rgba(255, 255, 255, 0.5)",
                          fontSize: 15,
                          fontWeight: 500,
                        }}
                      >
                        Balance
                      </span>
                      <span className="price-swap">
                        {isMobile
                          ? truncateString(maxTargetAmount, 6)
                          : truncateString(maxTargetAmount, 16)}
                      </span>
                      {!toTargetToken ? (
                        <span
                          className="max-swap"
                          onClick={() =>
                            isConnectWallet &&
                            setInputAmount(maxTargetAmount, "target")
                          }
                        >
                          Max
                        </span>
                      ) : null}
                    </div>
                  </div>
                </div>
              </div>
              <div
                className={
                  gasInputAmount === 0 && isConnectWallet
                    ? "btn-swap-disabled"
                    : "btn-swap"
                }
                onClick={swapToken}
              >
                {!isConnectWallet ? (
                  // <span className="title-btn-swap">Connect wallet</span>
                  // <ConnectKitButton />
                  <ConnectKitButton.Custom>
                    {({ isConnected, show, truncatedAddress, ensName }) => (
                      (
                        <>
                          <WalletIcon />
                          <span className="title-btn-swap" onClick={show}>
                            Connect wallet
                          </span>

                        </>
                      )

                    )}

                  </ConnectKitButton.Custom>
                ) : (
                  <>
                    <span className="title-btn-swap">
                      {" "}
                      {toTargetToken
                        ? gasAllowance
                          ? "Swap"
                          : waitingApproveGas
                            ? "Waiting..."
                            : "Approve and swap"
                        : targetAllowance
                          ? "Swap"
                          : waitingApproveTarget
                            ? "Waiting..."
                            : "Approve and swap"}{" "}
                    </span>
                    <SwapIcon
                      rotate={
                        confirmSwap ||
                        waitingApproveGas ||
                        waitingApproveTarget
                      }
                    />
                  </>
                )}
              </div>
            </div>
          </div>
          {/* <Modal ref={modalRef} onClose={closeModal}>
            <div
              className="header d-flex align-items-center"
              style={{ justifyContent: "flex-end", gap: "12px" }}
            >
              <span
                style={{ cursor: "pointer", marginTop: "-2px" }}
                onClick={closeModal}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="#F5F5F5"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  class="lucide lucide-x"
                >
                  <path d="M18 6 6 18" />
                  <path d="m6 6 12 12" />
                </svg>
              </span>
            </div>
            <div className="content-container">
              <span className="icon_success">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="28"
                  height="28"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="#ffffff"
                  strokeWidth="3"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  class="lucide lucide-check"
                >
                  <path d="M20 6 9 17l-5-5" />
                </svg>
              </span>
              <div className="content-container-desc">
                <span className="content-container-desc-title">
                  Swap Success!
                </span>
                <div
                  className="d-flex align-items-center"
                  style={{ gap: "12px" }}
                >
                  <div className="item">
                    <img
                      alt="token_simpol_1"
                      loading="lazy"
                      className="fadeIn"
                      src={gasTokenSelected.icon}
                      onError={(e) => {
                        e.target.onerror = null;
                        e.target.src = "assets/images/empty-token.svg";
                      }}
                      width="16px"
                      height="16px"
                    />
                    <span>
                      {truncateString(gasInputAmount, 16)}{" "}
                      {gasTokenSelected.symbol}
                    </span>
                  </div>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="#F5F5F5"
                    stroke-width="2.75"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    class="lucide lucide-arrow-right"
                  >
                    <path d="M5 12h14" />
                    <path d="m12 5 7 7-7 7" />
                  </svg>
                  <div className="item">
                    <img
                      alt="token_simpol_1"
                      loading="lazy"
                      className="fadeIn"
                      src={targetTokenSelected.icon}
                      onError={(e) => {
                        e.target.onerror = null;
                        e.target.src = "assets/images/empty-token.svg";
                      }}
                      width="16px"
                      height="16px"
                    />
                    <span>
                      {truncateString(targetInputAmount, 16)}{" "}
                      {targetTokenSelected.symbol}
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <Link
              to={`https://basescan.org/tx/${hash}`}
              target="_blank"
              className="view-explore"
            >
              View on explore
            </Link>
          </Modal> */}
        </>
      )}
    </>
  );
}
