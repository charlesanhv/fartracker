// Tooltip.js
import React, { useState, useRef, useEffect } from 'react';
import './Tooltip.css';

const Tooltip = ({ children, message }) => {
  const [visible, setVisible] = useState(false);
  const [position, setPosition] = useState({ top: 0, left: 0 });
  const tooltipRef = useRef(null);
  const containerRef = useRef(null);

  useEffect(() => {
    if (visible && tooltipRef.current && containerRef.current) {
      const tooltipRect = tooltipRef.current.getBoundingClientRect();
      const containerRect = containerRef.current.getBoundingClientRect();

      let top = containerRect.top - tooltipRect.height - 10;
      let left = containerRect.left + (containerRect.width / 2) - (tooltipRect.width / 2);

      // Adjust position to stay within the viewport
      if (top < 0) {
        top = containerRect.bottom + 10;
      }

      if (left < 0) {
        left = 10;
      } else if (left + tooltipRect.width > window.innerWidth) {
        left = window.innerWidth - tooltipRect.width - 10;
      }

      setPosition({ top, left });
    }
  }, [visible]);

  return (
    <div
      className="tooltip-container"
      ref={containerRef}
      onMouseEnter={() => setVisible(true)}
      onMouseLeave={() => setVisible(false)}
    >
      {children}
      {visible && (
        <div
          className="tooltip-message"
          style={{ top: `${position.top}px`, left: `${position.left}px` }}
          ref={tooltipRef}
        >
          {message}
        </div>
      )}
    </div>
  );
};

export default Tooltip;
