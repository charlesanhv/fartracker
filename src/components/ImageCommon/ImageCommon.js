import React, { useState } from 'react';

const ImageCommon = ({
  src,
  alt,
  width = 16,
  height = 16,
  type,
  borderRadius,
  imageError = '/assets/images/empty-token.svg',
  loadingSrc = '/assets/images/loading-spinner.svg', // Default loading image
}) => {
  const [currentSrc, setCurrentSrc] = useState(loadingSrc);

  const handleLoad = () => {
    setCurrentSrc(
      type === 'logo_token' ? `https://dd.dexscreener.com${src}` : src
    );
  };

  const handleError = (e) => {
    e.target.onerror = null; // Prevent infinite loop if fallback also fails
    e.target.src = imageError;
  };

  return (
    <img
      alt={alt}
      loading="lazy"
      className="fadeIn"
      src={src ? currentSrc : loadingSrc}
      style={{
        borderRadius: borderRadius,
        height: height,
        width: width,
      }}
      onLoad={handleLoad}
      onError={handleError}
    />
  );
};

export default ImageCommon;
