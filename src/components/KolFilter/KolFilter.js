import { KOL_OPTIONS } from "constants/trending_action_constants";
import { useDispatch, useSelector } from "react-redux";
import { actionSetKolOptions } from "../../redux/homeReducer";
import "./KolFilter.css";
import useOpen from "hooks/useOpen";
import { useRef } from "react";
import useClickOutside from "hooks/useClickOutSite";
const KolFilter = () => {
  const { toggle, isShowing, close } = useOpen();
  const kol_btn_ref = useRef(null);
  const popup_kol_ref = useRef(null);

  const kol_options = useSelector((state) => state.home.kol_options);

  const dispatch = useDispatch();
  const handleChooseKol = (name) => {
    if (kol_options.includes(name)) {
      dispatch(
        actionSetKolOptions(kol_options.filter((item) => item !== name))
      );
      return;
    }
    dispatch(actionSetKolOptions([...kol_options, name]));
  };

  const handleClearAllKol = () => {
    dispatch(actionSetKolOptions([]));
  };

  useClickOutside(popup_kol_ref, kol_btn_ref, close);
  return (
    <>
      <div className="KolFilter" style={{ position: "relative" }}>
        <div
          className="new-header-filter-btn kol-btn"
          onClick={() => toggle()}
          ref={kol_btn_ref}
          style={isShowing ? { backgroundColor: "#6146D733" } : {}}
        >
          {kol_options.length === 0 ? (
            <img
              src={
                !isShowing
                  ? "/assets/icons/icon_Kol.svg"
                  : "/assets/icons/icon_Kol_activate.svg"
              }
              alt="img"
              width="16px"
              height="16px"
            />
          ) : kol_options.length === 1 ? (
            <div className={`check-box isBoxChecked`}>
              <img
                src="/assets/icons/check_box_tick.svg"
                width="12px"
                height="8px"
                alt=""
              />
            </div>
          ) : (
            <div className={`check-box isBoxChecked`}>
              <img
                src="/assets/icons/horizonal_vector.svg"
                width="10px"
                alt=""
              />
            </div>
          )}
          <span
            className="new-header-text new-header-btn-text"
            style={isShowing ? { color: "#B49EF2" } : {}}
          >
            KOL
          </span>
        </div>

        {isShowing ? (
          <div className="popup-kol" ref={popup_kol_ref}>
            <div className="d-flex flex-column w-100" style={{ gap: "12px" }}>
              <div className="popup-kol-header">
                <span className="new-header-text popup-kol-header-text">
                  KOL
                </span>
                <div
                  className="kol-clear-all-btn"
                  onClick={() => handleClearAllKol()}
                >
                  <span className="new-header-text kol-clear-all-btn-text">
                    Clear All
                  </span>
                </div>
              </div>

              <div className="d-flex flex-column">
                {KOL_OPTIONS.map((item, index) => (
                  <div
                    className="popup-kol-item"
                    key={index}
                    onClick={() => handleChooseKol(item.value)}
                  >
                    <div
                      className="d-flex align-items-center justify-center"
                      style={{ width: "24px", height: "24px" }}
                    >
                      <div
                        className={`check-box ${
                          kol_options?.includes(item.value)
                            ? "isBoxChecked"
                            : ""
                        }`}
                      >
                        {kol_options?.includes(item.value) ? (
                          <img
                            src={
                              kol_options?.includes(item.value)
                                ? "/assets/icons/check_box_tick.svg"
                                : ""
                            }
                            width="12px"
                            height="8px"
                            alt=""
                          />
                        ) : null}
                      </div>
                    </div>
                    <span className="new-header-text popup-kol-text">
                      {item.label}
                    </span>
                  </div>
                ))}
              </div>
            </div>
          </div>
        ) : null}
      </div>
    </>
  );
};

export default KolFilter;
