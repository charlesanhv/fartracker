import { executeREST } from "lib/request";

// fetch data from API
export const fetchTransactions = (args = {}) => {
  // Constructing the query string based on args
  const queryParams = new URLSearchParams();
  let url = "";
  if (args?.limit) {
    queryParams.append("limit", args.limit.toString());
    url = `transaction?${queryParams}`;
  } else {
    url = "transaction";
  }
  return executeREST(url, {
    method: "GET",
    revalidate: 60,
    withAuth: false,
  });
};

export const fetchTransactionsWhale = (args = {}) => {
  // Constructing the query string based on args
  const queryParams = new URLSearchParams();
  let url = "";
  if (args?.skip || args?.take) {
    queryParams.append("skip", args.skip.toString());
    queryParams.append("take", args.take.toString());
    url = `/api/swap-by-whale?${queryParams}`;
  } else {
    url = "/api/swap-by-whale";
  }
  return executeREST(url, {
    method: "GET",
    revalidate: 60,
    withAuth: false,
  });
};

export const fetchTransactionsPowerPadge = (args = {}) => {
  // Constructing the query string based on args
  const queryParams = new URLSearchParams();
  let url = "";
  if (args?.skip || args?.take) {
    queryParams.append("skip", args.skip.toString());
    queryParams.append("take", args.take.toString());
    url = `/api/swap-by-power-badge-user?${queryParams}`;
  } else {
    url = "/api/swap-by-power-badge-user";
  }
  return executeREST(url, {
    method: "GET",
    revalidate: 60,
    withAuth: false,
  });
};

export const fetchTransactionsByUser = (args = {}) => {
  // Constructing the query string based on args
  const queryParams = new URLSearchParams();
  let url = "";
  if (args?.skip || args?.take || args?.type || args?.username) {
    queryParams.append("skip", args.skip.toString());
    queryParams.append("take", args.take.toString());
    queryParams.append("type", args.type.toString());
    queryParams.append("username", args.username.toString());
    url = `/api/action-by-user?${queryParams}`;
  } else {
    url = "/api/action-by-user";
  }
  return executeREST(url, {
    method: "GET",
    revalidate: 60,
    withAuth: false,
  });
};
