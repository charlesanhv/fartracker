import Web3 from "web3";
import moment, { max } from "moment";
import "moment-duration-format";
import { IS_EMPTY_DATA } from "constants";
import { filterTypes } from "constants/trending_action_constants";

export const formatBalance = (balance) => {
  if (!balance) return "0.00";
  const web3 = new Web3(Web3.givenProvider);
  const balanceInEth = web3.utils.fromWei(balance.toString(), "ether");
  return balanceInEth.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

export const formatTimestamp = (timestamp) => {
  const duration = moment.duration(Date.now() - timestamp);
  const days = Math.abs(Math.floor(duration.asDays()));
  const hours = Math.abs(Math.floor(duration.asHours())) % 24;
  const minutes = Math.abs(duration.minutes());

  // Xây dựng chuỗi thời gian định dạng
  let formattedDuration = "";

  if (days > 0) {
    formattedDuration += `${days}d `;
  }

  if (hours > 0) {
    formattedDuration += `${hours}h `;
  } else if (minutes > 0) {
    formattedDuration += `${minutes}m `;
  } else {
    const seconds = Math.abs(duration.seconds());
    formattedDuration += `${seconds}s `;
  }

  return formattedDuration.trim();
};
// Function to increment time from the initial timestamp
export const checkImageUrl = async (url) => {
  try {
    const response = await fetch(url, { method: "HEAD" });
    if (response.ok) {
      return url;
    }
    throw new Error("Image not found");
  } catch (error) {
    return null;
  }
};

export function isNumber(value) {
  return typeof value === "number" && Number.isFinite(value);
}

// export function bigIntFromParts(parts) {
//   if (isNumber(parts)) {
//     return parts;
//   }
//   const low = BigInt(parts.low);
//   const high = BigInt(parts.high);
//   const result = (high << 32n) + low;

//   return parts.unsigned ? result : BigInt.asIntN(64, result);
// }

export const parseAmount = (amount, fixed = 1) => {
  if (amount === 0) return 0;
  if (!amount || isNaN(amount)) return IS_EMPTY_DATA;
  return Number(amount.toFixed(fixed)).toLocaleString("en-US", {
    maximumFractionDigits: fixed,
  });
};

export function truncateString(str, maxLength, type = "end") {
  // Convert to string if necessary
  if (isNaN(str) && !String(str)) {
    return 0.0;
  }
  const strConverted = str.toString();

  // Check if the string length is less than or equal to the maxLength
  if (strConverted.length <= maxLength) {
    return strConverted;
  }

  if (type === "center") {
    const halfLength = Math.floor((maxLength - 3) / 2);
    return (
      strConverted.substring(0, halfLength) +
      "..." +
      strConverted.substring(strConverted.length - halfLength)
    );
  } else {
    // Default truncation at the end
    return strConverted.substring(0, maxLength) + "...";
  }
}

export const filterByDataType = (dataType) => {
  if (dataType === 'Mint') {
    return filterTypes.filter(item => item.type === 'vip' || item.type === 'user');
  }
  return filterTypes;
};