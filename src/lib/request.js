const getCookies = () => {
    return document.cookie.split(';').reduce((cookies, cookie) => {
        const [name, value] = cookie.split('=').map(c => c.trim());
        cookies[name] = value;
        return cookies;
    }, {});
};

const clearAllCookies = () => {
    document.cookie.split(';').forEach(cookie => {
        const [name] = cookie.split('=');
        document.cookie = `${name}=;expires=${new Date(0).toUTCString()};path=/`;
    });
};

const apiUrl = process.env.REACT_APP_API_URL;

export async function executeREST(endpoint, options = {}) {
    const {
        method = 'GET',  // Default method to GET if not specified
        headers = {},
        body,
        cache = 'no-cache', // Default cache policy
        revalidate,
        withAuth = true
    } = options;

    let authHeaders = {};
    if (withAuth) {
        const cookies = getCookies();
        const token = cookies['token']; // Replace 'token' with your actual cookie name
        if (token) {
            authHeaders = { Authorization: `Bearer ${token}` };
        }
    }

    const input = {
        method,
        headers: {
            "Content-Type": "application/json",
            ...headers,
            ...authHeaders
        },
        body: body ? JSON.stringify(body) : null,
        cache,
        ...revalidate && { next: { revalidate } }
    };

    const url = `${apiUrl}${endpoint}`;
    const response = await fetch(url, input);

    if (response.status === 401) {
        console.error('Unauthorized request - logging out');
        clearAllCookies();
        // Uncomment below to redirect to login page or implement similar functionality
        // window.location.href = '/login';
        throw new UnauthorizedError(response);
    }

    if (!response.ok) {
        const errorBody = await response.text();
        console.error(errorBody);
        throw new HTTPError(response, errorBody);
    }

    const responseBody = await response.json();

    if ("error" in responseBody) {
        throw new RESTError(responseBody);
    }

    return responseBody;
}

class RESTError extends Error {
    constructor(errorResponse) {
        super(errorResponse?.error);
        this.name = "RESTError";
        Object.setPrototypeOf(this, new.target.prototype);
    }
}

class HTTPError extends Error {
    constructor(response, body) {
        super(`HTTP error ${response?.status}: ${response?.statusText}\n${body}`);
        this.name = "HTTPError";
        Object.setPrototypeOf(this, new.target.prototype);
    }
}

class UnauthorizedError extends Error {
    constructor(response) {
        super(`Unauthorized error ${response?.status}: ${response?.statusText}`);
        this.name = "UnauthorizedError";
        Object.setPrototypeOf(this, new.target.prototype);
    }
}
