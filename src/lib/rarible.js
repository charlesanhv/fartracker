export const searchContract = async (contractValue) => {
    if (['', undefined, null].includes(contractValue)) return []
    const listResultId = await getIdsListSearch(contractValue)
    const listData = await getResultByIDs(listResultId)

    const convertImageMediaUrl = (imagesMedia = []) => {
        if (imagesMedia && imagesMedia.length === 0) return
        return imagesMedia.find(image => image.cropMode === "THUMB").url
    }
    // console.log('---> listData', listData)
    return listData.map((item) => ({
        id: item.id,
        name: item.name,
        image: convertImageMediaUrl(item.imageMedia)
    }))
}

async function getResultByIDs(listIds = []) {
    if (listIds.length === 0) return []
    const res = await fetch("https://rarible.com/marketplace/api/v4/collections/byIds", {
        method: "POST",
        body: JSON.stringify(listIds),
        headers: {
            "Content-Type": "application/json"
        }
    })
    const data = await res.json()
    return data
}
async function getIdsListSearch(valueSearch) {
    const res = await fetch("https://rarible.com/marketplace/search/v1/suggest/completion/collections?size=10", {
        method: "POST",
        body: JSON.stringify({
            text: valueSearch,
            verifiedOnly: false
        }),
        headers: {
            "Content-Type": "application/json"
        }
    })
    const resData = await res.json()
    return resData.map(item => item.id)
}