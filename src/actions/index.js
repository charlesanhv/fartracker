import SimpleCrypto from "simple-crypto-js";
import * as CONSTANTS from "../constants";
import { SWAP_ROUTE_PATH, BASE_TOKEN, BASE_FEE_V3 } from "../constants";

import { ethers } from "ethers";

export const asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};
export const sleep = async (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}
export const EncryptAPI = {
    encode: function (data, lock_key) {
        var encryptObj = new SimpleCrypto(lock_key ?? CONSTANTS.LOCAL_STORAGE.DEFAULT_ENCRYPT_KEY);
        return encryptObj.encrypt(data);
    },
    decode: function (enData, lock_key) {
        var encryptObj = new SimpleCrypto(lock_key ?? CONSTANTS.LOCAL_STORAGE.DEFAULT_ENCRYPT_KEY);
        return encryptObj.decrypt(enData);
    }
}
export const StorageAPI = {
    save: function (key, data, lock_key = null) {
        var enData = EncryptAPI.encode(data, lock_key);
        localStorage.setItem(key, enData);
    },
    load: function (key, lock_key = null) {
        var enData = localStorage.getItem(key);
        if (enData != null) {
            return EncryptAPI.decode(enData, lock_key);
        }
        return false;
    },
    remove: function (key) {
        localStorage.removeItem(key);
    },
    removeAll: function () {
        localStorage.removeAll();
    }
}
export const CookieAPI = {

}
export const NetworkAPI = {

}

function removeMatchingElements(arr) {
    if (arr.length < 3) {
        return arr;
    }

    const firstValue = arr[0].toLowerCase();
    const lastValue = arr[arr.length - 1].toLowerCase();

    const newArr = arr.filter((element) => element.toLowerCase() != firstValue && element.toLowerCase() != lastValue);
    newArr.unshift(firstValue);
    newArr.push(lastValue);
    return newArr;
}
export const BlockchainAPI = {
    getStableCryptoPrice: async () => {
        try {
            const url = 'https://pro-api.coingecko.com/api/v3/simple/price?x_cg_pro_api_key=CG-1JSSwT9Dgb4VBmM4t87RQj6m&vs_currencies=usd&include_24hr_change=true&include_24hr_vol=true&include_market_cap=true&ids=ethereum,dai';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                return {
                    eth: data?.ethereum?.usd,
                    dai: data?.dai?.usd
                }
            }
        } catch (error) {

        }
        return false;

    },
    uniswap: {
        generateSwapPathV3: function (arrayTokenAndFee) {
            var layer1Arr = [];
            for (let index = 0; index < arrayTokenAndFee.length; index++) {
                layer1Arr.push(ethers.utils.arrayify(arrayTokenAndFee[index]));
                if (index < arrayTokenAndFee.length - 1) {
                    layer1Arr.push(ethers.utils.zeroPad(ethers.utils.hexlify(BASE_FEE_V3), 24 / 8));
                }
            }
            var layer2Arr = ethers.utils.concat(layer1Arr);
            // return layer2Arr;
            return ethers.utils.hexlify(layer2Arr);
        },
        createPossiblePath: function (token1_address, token2_address) {
            var routerPathList = [];
            for (let index = 0; index < SWAP_ROUTE_PATH["BASE_MAINNET"].length; index++) {
                var routerPath = Object.assign([], SWAP_ROUTE_PATH["BASE_MAINNET"][index]);
                routerPath.unshift(token1_address);
                routerPath.push(token2_address);
                console.log(routerPath)
                routerPathList.push(removeMatchingElements(routerPath));
            }
            routerPathList = Array.from(new Set(routerPathList.map(JSON.stringify)), JSON.parse);
            return routerPathList;
        },
        createPossiblePathV3: function (pathV2) {
            var routerPathList = [];
            for (let index = 0; index < pathV2.length; index++) {
                routerPathList.push(this.uniswap.generateSwapPathV3(pathV2[index]));
            }
            return routerPathList;
        }
    }

}