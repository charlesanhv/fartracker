const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = function(app) {
  app.use(
    '/ds-data',
    createProxyMiddleware({
      target: 'https://dd.dexscreener.com',
      changeOrigin: true,
      pathRewrite: {
        '^/ds-data': '/ds-data',
      },
    })
  );
};
