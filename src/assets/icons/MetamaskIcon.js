import { SvgContainer } from "components/icons/SvgContainer";
import React from "react";

export function MetamaskIcon(props) {
  return (
    <SvgContainer size={40} fill="none" {...props}>
      <path fill="#F7F9FB" d="M0 0H40V40H0z"></path>
      <path
        fill="#E2761B"
        stroke="#E2761B"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="0.107"
        d="M32.688 6.667l-10.633 7.896 1.967-4.659 8.666-3.237z"
      ></path>
      <path
        fill="#E4761B"
        stroke="#E4761B"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="0.107"
        d="M8.143 6.667l10.547 7.971-1.87-4.734-8.677-3.237zm20.72 18.304L26.03 29.31l6.059 1.667 1.742-5.909-4.97-.096zm-21.842.096l1.731 5.91 6.06-1.668-2.832-4.338-4.959.096z"
      ></path>
      <path
        fill="#E4761B"
        stroke="#E4761B"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="0.107"
        d="M14.47 17.641l-1.689 2.554 6.016.267-.213-6.465-4.114 3.644zm11.893 0l-4.168-3.719-.139 6.54 6.006-.267-1.7-2.554zM14.812 29.31l3.611-1.763-3.12-2.437-.491 4.2zm7.597-1.763l3.623 1.763-.503-4.2-3.12 2.437z"
      ></path>
      <path
        fill="#D7C1B3"
        stroke="#D7C1B3"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="0.107"
        d="M26.032 29.31l-3.623-1.764.289 2.362-.032.994 3.366-1.593zm-11.22 0l3.366 1.592-.022-.994.267-2.362-3.611 1.764z"
      ></path>
      <path
        fill="#233447"
        stroke="#233447"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="0.107"
        d="M18.232 23.55l-3.013-.887 2.126-.973.887 1.86zm4.37 0l.887-1.86 2.137.973-3.024.887z"
      ></path>
      <path
        fill="#CD6116"
        stroke="#CD6116"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="0.107"
        d="M14.81 29.31l.514-4.34-3.345.097 2.832 4.242zm10.708-4.34l.513 4.34 2.831-4.243-3.344-.096zm2.543-4.776l-6.006.268.556 3.088.887-1.86 2.137.973 2.426-2.469zm-12.844 2.469l2.137-.973.876 1.86.566-3.088-6.016-.268 2.437 2.469z"
      ></path>
      <path
        fill="#E4751F"
        stroke="#E4751F"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="0.107"
        d="M12.781 20.194l2.522 4.916-.085-2.447-2.437-2.469zm12.855 2.469l-.107 2.447 2.533-4.916-2.426 2.469zm-6.839-2.201l-.566 3.088.705 3.643.16-4.797-.299-1.934zm3.26 0l-.289 1.923.128 4.808.716-3.643-.555-3.088z"
      ></path>
      <path
        fill="#F6851B"
        stroke="#F6851B"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="0.107"
        d="M22.613 23.55l-.716 3.643.513.353 3.12-2.436.107-2.447-3.024.886zm-7.394-.887l.085 2.447 3.12 2.436.513-.353-.705-3.644-3.013-.886z"
      ></path>
      <path
        fill="#C0AD9E"
        stroke="#C0AD9E"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="0.107"
        d="M22.666 30.902l.032-.994-.268-.235h-4.028l-.246.236.022.993-3.366-1.592 1.175.962 2.383 1.656h4.092l2.394-1.656 1.176-.962-3.366 1.592z"
      ></path>
      <path
        fill="#161616"
        stroke="#161616"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="0.107"
        d="M22.409 27.547l-.513-.353h-2.96l-.513.353-.267 2.361.246-.235h4.028l.268.235-.289-2.361z"
      ></path>
      <path
        fill="#763D16"
        stroke="#763D16"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="0.107"
        d="M33.137 15.076l.908-4.36-1.357-4.05-10.28 7.63 3.954 3.345 5.589 1.635 1.24-1.443-.535-.385.855-.78-.662-.513.854-.651-.566-.428zm-26.34-4.36l.908 4.36-.577.428.855.651-.652.513.855.78-.534.385 1.229 1.443 5.588-1.635 3.954-3.345-10.28-7.63-1.346 4.05z"
      ></path>
      <path
        fill="#F6851B"
        stroke="#F6851B"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="0.107"
        d="M31.951 19.276l-5.589-1.635 1.7 2.554-2.533 4.915 3.334-.042h4.969l-1.881-5.792zm-17.482-1.635l-5.588 1.635-1.86 5.792h4.958l3.324.042-2.522-4.915 1.688-2.554zm7.587 2.821l.353-6.165 1.624-4.392H16.82l1.603 4.392.374 6.165.128 1.945.01 4.787h2.96l.022-4.787.14-1.945z"
      ></path>
    </SvgContainer>
  );
}
