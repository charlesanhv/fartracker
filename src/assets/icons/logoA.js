import React from "react";

export function LogoA() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="18"
      height="20"
      fill="none"
      viewBox="0 0 18 20"
    >
      <path
        fill="url(#paint0_linear_2560_65882)"
        stroke="url(#paint1_linear_2560_65882)"
        d="M1.525 16.232l.354-.353-.354.353 2.243 2.243A3.5 3.5 0 006.243 19.5h5.514a3.5 3.5 0 002.475-1.025l2.243-2.243a3.5 3.5 0 001.025-2.475V4.243a3.5 3.5 0 00-1.025-2.475l-.243-.243-.353.354.353-.354A3.5 3.5 0 0013.757.5H4.243a3.5 3.5 0 00-2.475 1.025l-.243.243A3.5 3.5 0 00.5 4.243v9.514a3.5 3.5 0 001.025 2.475z"
      ></path>
      <path
        fill="url(#paint2_linear_2560_65882)"
        stroke="#232331"
        strokeWidth="0.6"
        d="M6.78 5.035h3.112l4.508 9h-3.845l.542-.683-.414-.925H6.27l-.478.925.542.683H3l4.093-8.323-.312-.677zm.128 5.752h3.08L8.464 7.569l-1.556 3.218z"
      ></path>
      <defs>
        <linearGradient
          id="paint0_linear_2560_65882"
          x1="1"
          x2="17"
          y1="1"
          y2="19"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#2C2C3D"></stop>
          <stop offset="0.498" stopColor="#1C1C29"></stop>
          <stop offset="1" stopColor="#602CB8"></stop>
        </linearGradient>
        <linearGradient
          id="paint1_linear_2560_65882"
          x1="1"
          x2="17.5"
          y1="1.5"
          y2="20"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#7B52CD"></stop>
          <stop offset="0.49" stopColor="#352F50"></stop>
          <stop offset="1" stopColor="#8C52FF"></stop>
        </linearGradient>
        <linearGradient
          id="paint2_linear_2560_65882"
          x1="2.011"
          x2="14.448"
          y1="9.232"
          y2="9.232"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#DA9EFF"></stop>
          <stop offset="1" stopColor="#874DFD"></stop>
        </linearGradient>
      </defs>
    </svg>
  );
}
