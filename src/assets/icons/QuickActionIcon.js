import React from "react";

function QuickActionIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="16"
      height="16"
      fill="none"
      viewBox="0 0 16 16"
    >
      <path
        fill="#8A66D8"
        d="M13.487 7.385a.5.5 0 00-.311-.354l-3.602-1.35.916-4.583a.5.5 0 00-.856-.44l-7 7.5a.5.5 0 00.19.81l3.602 1.35-.916 4.584a.5.5 0 00.856.439l7-7.5a.501.501 0 00.12-.456z"
      ></path>
    </svg>
  );
}

export default QuickActionIcon;
