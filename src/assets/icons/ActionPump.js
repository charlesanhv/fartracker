import React from "react";

export function ActionPump() {
  return (
    <svg
      fill="none"
      height="16"
      viewBox="0 0 17 16"
      width="17"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M3.28564 13L8.28564 8L13.2856 13"
        stroke="#58D073"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.2"
      />
      <path
        d="M3.28564 8L8.28564 3L13.2856 8"
        stroke="#58D073"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.2"
      />
    </svg>
  );
}


