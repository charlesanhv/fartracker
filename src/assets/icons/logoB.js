import React from "react";

export function LogoB() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="18"
      height="20"
      fill="none"
      viewBox="0 0 18 20"
    >
      <path
        fill="url(#paint0_linear_2560_65886)"
        stroke="url(#paint1_linear_2560_65886)"
        d="M1.525 16.232l.354-.353-.354.353 2.243 2.243A3.5 3.5 0 006.243 19.5h5.514a3.5 3.5 0 002.475-1.025l2.243-2.243a3.5 3.5 0 001.025-2.475V4.243a3.5 3.5 0 00-1.025-2.475l-.243-.243-.353.354.353-.354A3.5 3.5 0 0013.757.5H4.243a3.5 3.5 0 00-2.475 1.025l-.243.243A3.5 3.5 0 00.5 4.243v9.514a3.5 3.5 0 001.025 2.475z"
      ></path>
      <path
        fill="url(#paint2_linear_2560_65886)"
        stroke="#232331"
        strokeWidth="0.6"
        d="M7.625 10.47v1.828h1.892c.926 0 1.39-.287 1.39-.863 0-.643-.453-.965-1.359-.965H7.625zm1.86-1.549c.948 0 1.422-.273 1.422-.818 0-.673-.485-1.01-1.454-1.01H7.625v1.828h1.86zm.666-3.853c2.336 0 3.504.853 3.504 2.558 0 1.147-.5 1.826-1.498 2.038.999.212 1.498.944 1.498 2.196 0 1.6-1.168 2.4-3.504 2.4h-5.75l.539-.692v-7.68l-.54-.82h5.751z"
      ></path>
      <defs>
        <linearGradient
          id="paint0_linear_2560_65886"
          x1="9"
          x2="9"
          y1="1"
          y2="19"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#2C2C3D"></stop>
          <stop offset="1" stopColor="#1A1A26"></stop>
        </linearGradient>
        <linearGradient
          id="paint1_linear_2560_65886"
          x1="1"
          x2="17.5"
          y1="1.5"
          y2="20"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#4574A3"></stop>
          <stop offset="0.49" stopColor="#2F4250"></stop>
          <stop offset="1" stopColor="#4D6082"></stop>
        </linearGradient>
        <linearGradient
          id="paint2_linear_2560_65886"
          x1="3.598"
          x2="13.694"
          y1="9.355"
          y2="9.355"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#70CFEC"></stop>
          <stop offset="1" stopColor="#8997F4"></stop>
        </linearGradient>
      </defs>
    </svg>
  );
}
