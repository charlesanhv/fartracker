export function ArowLeftDisabled() {
  return (
    <svg
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g id="Icon" opacity="0.2">
        <rect width="20" height="20" fill="black" fillOpacity="0.01" />
        <path
          id="stroke"
          d="M12.5 5L7.5 10L12.5 15"
          stroke="white"
          strokeWidth="1.4"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </g>
    </svg>
  );
}
