import React from "react";
import "./icon.css";

function SwapIcon({rotate = false}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      fill="none"
      viewBox="0 0 32 32"
      className={rotate ? "rotate" : ""}
    >
      <path stroke="#fff" d="M22.02 12.464h6v-6"></path>
      <path stroke="#fff" d="M22.02 12.464h6v-6"></path>
      <path stroke="#fff" d="M22.02 12.464h6v-6"></path>
      <path stroke="#fff" d="M22.02 12.464h6v-6"></path>
      <path
        stroke="#fff"
        d="M8.222 8.222a11 11 0 0115.556 0l4.243 4.242"
      ></path>
      <path
        stroke="#fff"
        d="M8.222 8.222a11 11 0 0115.556 0l4.243 4.242"
      ></path>
      <path
        stroke="#fff"
        d="M8.222 8.222a11 11 0 0115.556 0l4.243 4.242"
      ></path>
      <path
        stroke="#fff"
        d="M8.222 8.222a11 11 0 0115.556 0l4.243 4.242M9.98 19.536h-6v6"
      ></path>
      <path stroke="#fff" d="M9.98 19.536h-6v6"></path>
      <path stroke="#fff" d="M9.98 19.536h-6v6"></path>
      <path stroke="#fff" d="M9.98 19.536h-6v6"></path>
      <path
        stroke="#fff"
        d="M23.778 23.778a11.001 11.001 0 01-15.556 0l-4.243-4.242"
      ></path>
      <path
        stroke="#fff"
        d="M23.778 23.778a11.001 11.001 0 01-15.556 0l-4.243-4.242"
      ></path>
      <path
        stroke="#fff"
        d="M23.778 23.778a11.001 11.001 0 01-15.556 0l-4.243-4.242"
      ></path>
      <path
        stroke="#fff"
        d="M23.778 23.778a11.001 11.001 0 01-15.556 0l-4.243-4.242"
      ></path>
    </svg>
  );
}

export default SwapIcon;
