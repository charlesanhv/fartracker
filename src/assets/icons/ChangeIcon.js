import React from "react";

function ChangeIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="33"
      height="32"
      fill="none"
      viewBox="0 0 33 32"
    >
      <path
        fill="#C4C6CD"
        d="M26.499 4h-20a2.002 2.002 0 00-2 2v20a2.002 2.002 0 002 2h20a2.002 2.002 0 002-2V6a2.002 2.002 0 00-2-2zM21.45 17.465l-4.243 4.24a1.003 1.003 0 01-1.416 0l-4.241-4.24a1 1 0 011.414-1.414l2.535 2.535V11a1 1 0 012 0v7.587l2.537-2.536a1 1 0 011.414 1.414z"
      ></path>
    </svg>
  );
}

export default ChangeIcon;
