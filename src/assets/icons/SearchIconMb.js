export function SearchIconMb() {
  return (
    <svg
      className="iconSearchConnectWallet"
      fill="none"
      height="24"
      viewBox="0 0 24 24"
      width="24"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M10.8748 18.75C15.224 18.75 18.7498 15.2242 18.7498 10.875C18.7498 6.52576 15.224 3 10.8748 3C6.52551 3 2.99976 6.52576 2.99976 10.875C2.99976 15.2242 6.52551 18.75 10.8748 18.75Z"
        stroke="white"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.6"
      />
      <path
        d="M16.4429 16.4438L20.9992 21.0002"
        stroke="white"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="1.6"
      />
    </svg>
  );
}
