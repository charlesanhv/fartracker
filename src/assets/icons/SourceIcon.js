import React from "react";

function SourceIcon({color = "#2869E6"}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="20"
      fill="none"
      className="injected-svg"
      data-src="/assets/images/linksource_fun.svg"
      viewBox="0 0 21 21"
    >
      <path
        fill={color}
        d="M4.792 18.008c-.459 0-.851-.163-1.177-.49a1.605 1.605 0 01-.49-1.177V4.674c0-.458.163-.85.49-1.177.326-.326.718-.49 1.177-.49h5c.236 0 .434.08.593.24.16.16.24.358.24.594s-.08.434-.24.594a.806.806 0 01-.593.24h-5V16.34h11.666v-5c0-.236.08-.434.24-.594.16-.16.358-.24.594-.24s.434.08.593.24c.16.16.24.358.24.594v5c0 .458-.163.85-.49 1.177-.326.327-.718.49-1.177.49H4.792zM16.458 5.84l-7.166 7.167a.79.79 0 01-.584.229.79.79 0 01-.583-.23.79.79 0 01-.23-.582.79.79 0 01.23-.584l7.167-7.167h-2.167a.806.806 0 01-.594-.24.806.806 0 01-.24-.593c0-.236.08-.434.24-.594.16-.16.358-.24.594-.24h4.167c.236 0 .434.08.593.24.16.16.24.358.24.594v4.167c0 .236-.08.434-.24.594a.806.806 0 01-.593.24.806.806 0 01-.594-.24.806.806 0 01-.24-.594V5.84z"
      ></path>
    </svg>
  );
}

export default SourceIcon;
