export function ArowRightEnabled({color="#FFF"}) {
  return (
    <svg
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g id="Icon">
        <rect width="20" height="20" fill="black" fillOpacity="0.01" />
        <path
          id="stroke"
          d="M7.5 5L12.5 10L7.5 15"
          // stroke="white"
          stroke={color}
          strokeWidth="1.4"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </g>
    </svg>
  );
}
