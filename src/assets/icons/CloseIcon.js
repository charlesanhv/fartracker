import React from "react";

function CloseIcon({color = '#626576'}) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      fill="none"
      viewBox="0 0 24 24"
    >
      <g clipPath="url(#clip0_1662_100205)">
        <path fill="#fff" fillOpacity="0.01" d="M0 0H24V24H0z"></path>
        <mask
          id="mask0_1662_100205"
          style={{ maskType: "alpha" }}
          width="24"
          height="24"
          x="0"
          y="0"
          maskUnits="userSpaceOnUse"
        >
          <path
            fill="#090A0B"
            d="M12 2.25A9.75 9.75 0 1021.75 12 9.761 9.761 0 0012 2.25zm3.53 12.22a.751.751 0 11-1.06 1.06L12 13.06l-2.47 2.47a.75.75 0 01-1.06-1.06L10.94 12 8.47 9.53a.75.75 0 011.06-1.06L12 10.94l2.47-2.47a.75.75 0 011.06 1.06L13.06 12l2.47 2.47z"
          ></path>
          <path
            fill="#000"
            fillOpacity="0.2"
            d="M12 2.25A9.75 9.75 0 1021.75 12 9.761 9.761 0 0012 2.25zm3.53 12.22a.751.751 0 11-1.06 1.06L12 13.06l-2.47 2.47a.75.75 0 01-1.06-1.06L10.94 12 8.47 9.53a.75.75 0 011.06-1.06L12 10.94l2.47-2.47a.75.75 0 011.06 1.06L13.06 12l2.47 2.47z"
          ></path>
          <path
            fill="#000"
            fillOpacity="0.2"
            d="M12 2.25A9.75 9.75 0 1021.75 12 9.761 9.761 0 0012 2.25zm3.53 12.22a.751.751 0 11-1.06 1.06L12 13.06l-2.47 2.47a.75.75 0 01-1.06-1.06L10.94 12 8.47 9.53a.75.75 0 011.06-1.06L12 10.94l2.47-2.47a.75.75 0 011.06 1.06L13.06 12l2.47 2.47z"
          ></path>
          <path
            fill="#000"
            fillOpacity="0.2"
            d="M12 2.25A9.75 9.75 0 1021.75 12 9.761 9.761 0 0012 2.25zm3.53 12.22a.751.751 0 11-1.06 1.06L12 13.06l-2.47 2.47a.75.75 0 01-1.06-1.06L10.94 12 8.47 9.53a.75.75 0 011.06-1.06L12 10.94l2.47-2.47a.75.75 0 011.06 1.06L13.06 12l2.47 2.47z"
          ></path>
        </mask>
        <g mask="url(#mask0_1662_100205)">
          <path fill={color} d="M0 0H24V24H0z"></path>
        </g>
      </g>
      <defs>
        <clipPath id="clip0_1662_100205">
          <path fill="#fff" d="M0 0H24V24H0z"></path>
        </clipPath>
      </defs>
    </svg>
  );
}

export default CloseIcon;
