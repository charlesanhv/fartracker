import React from "react";

export function ActionDump() {
    return (
        <svg
            fill="none"
            height="17"
            viewBox="0 0 16 17"
            width="16"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M13 8.5L8 13.5L3 8.5"
                stroke="#F95E5E"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="1.2"
            />
            <path
                d="M13 3.5L8 8.5L3 3.5"
                stroke="#F95E5E"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="1.2"
            />
        </svg>
    );
}


