import React from "react";

export function LogoS() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="18"
      height="20"
      fill="none"
      viewBox="0 0 18 20"
    >
      <path
        fill="url(#paint0_linear_2561_50318)"
        stroke="url(#paint1_linear_2561_50318)"
        d="M1.525 16.232l.354-.353-.354.353 2.243 2.243A3.5 3.5 0 006.243 19.5h5.514a3.5 3.5 0 002.475-1.025l2.243-2.243a3.5 3.5 0 001.025-2.475V3.828a2.5 2.5 0 00-.732-1.767l-.829-.829-.353.354.353-.354A2.5 2.5 0 0014.172.5H3.828a2.5 2.5 0 00-1.767.732l-.829.829A2.5 2.5 0 00.5 3.828v9.93a3.5 3.5 0 001.025 2.474z"
      ></path>
      <path
        fill="url(#paint2_linear_2561_50318)"
        stroke="#232331"
        strokeWidth="0.6"
        d="M13.285 5v3.179c-1.595-.707-2.987-1.06-4.177-1.06-.729 0-1.105.203-1.128.609 0 .41.962.893 2.885 1.449 1.946.697 2.92 1.69 2.92 2.98 0 2.033-1.37 3.05-4.11 3.05-1.07 0-2.353-.265-3.848-.794l-.759.827v-3.548c1.554.953 3.056 1.43 4.505 1.43.775 0 1.162-.222 1.162-.664 0-.42-.752-.85-2.256-1.292-1.253-.387-2.18-.875-2.782-1.463A2.857 2.857 0 015 7.783c0-.962.53-1.712 1.593-2.25.756-.332 1.672-.499 2.748-.499.816 0 1.875.221 3.179.663L13.285 5z"
      ></path>
      <defs>
        <linearGradient
          id="paint0_linear_2561_50318"
          x1="1"
          x2="16.5"
          y1="1"
          y2="20"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#2C2C3D"></stop>
          <stop offset="0.582" stopColor="#1C1C28"></stop>
          <stop offset="1" stopColor="#982163"></stop>
        </linearGradient>
        <linearGradient
          id="paint1_linear_2561_50318"
          x1="1"
          x2="17.5"
          y1="1.5"
          y2="20"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#A3459F"></stop>
          <stop offset="0.49" stopColor="#4B2F50"></stop>
          <stop offset="1" stopColor="#824D6A"></stop>
        </linearGradient>
        <linearGradient
          id="paint2_linear_2561_50318"
          x1="4.238"
          x2="13.821"
          y1="9.775"
          y2="9.775"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#ECB3DD"></stop>
          <stop offset="1" stopColor="#C7628C"></stop>
        </linearGradient>
      </defs>
    </svg>
  );
}
