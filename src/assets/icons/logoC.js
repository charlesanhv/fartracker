import React from "react";

export function LogoC() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="18"
      height="20"
      fill="none"
      viewBox="0 0 18 20"
    >
      <path
        fill="url(#paint0_linear_2560_65890)"
        stroke="url(#paint1_linear_2560_65890)"
        d="M1.525 16.232l.354-.353-.354.353 2.243 2.243A3.5 3.5 0 006.243 19.5h5.514a3.5 3.5 0 002.475-1.025l2.243-2.243a3.5 3.5 0 001.025-2.475V4.243a3.5 3.5 0 00-1.025-2.475l-.243-.243-.353.354.353-.354A3.5 3.5 0 0013.757.5H4.243a3.5 3.5 0 00-2.475 1.025l-.243.243A3.5 3.5 0 00.5 4.243v9.514a3.5 3.5 0 001.025 2.475z"
      ></path>
      <path
        fill="url(#paint2_linear_2560_65890)"
        stroke="#232331"
        strokeWidth="0.6"
        d="M13.27 13.613c-1.032.534-2.22.8-3.561.8-3.673 0-5.51-1.602-5.51-4.805 0-3.085 1.816-4.628 5.447-4.628.918 0 1.91.178 2.977.534l.647-.584v3.015c-1.172-.669-2.376-1.003-3.612-1.003-1.739 0-2.609.927-2.609 2.78 0 1.824.868 2.736 2.603 2.736 1.646 0 2.852-.212 3.618-.635v1.79z"
      ></path>
      <defs>
        <linearGradient
          id="paint0_linear_2560_65890"
          x1="9"
          x2="9"
          y1="1"
          y2="19"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#2C2C3D"></stop>
          <stop offset="1" stopColor="#1A261F"></stop>
        </linearGradient>
        <linearGradient
          id="paint1_linear_2560_65890"
          x1="1"
          x2="17.5"
          y1="1.5"
          y2="20"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#689981"></stop>
          <stop offset="0.49" stopColor="#2F5040"></stop>
          <stop offset="1" stopColor="#58826D"></stop>
        </linearGradient>
        <linearGradient
          id="paint2_linear_2560_65890"
          x1="3.412"
          x2="13.308"
          y1="9.352"
          y2="9.352"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#93CC9C"></stop>
          <stop offset="1" stopColor="#549977"></stop>
        </linearGradient>
      </defs>
    </svg>
  );
}
