import React from "react";

function SortIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="22"
      fill="none"
      viewBox="0 0 24 22"
    >
      <g clipPath="url(#clip0_1668_14423)">
        <path
          fill="#fff"
          fillOpacity="0.01"
          d="M0 0H12V11H0z"
          transform="translate(6)"
        ></path>
        <g clipPath="url(#clip1_1668_14423)">
          <path
            fill="#fff"
            fillOpacity="0.01"
            d="M0 0H12V12H0z"
            transform="translate(6 -.5)"
          ></path>
          <mask
            id="mask0_1668_14423"
            style={{ maskType: "alpha" }}
            width="12"
            height="13"
            x="6"
            y="-1"
            maskUnits="userSpaceOnUse"
          >
            <path
              fill="#fff"
              fillOpacity="0.01"
              d="M0 0H12V12H0z"
              transform="translate(6 -.5)"
            ></path>
            <path
              fill="#090A0B"
              d="M11.688 3.14L8.39 7.263a.3.3 0 00.234.487h6.752a.3.3 0 00.234-.487L12.312 3.14a.4.4 0 00-.624 0z"
            ></path>
          </mask>
          <g mask="url(#mask0_1668_14423)">
            <path fill="#626576" d="M6 -0.5H18V11.5H6z"></path>
          </g>
        </g>
      </g>
      <g clipPath="url(#clip2_1668_14423)">
        <path
          fill="#fff"
          fillOpacity="0.01"
          d="M0 0H12V11H0z"
          transform="translate(6 11)"
        ></path>
        <g clipPath="url(#clip3_1668_14423)">
          <path
            fill="#fff"
            fillOpacity="0.01"
            d="M0 0H12V12H0z"
            transform="translate(6 10.5)"
          ></path>
          <mask
            id="mask1_1668_14423"
            style={{ maskType: "alpha" }}
            width="12"
            height="13"
            x="6"
            y="10"
            maskUnits="userSpaceOnUse"
          >
            <path
              fill="#fff"
              fillOpacity="0.01"
              d="M0 0H12V12H0z"
              transform="translate(6 10.5)"
            ></path>
            <path
              fill="#090A0B"
              d="M11.688 18.86L8.39 14.737a.3.3 0 01.234-.487h6.752a.3.3 0 01.234.487l-3.298 4.123a.4.4 0 01-.624 0z"
            ></path>
          </mask>
          <g mask="url(#mask1_1668_14423)">
            <path fill="#626576" d="M6 10.5H18V22.5H6z"></path>
          </g>
        </g>
      </g>
      <defs>
        <clipPath id="clip0_1668_14423">
          <path
            fill="#fff"
            d="M0 4.8c0-1.68 0-2.52.327-3.162A3 3 0 011.638.327C2.28 0 3.12 0 4.8 0h14.4c1.68 0 2.52 0 3.162.327a3 3 0 011.311 1.311C24 2.28 24 3.12 24 4.8V11H0V4.8z"
          ></path>
        </clipPath>
        <clipPath id="clip1_1668_14423">
          <path
            fill="#fff"
            d="M0 0H12V12H0z"
            transform="translate(6 -.5)"
          ></path>
        </clipPath>
        <clipPath id="clip2_1668_14423">
          <path
            fill="#fff"
            d="M0 11h24v6.2c0 1.68 0 2.52-.327 3.162a3 3 0 01-1.311 1.311C21.72 22 20.88 22 19.2 22H4.8c-1.68 0-2.52 0-3.162-.327a3 3 0 01-1.311-1.311C0 19.72 0 18.88 0 17.2V11z"
          ></path>
        </clipPath>
        <clipPath id="clip3_1668_14423">
          <path
            fill="#fff"
            d="M0 0H12V12H0z"
            transform="translate(6 10.5)"
          ></path>
        </clipPath>
      </defs>
    </svg>
  );
}

export default SortIcon;
