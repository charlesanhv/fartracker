import React from "react";

function LoadingIcon() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="14"
            height="14"
            fill="none"
            viewBox="0 0 14 14"
        >
            <path fill="#fff" fillOpacity="0.01" d="M0 0H14V14H0z"></path>
            <path
                fill="#C4C6CD"
                fillRule="evenodd"
                d="M7 1.75a5.25 5.25 0 100 10.5 5.25 5.25 0 000-10.5zM.583 7a6.417 6.417 0 1112.834 0A6.417 6.417 0 01.583 7z"
                clipRule="evenodd"
                opacity="0.14"
            ></path>
            <path
                fill="#A3A5AE"
                fillRule="evenodd"
                d="M6.417 1.167c0-.323.261-.584.583-.584a6.417 6.417 0 11-4.537 10.954.583.583 0 01.825-.825A5.251 5.251 0 107 1.75a.583.583 0 01-.583-.583z"
                clipRule="evenodd"
            ></path>
        </svg>
    );
}

export default LoadingIcon;
