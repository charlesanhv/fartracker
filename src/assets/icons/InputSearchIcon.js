export function InputSearchIcon() {
    return (
        <svg xmlns="http://www.w3.org/2000/svg" width="7" height="10" viewBox="0 0 7 10" fill="none">
        <path d="M5.49355 0.5H6.5L1.50645 9.5H0.5L5.49355 0.5Z" fill="#BDBDC7"/>
      </svg>
    );
  }