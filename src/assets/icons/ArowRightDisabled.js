export function ArowRightDisabled() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="20"
      height="20"
      viewBox="0 0 20 20"
      fill="none"
    >
      <g opacity="0.2">
        <rect width="20" height="20" fill="black" fillOpacity="0.01" />
        <path
          d="M7.5 5L12.5 10L7.5 15"
          stroke="white"
          strokeWidth="1.4"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </g>
    </svg>
  );
}
