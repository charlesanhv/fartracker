import React from "react";

function DropDownIcon() {
    return (
        <svg
            fill="none"
            height="18"
            viewBox="0 0 18 18"
            width="18"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path
                d="M14.625 6.75L9 12.375L3.375 6.75"
                stroke="#F1F2F3"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="1.6"
            />
        </svg>
    );
}

export default DropDownIcon;
