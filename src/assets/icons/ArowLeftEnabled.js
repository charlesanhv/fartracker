export function ArowLeftEnabled({color="#fff"}) {
    return (
        <svg
        xmlns="http://www.w3.org/2000/svg"
        width="20"
        height="20"
        viewBox="0 0 20 20"
        fill="none"
      >
        <rect width="20" height="20" fill="black" fillOpacity="0.01" />
        <path
          d="M12.5 5L7.5 10L12.5 15"
          stroke={color}
          strokeWidth="1.4"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </svg>
    );
  }
  