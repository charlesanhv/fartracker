export function ClearIcon() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="16"
      height="16"
      viewBox="0 0 16 16"
      fill="none"
    >
      <g clip-path="url(#clip0_1970_18413)">
        <rect width="16" height="16" fill="black" fillOpacity="0.01" />
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M7.99984 15.3334C12.0499 15.3334 15.3332 12.0502 15.3332 8.00008C15.3332 3.94999 12.0499 0.666748 7.99984 0.666748C3.94975 0.666748 0.666504 3.94999 0.666504 8.00008C0.666504 12.0502 3.94975 15.3334 7.99984 15.3334ZM5.61888 4.66675L4.6665 5.61913L7.04753 8.00016L4.66665 10.381L5.61903 11.3334L7.99991 8.95254L10.3808 11.3334L11.3332 10.381L8.95229 8.00016L11.3333 5.61913L10.3809 4.66675L7.99991 7.04777L5.61888 4.66675Z"
          fill="#6A6A75"
        />
      </g>
      <defs>
        <clipPath id="clip0_1970_18413">
          <rect width="16" height="16" fill="white" />
        </clipPath>
      </defs>
    </svg>
  );
}
