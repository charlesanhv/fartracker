import React from "react";

export function LogoD() {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="18"
      height="20"
      fill="none"
      viewBox="0 0 18 20"
    >
      <path
        fill="url(#paint0_linear_2560_65894)"
        stroke="url(#paint1_linear_2560_65894)"
        d="M1.525 16.232l.354-.353-.354.353 2.243 2.243A3.5 3.5 0 006.243 19.5h5.514a3.5 3.5 0 002.475-1.025l2.243-2.243a3.5 3.5 0 001.025-2.475V4.243a3.5 3.5 0 00-1.025-2.475l-.243-.243-.353.354.353-.354A3.5 3.5 0 0013.757.5H4.243a3.5 3.5 0 00-2.475 1.025l-.243.243A3.5 3.5 0 00.5 4.243v9.514a3.5 3.5 0 001.025 2.475z"
      ></path>
      <path
        fill="url(#paint2_linear_2560_65894)"
        stroke="#232331"
        strokeWidth="0.6"
        d="M4 14.247l.571-.685V5.76L4 5.068h4.913c3.402 0 5.104 1.543 5.104 4.628 0 3.034-1.68 4.551-5.04 4.551H4zm3.313-1.898h1.308c1.697 0 2.545-.889 2.545-2.666 0-1.803-.87-2.704-2.608-2.704H7.313v5.37z"
      ></path>
      <defs>
        <linearGradient
          id="paint0_linear_2560_65894"
          x1="9"
          x2="9"
          y1="1"
          y2="19"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#2C2C3D"></stop>
          <stop offset="1" stopColor="#1A2326"></stop>
        </linearGradient>
        <linearGradient
          id="paint1_linear_2560_65894"
          x1="1"
          x2="17.5"
          y1="1.5"
          y2="20"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#487583"></stop>
          <stop offset="0.49" stopColor="#334044"></stop>
          <stop offset="1" stopColor="#487583"></stop>
        </linearGradient>
        <linearGradient
          id="paint2_linear_2560_65894"
          x1="3.131"
          x2="14.059"
          y1="9.349"
          y2="9.349"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#7CA6B3"></stop>
          <stop offset="1" stopColor="#64728C"></stop>
        </linearGradient>
      </defs>
    </svg>
  );
}
