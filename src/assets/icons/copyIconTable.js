import React from "react";

function CopyIconTable({color="#C4C6CD" }) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="12"
      height="13"
      fill="none"
      viewBox="0 0 12 13"
    >
      <path stroke={color} d="M10.312 8.75V2.187H3.75"></path>
      <path stroke={color} d="M8.437 4.062H1.874v6.563h6.563V4.062z"></path>
    </svg>
  );
}

export default CopyIconTable;
