import React from "react";

export const VipIcon = () => {
    return (
        <svg
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
        width="16"
        height="16"
        fill="none"
        viewBox="0 0 19 18"
    >
        <path fill="url(#pattern0_4375_7637)" d="M0.5 0H18.5V18H0.5z"></path>
        <defs>
            <pattern
                id="pattern0_4375_7637"
                width="1"
                height="1"
                patternContentUnits="objectBoundingBox"
            >
                <use transform="scale(.01563)" xlinkHref="#image0_4375_7637"></use>
            </pattern>
            <image
                id="image0_4375_7637"
                width="64"
                height="64"
                xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAABrJJREFUeF7VW01sE0cUfs/LBkpVGtSIiqpRE0RUlRInFj3BoQni0h5oOJFTk9g5IFWFRMJGXEjMBRFHSoKExCFOnFt64ufQXhDJBU5FdhzaChE1S1MVlabCRLS0xLuvnY3t7Dq73l3vzpLMybJn3rz3zZs337x5RuDcLp1KNwivhDYMQAsRNABgAxDVArLPmkYgAWIOgCRCmgPCTI0oZvqvfSTxVBF5CE/0zrcB0BekQMcGQ51PKAUAZglxKjrePOt8eOURngEw0rdYm195cQaQ+gig1mtFC/IkIIiLNeKsV57hGgCfDNfhiQC5/z0rtW2bOOYWCFcADEfmzxDQIMcVt3Ik1SNiE8GUVUez36sCYOTUTw351dVJAmirdmKPx0miKLZX4w2OAdgEq26GXY6I4ucmWkadgOsIgEQ4O0IIfU4m8LsvEg5GJ5rjdue1DcBw+MGkgkq3XcGvs18AIHU2Geyxo4MtAIZ659NA1GpH4Obpg5lYsjlkpY8lAFtp5cuNteMJFQFIhOcHCGnQCsXN/DsijEbHg/2Oj0EW7RUgRxF1swJBRP1mp4OhBxTO+bRfBOfQsTr45FgdPMo8hzvTT3jgmBNFMWTEEwwBGIpkFwHKbms81AKAg4d3w2fh90vSp+KP4OnSP57PhgCz0WSwvVzwBgCGwtluQJj0XAMDgXvqd0DXQJPulyunf4R//5a5TG+0FXQAMNdfXV2d8WP1364T4csLTbBjp1Ay9o+ll5CKL3AxviA0J8pvNfanGnPFSXQA+MX0mPEnz+6Dt+tqdMYupFfg+tXHPAEAQoqfG28pnWwlAPwKfGbGM6vvTP8G92//yRUAANB5QQkAP/b+9p0B6Izugz31bxgaOZ34GZYe/sUbAJ0XrAPgQ+T/PFwPHx82TxYleue5G1+MBbFkcDf7rALAcnhExIIft3bk+Ltw+PgeU/m/PHwB3yTY6etPQ8R2lmNUARiOZCcVAG43PSvjmQ73by/zIkEmiOJYLNncpwLAk/gwlne0c6/lst64+hgepVcs+3nYQYolg41YOPu5+F5TaBd0fPWBLZ15McBKk8ui3IjDkWyHAnDdlpYOOjGWdzK6T0d0zIYz5scYoO8tAD14uXduEAkHvJy80llvNI/fAXBdBxzDoUiWrX6HVwA4NZ7N++DeM3hw95ljFZjnuLs40U1M9M6nyaN0FyM6XReaNlBcx5Y5GDAz/QS+v73sYIRm/ZEyzAM8u/oyllf/4ZtVKVPtIJfbR2IAULWTa8cd7XwPDh17xwtRjmS4PT49AcAO0XFklc3Od2/9DvduPbXZ27ibawC2svEMElcAOCE6rpapbLCXtNlVENyCQa98HSRXx6Bdnu/V6q8sv4LUxQXPcoZYOAZdESFGebdr8np2jT14ZLeaEbbbmPHTw4vwfPmV3SE2+tFNHIrMjQLgGRu9Pe3iZPvwMR7UzBD6kQozQu7rKwdsX5SmE4vwdOmlpwvAhAUATuCl7nSDIAhcrsNmGhu9B5j1dUt0KqEmy3Ij94SIkQL7Q7vghI08Ad8sMUmxZEsRAH/jQHvnXvUtsFLzguVZ7JlULBns8S0pqlXGKgD6YDzokqJMuUQk+8yv1+DoeLPp4rDcwHcTv3oe8PQC19xfpcLFH3hkhoysYNdl5gFGjUV6FvF5PY6W5iToKdYWrj+Ndadr84KwyNsL2JWZXZ3LG6+z3gDonCzLofOpkFqErXsc9cMLWJaYXaK0zUfjmcnqe0Bxfv3zuA9e0DWwX/c2yNx96uKCxxTXLISQJMtKe3H1N3gA++JyeK4PEUd4RCF2Zzh95YBO9FR8gQvLM9Rfs/cNPaD4ZSKSneFRB1zOAL+dWIIf7pVqFXhgrpG5Hvm1ExnWCDF6vE0QuBRJMRLU1LpLfQv0oRZAtZWV1+c1gc8SAN5bgfNSbxBPSP3nxo2LqCsWSr6uq7KXAJWXxJTLtiyVTUTmUgTY5aVSfslCgKloMljx2d8SAKZsIjKXJsCtVSyNlImNt7gvll4/GbaOJ9hZ+YrHoJmL+sEU3W8PPdOzkmdrC2iFMKIUQBzgfWewUtwgmOWUtRpARwXejgFgE6+l0QIz6r9AN0FDhNl8Xu7RUly7alUFQFH4WkKVBl4XEIzgVLPqtoiQXQQL3tCHgF1+bYuC4WMv88roYCrkiku78gAtSCoQAaGNp0d4aXhVp4Bdr2CFlwop3QjwqQfbQ1KAbgoYuLGp/zxtBg7zDFEQWmWkVkRsQaIGImT1suUBVEKkHCFKikKPA4QZuUaePX9tLXPDq/0Hhaj1AUDDaBUAAAAASUVORK5CYII="
            ></image>
        </defs>
    </svg>
    );
}