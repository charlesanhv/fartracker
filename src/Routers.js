import { BrowserRouter, Route, Routes } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { useEffect } from "react";

// import * as actions from './actions'
import * as CONSTANTS from "./constants";
import DefaultLayout from "./layouts/Default";

import UserProfile from "./screens/Users/Profile";
import UserSetting from "./screens/Users/Setting";
import { BlockchainAPI, sleep } from "actions";
// const {Login} = lazy(() => import('./screens/Auth'));
// const Dashboard = lazy(() => import('./screens/Dashboard'));
// const Home = lazy(() => import('./screens/Home'));
// const UserProfile = lazy(() => import('./screens/Users/Profile'));
// const UserSetting = lazy(() => import('./screens/Users/Setting'));

import { Footer, Header, Loading } from "components";
import { Dashboard, Home, Login } from "screens";
import { PrivateRoute, RankRoute } from "components/Route";

// const allPublicRoutes = [
//     { path: CONSTANTS.ROUTE.NAVIGATE.LOGIN, component: <DefaultLayout Header={Header} Footer={Footer} Content={Login} /> },
//     { path: CONSTANTS.ROUTE.NAVIGATE.ROOT, component: <DefaultLayout Header={Header} Footer={Footer} Content={Home} /> },
// ]
const allGlobalRoutes = [
  {
    path: CONSTANTS.ROUTE.NAVIGATE.LOGIN,
    component: (
      <DefaultLayout Header={Header} Footer={Footer} Content={Login} />
    ),
  },
  {
    path: CONSTANTS.ROUTE.NAVIGATE.ROOT,
    component: <DefaultLayout Header={Header} Footer={Footer} Content={Home} />,
  },
  {
    path: CONSTANTS.ROUTE.NAVIGATE.MINT,
    component: <DefaultLayout Header={Header} Footer={Footer} Content={Home} />,
  },
  {
    path: CONSTANTS.ROUTE.NAVIGATE.FILTER_USER,
    component: <DefaultLayout Header={Header} Footer={Footer} Content={Home} />,
  },
  {
    path: CONSTANTS.ROUTE.NAVIGATE.MINT_FILTER_USER,
    component: <DefaultLayout Header={Header} Footer={Footer} Content={Home} />,
  },
];
const allPrivateRoutes = [
  {
    path: CONSTANTS.ROUTE.NAVIGATE.DASHBOARD,
    component: (
      <DefaultLayout Header={Header} Footer={Footer} Content={Dashboard} />
    ),
    rankRequire: CONSTANTS.ROUTE.RANK.REGISTER,
  },
  {
    path: CONSTANTS.ROUTE.NAVIGATE.USER,
    component: (
      <DefaultLayout Header={Header} Footer={Footer} Content={UserProfile} />
    ),
    // component: <Navigate to={ "/" + CONSTANTS.ROUTE.NAVIGATE.USER + "/" + CONSTANTS.ROUTE.NAVIGATE.USER_PROFILE} />,
    rankRequire: CONSTANTS.ROUTE.RANK.REGISTER,
    child: [
      {
        path: CONSTANTS.ROUTE.NAVIGATE.USER_PROFILE,
        component: (
          <DefaultLayout
            Header={Header}
            Footer={Footer}
            Content={UserProfile}
          />
        ),
        rankRequire: CONSTANTS.ROUTE.RANK.REGISTER,
        // child: [
        //     {
        //         path: ':todoId',
        //         component: <DefaultLayout  Header={Header}  Content={Todo} />,
        //     }
        // ]
      },
      {
        path: CONSTANTS.ROUTE.NAVIGATE.USER_SETTINGS,
        component: (
          <DefaultLayout
            Header={Header}
            Footer={Footer}
            Content={UserSetting}
          />
        ),
        rankRequire: CONSTANTS.ROUTE.RANK.REGISTER,
      },
    ],
  },
];

export default function Routers() {
  const dispatch = useDispatch();
  const { logged, rank } = useSelector(
    (state) => state.reducer.client_auth_info
  );
  const is_loading = useSelector((state) => state.reducer.is_loading);

  const preCheck = async () => {
    //load local to redux
    // if(window.ethereum != null){
    //     var web3Provider = new ethers.BrowserProvider(window.ethereum)
    //     var signer = await web3Provider.getSigner();
    // }
    while (true) {
      var data = await BlockchainAPI.getStableCryptoPrice();
      if (data != false) {
        dispatch({
          type: CONSTANTS.REDUX_ACTION.SET_STABLE_CRYPTO_PRICE,
          payload: { ...data, usd: 1 },
        });
      }
      await sleep(3000);
    }
  };
  //life cycle
  useEffect(() => {
    preCheck();
    // dispatch({type: CONSTANTS.REDUX_ACTION.SET_LOADING_STATE, payload: false});
  }, []);
  return is_loading ? (
    <Loading />
  ) : (
    <BrowserRouter>
      <Routes>
        <Route path={"*"} element={<Loading />} />
        {/* <Route element={<PublicRoute logged={logged} />} >
                    {
                        allPublicRoutes.map((ele, idx) => (
                            <Route key={"public" + idx} path={ele.path} element={ele.component} />
                        ))
                    }
                </Route> */}
        {allGlobalRoutes.map((ele, idx) => (
          <Route key={"global" + idx} path={ele.path} element={ele.component} />
        ))}
        <Route element={<PrivateRoute logged={logged} />}>
          {allPrivateRoutes.map((ele, idx) => (
            <Route
              key={"private" + idx}
              path={ele.path}
              element={
                <RankRoute rankRequire={ele.rankRequire} client_rank={rank} />
              }
            >
              <Route index element={ele.component} />
              {ele.child?.map((ele2, idx2) => (
                <Route
                  key={"private" + idx + idx2}
                  path={ele2.path}
                  element={
                    <RankRoute
                      rankRequire={ele2.rankRequire}
                      client_rank={rank}
                    />
                  }
                >
                  <Route index element={ele2.component} />
                  {ele2.child?.map((ele3, idx3) => (
                    <Route
                      key={"private" + idx + "" + idx2 + idx3}
                      element={
                        <RankRoute
                          rankRequire={ele3.rankRequire}
                          client_rank={rank}
                        />
                      }
                    >
                      <Route path={ele3.path} element={ele3.component} />
                    </Route>
                  ))}
                </Route>
              ))}
            </Route>
          ))}
        </Route>
      </Routes>
    </BrowserRouter>
  );
}
