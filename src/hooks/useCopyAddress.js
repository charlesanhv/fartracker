const useCopy = () => {

    function handleCopy(value, onClick) {
        navigator.clipboard.writeText(value);
        onClick && onClick();
    }

    return {
        handleCopy
    }
}

export default useCopy