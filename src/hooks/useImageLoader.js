import { checkImageUrl } from 'lib/util';
import { useState, useEffect } from 'react';

const useImageLoader = (url) => {
  const [image, setImage] = useState(null);

  useEffect(() => {
    const loadImage = async () => {
      const loadedImage = await checkImageUrl(url);
      setImage(loadedImage);
    };

    if (url) {
      loadImage();
    }
  }, [url]);

  return image;
};

export default useImageLoader;
