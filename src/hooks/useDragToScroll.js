import { useRef, useEffect } from 'react';

const useDragToScroll = () => {
    const componentRef = useRef(null);
    const isDragging = useRef(false);
    const startX = useRef(0);
    const scrollLeft = useRef(0);

    useEffect(() => {
        const componentContainer = componentRef.current;

        const handleMouseDown = (e) => {
            isDragging.current = true;
            startX.current = e.pageX - componentContainer.offsetLeft;
            scrollLeft.current = componentContainer.scrollLeft;
            componentContainer.style.cursor = 'grabbing';
        };

        const handleMouseLeaveOrUp = () => {
            isDragging.current = false;
            componentContainer.style.cursor = 'grab';
        };

        const handleMouseMove = (e) => {
            if (!isDragging.current) return;
            e.preventDefault();
            const x = e.pageX - componentContainer.offsetLeft;
            const walk = (x - startX.current) * 2; // Adjust scrolling speed as needed
            componentContainer.scrollLeft = scrollLeft.current - walk;
        };

        componentContainer.addEventListener('mousedown', handleMouseDown);
        componentContainer.addEventListener('mouseleave', handleMouseLeaveOrUp);
        componentContainer.addEventListener('mouseup', handleMouseLeaveOrUp);
        componentContainer.addEventListener('mousemove', handleMouseMove);

        return () => {
            componentContainer.removeEventListener('mousedown', handleMouseDown);
            componentContainer.removeEventListener('mouseleave', handleMouseLeaveOrUp);
            componentContainer.removeEventListener('mouseup', handleMouseLeaveOrUp);
            componentContainer.removeEventListener('mousemove', handleMouseMove);
        };
    }, []);

    return componentRef;
};

export default useDragToScroll;
