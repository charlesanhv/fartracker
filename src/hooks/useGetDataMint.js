import { useDispatch } from "react-redux"
import { actionSetDataMint } from "../redux/homeReducer";

const useGetDataMint = () => {

    const dispatch = useDispatch();

    const getDataMint = (data) => {
        if (data) {
            dispatch(actionSetDataMint(data))
        }
    }

    return { getDataMint }
}

export default useGetDataMint