const useClickOutSide = (ref,btnRef ,callback) => {
    const handleClick = (e) => {
        if (ref.current && !ref.current.contains(e.target) && btnRef.current &&!btnRef.current.contains(e.target)) {
            callback();
        }
    };
    document.addEventListener("mousedown", handleClick);
    return () => {
        document.removeEventListener("mousedown", handleClick);
    };
}

export default useClickOutSide