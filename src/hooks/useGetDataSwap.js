import { useDispatch } from "react-redux"
import { actionSetDataSwap } from "../redux/homeReducer";

const useGetDataSwap = () => {

    const dispatch = useDispatch();

    const getDataSwap = (data) => {
        if (data) {
            dispatch(actionSetDataSwap(data))
        }
    }

    return { getDataSwap }
}

export default useGetDataSwap