import { useState } from 'react';

const useCopy = () => {
  const [isCopied, setIsCopied] = useState(false);

  const copyToClipboard = async (textToCopy) => {
    try {
      // Check if the document is focused
      if (!document.hasFocus()) {
        console.error('Failed to copy text: Document is not focused');
        return;
      }

      await navigator.clipboard.writeText(textToCopy);
      setIsCopied(true);
      setTimeout(() => setIsCopied(false), 2000); // Reset icon after 2 seconds
    } catch (error) {
      console.error('Failed to copy text: ', error);
      setIsCopied(false);
    }
  };

  return [isCopied, copyToClipboard];
};

export default useCopy;
