import { useEffect, useState } from "react"

const useShowNoti = (isShow = false) => {
    const [isShowing, setIsShowing] = useState(isShow)

    function openNoti() {
        setIsShowing(true)
    }

    useEffect(() => {
        if (isShowing) {
            const showNoti = setTimeout(() => {
                setIsShowing(false)
            }, 3000)
            return () => {
                clearTimeout(showNoti)
            }
        }
    }, [isShowing])

    return {
        isShowing,
        openNoti
    }
}

export default useShowNoti