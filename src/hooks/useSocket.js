import { useEffect, useState } from 'react';
import { io } from 'socket.io-client';

const useWebSocket = (url) => {
  const [messages, setMessages] = useState([]);
  const [messagesMint, setMessagesMint] = useState([]);
  const [isConnected, setIsConnected] = useState(false);
  const [isConnecting, setIsConnecting] = useState(false);
  const [socket, setSocket] = useState(null);

  useEffect(() => {
    setIsConnecting(true);
    const socketClient = io(url);
    setSocket(socketClient);

    socketClient.on('connect', () => {
      setIsConnected(true);
      setIsConnecting(false);
    });

    socketClient.on('disconnect', () => {
      setIsConnected(false);
      setIsConnecting(false);
    });

    socketClient.on('connect_error', () => {
      setIsConnecting(false);
      setIsConnected(false);
    });

    socketClient.on('SWAP_LIST', (data) => {
      // console.log(data)
      setMessages(data);
    });
    // socketClient.on('NEW_SWAP_ACTION', (data) => {
    //   setMessages((prevMessages) => [data, ...prevMessages]);
    // });
    socketClient.on('NEW_SWAPS_ACTION', (data) => {
      setMessages((prevMessages) => [...data, ...prevMessages]);
    });
    socketClient.on('MINT_LIST', (data) => {
      // console.log(data)
      setMessagesMint(data);
    });
    // socketClient.on('NEW_MINT_ACTION', (data) => {
    //   setMessagesMint((prevMessages) => [data, ...prevMessages]);
    // });
    socketClient.on('NEW_MINTS_ACTION', (data) => {
      setMessagesMint((prevMessages) => [...data, ...prevMessages]);
    });
    return () => {
      socketClient.disconnect();
    };
  }, [url]);

  const sendMessage = (message) => {
    if (socket && isConnected) {
      socket.emit('message', message);
    }
  };

  return { messages, messagesMint, isConnected, isConnecting, sendMessage };
};

export default useWebSocket;
