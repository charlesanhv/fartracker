import { LIST_CHAIN } from 'constants';
import { useState } from 'react';

const useDropdown = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedItem, setSelectedItem] = useState(LIST_CHAIN.BASE_MAINNET);

  const toggleDropdown = () => setIsOpen(!isOpen);

  const selectItem = (item) => {
    setSelectedItem(item);
    setIsOpen(false);
  };

  return {
    isOpen,
    selectedItem,
    toggleDropdown,
    selectItem,
  };
};

export default useDropdown;
