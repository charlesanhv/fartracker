import { useCallback, useEffect, useState } from "react";

export const useFetch = (
	fetchFn,
	optionalProps = {},
) => {
	const { args, skip = false } = optionalProps;

	const [loading, setLoading] = useState(false);
	const [result, setResult] = useState(null);
	const [error, setError] = useState(null);

	const useFetchArgsDeps = args ? Object.values(args) : [];

	const fetchData = useCallback(
		async (immediateArgs) => {
			setLoading(true);
			try {
				const result = await fetchFn(immediateArgs || args);
				// const result = await response.json();
				setResult(result);
				return result;
			} catch (e) {
				setError(e);
				return null;
			} finally {
				setLoading(false);
			}
		},
		// eslint-disable-next-line react-hooks/exhaustive-deps
		[fetchFn, ...useFetchArgsDeps],
	);

	useEffect(() => {
		if (skip) {
			return;
		}

		void fetchData();
	}, [skip, fetchData]);

	return [{ data: result, loading, error }, fetchData];
};
