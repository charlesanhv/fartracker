import { useSelector, useDispatch, Provider } from "react-redux";
import {
  REDUX_ACTION,
  WEB3_CONNECT_TYPE,
  WALLET_CONNECT_PROJECT_ID,
  UNIVERSAL_ABI,
  ROUTER_ADDRESS,
  ERC20_ABI,
  BASE_TOKEN_ADDRESS,
  WEB3_ABI,
  BASE_FEE,
  FARTRACKER_SWAP_FEE,
  WEB3_ZERO_ADDRESS,
  PROVIDER_GET_DATA
} from "../constants";
import { EthereumProvider } from "@walletconnect/ethereum-provider";
import { ethers, parseEther } from "ethers";
import { useState, useEffect } from "react";
import { BlockchainAPI } from "actions";
import {
  AllowanceProvider,
  MaxAllowanceTransferAmount,
  AllowanceTransfer,
} from "@uniswap/permit2-sdk";
import { signTypedData } from "@uniswap/conedison/provider/index";
import { useAccount, useSendTransaction, useWriteContract, useSignMessage, useSwitchChain } from 'wagmi'


const useWeb3 = () => {
  const [providerGetData, setProviderGetData] = useState(new ethers.providers.JsonRpcProvider(PROVIDER_GET_DATA.BASE_MAINNET));
  const dispatch = useDispatch();
  const [web3Lib, setWeb3Lib] = useState(null);
  const account = useAccount()
  const { writeContractAsync } = useWriteContract()
  const { sendTransactionAsync: wagmiSendTransaction } = useSendTransaction();
  const { signMessageAsync: wagmiSignMessage } = useSignMessage();
  const { chains: wagmiChains, switchChainAsync: wagmiSwitchChain } = useSwitchChain()

  const setReduxWeb3Info = (web3_info) => {
    dispatch({
      type: REDUX_ACTION.SET_WEB3_INFO,
      payload: JSON.stringify(web3_info),
    });
  };
  const web3Info = useSelector((state) => state.reducer.client_web3_info);
  const stableCryptoPrice = useSelector(
    (state) => state.reducer.stable_crypto_price
  );
  const signMessage = async (message) => {
    if (!account.isConnected) {
      return false;
    }
    try {
      const rawSig = await wagmiSignMessage(message);
      console.log("rawSig : ", rawSig);
      return rawSig;
    } catch (error) {
      console.log(error);
      return false;
    }
  };
  const switchChain = async (newChainNetwork) => {
    if (!account.isConnected) {
      return false;
    }
    try {
      await wagmiSwitchChain({chainId: newChainNetwork.id});
    } catch (switchError) {
      console.log(switchError);
    }
  };

  const getSwapTokenPrice = (swap_action) => {
    const tokenGetPrice =
      swap_action.swap.tokenGetPrice?.address?.length > 0
        ? {
          address: swap_action.swap.tokenGetPrice.address ?? "0x",
          // value: bigIntFromParts(swap_action.swap.tokenIn.value),
          value: swap_action.swap.tokenGetPrice.value,
          name: "GET PRICE",
          symbol: "PRICE",
          decimals:
            swap_action.swap.tokenGetPrice.address.toLowerCase() ==
              BASE_TOKEN_ADDRESS.BASE_MAINNET.USDC ||
              swap_action.swap.tokenGetPrice.address.toLowerCase() ==
              BASE_TOKEN_ADDRESS.BASE_MAINNET.USDbC
              ? 6
              : 18,
        }
        : {};
    const tokenIn = {
      address: swap_action.swap.tokenIn.address ?? "0x",
      // value: bigIntFromParts(swap_action.swap.tokenIn.value),
      value: swap_action.swap.tokenIn.value,
      name: swap_action.tokenInInfo.name,
      symbol: swap_action.tokenInInfo.symbol,
      decimals: swap_action.tokenInInfo.decimals,
    };
    const tokenOut = {
      address: swap_action.swap.tokenOut.address ?? "0x",
      // value: bigIntFromParts(swap_action.swap.tokenOut.value),
      value: swap_action.swap.tokenOut.value,
      name: swap_action.tokenOutInfo.name,
      symbol: swap_action.tokenOutInfo.symbol,
      decimals: swap_action.tokenOutInfo.decimals,
    };
    tokenIn.amount = Number(tokenIn.value) / 10 ** tokenIn.decimals;
    tokenOut.amount = Number(tokenOut.value) / 10 ** tokenOut.decimals;
    tokenGetPrice.amount =
      swap_action.swap.tokenGetPrice?.address?.length > 0
        ? Number(tokenGetPrice.value) / 10 ** Number(tokenGetPrice.decimals)
        : null;

    if (tokenIn.address.toLowerCase() == BASE_TOKEN_ADDRESS.BASE_MAINNET.WETH) {
      tokenIn.price = stableCryptoPrice.eth;
    } else if (
      tokenIn.address.toLowerCase() == BASE_TOKEN_ADDRESS.BASE_MAINNET.DAI
    ) {
      tokenIn.price = stableCryptoPrice.dai;
    } else if (
      tokenIn.address.toLowerCase() == BASE_TOKEN_ADDRESS.BASE_MAINNET.USDC ||
      tokenIn.address.toLowerCase() == BASE_TOKEN_ADDRESS.BASE_MAINNET.USDbC
    ) {
      tokenIn.price = stableCryptoPrice.usd;
    }
    if (tokenIn.price > 0) {
      tokenOut.price = (tokenIn.amount * tokenIn.price) / tokenOut.amount;
    } else {
      if (
        tokenOut.address.toLowerCase() == BASE_TOKEN_ADDRESS.BASE_MAINNET.WETH
      ) {
        tokenOut.price = stableCryptoPrice.eth;
      } else if (
        tokenOut.address.toLowerCase() == BASE_TOKEN_ADDRESS.BASE_MAINNET.DAI
      ) {
        tokenOut.price = stableCryptoPrice.dai;
      } else if (
        tokenOut.address.toLowerCase() ==
        BASE_TOKEN_ADDRESS.BASE_MAINNET.USDC ||
        tokenOut.address.toLowerCase() == BASE_TOKEN_ADDRESS.BASE_MAINNET.USDbC
      ) {
        tokenOut.price = stableCryptoPrice.usd;
      }
      if (tokenOut.price > 0) {
        tokenIn.price = (tokenOut.amount * tokenOut.price) / tokenIn.amount;
      }
    }
    if (tokenIn.price > 0 && tokenOut.price > 0) {
      return { tokenIn, tokenOut };
    } else if (swap_action.swap.tokenGetPrice?.address?.length > 0) {
      if (
        tokenGetPrice.address.toLowerCase() ==
        BASE_TOKEN_ADDRESS.BASE_MAINNET.WETH
      ) {
        tokenGetPrice.price = stableCryptoPrice.eth;
      } else if (
        tokenGetPrice.address.toLowerCase() ==
        BASE_TOKEN_ADDRESS.BASE_MAINNET.DAI
      ) {
        tokenGetPrice.price = stableCryptoPrice.dai;
      } else if (
        tokenGetPrice.address.toLowerCase() ==
        BASE_TOKEN_ADDRESS.BASE_MAINNET.USDC ||
        tokenGetPrice.address.toLowerCase() ==
        BASE_TOKEN_ADDRESS.BASE_MAINNET.USDbC
      ) {
        tokenGetPrice.price = stableCryptoPrice.usd;
      }
      if (tokenGetPrice.price > 0) {
        tokenIn.price =
          (tokenGetPrice.amount * tokenGetPrice.price) / tokenIn.amount;
        tokenOut.price =
          (tokenGetPrice.amount * tokenGetPrice.price) / tokenOut.amount;
      }
      return { tokenIn, tokenOut };
    } else {
      return { tokenIn, tokenOut };
    }
  };
  function sqrtToPrice(sqrt, decimals0, decimals1, token0IsInput = true) {
    const numerator = sqrt ** 2;
    const denominator = 2 ** 192;
    var ratio = numerator / denominator;
    const shiftDecimals = Math.pow(10, decimals0 - decimals1);
    if (!token0IsInput) {
      ratio = 1 / ratio;
    }
    return ratio;
  }
  function toDeadline(expiration) {
    return Math.floor((Date.now() + expiration) / 1000);
  }
  const kyberswapFunction = {
    querySwapRoute: async function (
      tokenInAddress,
      tokenOutAddress,
      amountIn,
      feeProxy
    ) {
      var url = `https://aggregator-api.kyberswap.com/base/api/v1/routes?tokenIn=${tokenInAddress}&tokenOut=${tokenOutAddress}&amountIn=${amountIn}`;
      if (Number(feeProxy) > 0) {
        url += `&feeAmount=${feeProxy}&chargeFeeBy=currency_in&isInBps=1&feeReceiver=0x433eEA2f2A68638Bfb5Ed5413b037317cd8B9751`;
      }
      const options = {
        method: "GET",
        headers: {
          "x-client-id": "fartracker",
        },
      };
      const response = await fetch(url, options);
      if (response.ok) {
        return await response.json();
      }
      return false;
    },
    encodedSwapData: async function (
      routeSummary,
      sender,
      recipient,
      slippageTolerance
    ) {
      const response = await fetch(
        "https://aggregator-api.kyberswap.com/base/api/v1/route/build",
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            "x-client-id": "fartracker",
          },
          body: JSON.stringify({
            routeSummary: routeSummary,
            sender: sender,
            recipient: recipient,
            slippageTolerance: slippageTolerance,
          }),
        }
      );
      if (response.ok) {
        return await response.json();
      } else {
        return false;
      }
    },
    swapToken: async function (tokenIn, tokenOut, amountIn, decimalsIn) {
      try {
        // ROUTER_ADDRESS.BASE_MAINNET.TOKEN_IN_IS_ETH_KYBERSWAP
        // const data = await this.querySwapRoute(tokenIn, tokenOut, ethers.utils.parseUnits(amountIn, Number(decimalsIn)));
        const data = await this.querySwapRoute(
          tokenIn,
          tokenOut,
          ethers.utils.parseUnits(amountIn, Number(decimalsIn)),
          25
        ); // 0.25%
        console.log(data);
        if (data == false) {
          return false;
        }
        // const user_address = await web3Signer.getAddress();
        const user_address = account.address;
        const swapData = await this.encodedSwapData(
          data.data.routeSummary,
          user_address,
          user_address,
          500
        ); // 5%
        console.log(swapData);
        const gas = await estimateGas(
          user_address,
          ROUTER_ADDRESS.BASE_MAINNET.ROUTER_KYBERSWAP,
          swapData.data.data,
          tokenIn == ROUTER_ADDRESS.BASE_MAINNET.TOKEN_IN_IS_ETH_KYBERSWAP
            ? ethers.utils.parseUnits(amountIn, Number(decimalsIn))
            : 0
        );
        if (gas > 0) {
          const tx = await wagmiSendTransaction({
            data: swapData.data.data,
            from: user_address,
            to: ROUTER_ADDRESS.BASE_MAINNET.ROUTER_KYBERSWAP,
            value:
              tokenIn == ROUTER_ADDRESS.BASE_MAINNET.TOKEN_IN_IS_ETH_KYBERSWAP
                ? ethers.utils.parseUnits(amountIn, Number(decimalsIn))
                : 0,
          });
          return tx;
        }
        console.log(gas);
      } catch (error) {
        console.log(error);
        return false;
      }
    },
  };
  const estimateGas = async (fromAddress, toAddress, data, value) => {
    // var tempGas = await web3Provider.estimateGas({
    //   from: fromAddress,
    //   to: toAddress,
    //   data: data,
    //   value: value,
    // });
    var tempGas = await providerGetData.estimateGas({
      from: fromAddress,
      to: toAddress,
      data: data,
      value: value,
    });
    return tempGas;
  };
  const sendTransaction = async (fromAddress, toAddress, data, value) => {
    const tx = await wagmiSendTransaction({
      data: data,
      from: fromAddress,
      to: toAddress,
      value: value
    });
    return tx;
  };

  const tokenFunction = {
    checkApproveToken: async (tokenAddress, owner, spender, amount) => {
      const tokenInContract = new ethers.Contract(
        tokenAddress,
        WEB3_ABI.ERC20_ABI,
        providerGetData
      );
      const allowance = await tokenInContract.allowance(owner, spender);
      if (allowance > 0 && allowance >= amount) {
        return true;
      }
      return false;
    },
    approveToken: async (
      tokenAddress,
      spender,
      amount = ethers.utils.parseUnits((1e18).toString(), "ether")
    ) => {
      try {
        // const tokenInContract = new ethers.Contract(
        //   tokenAddress,
        //   WEB3_ABI.ERC20_ABI,
        //   web3Provider
        // );
        // const tokenWithSigner = tokenInContract.connect(web3Signer);
        // const tx = await tokenWithSigner.approve(spender, amount);
        const tx = await writeContractAsync({
          abi: WEB3_ABI.ERC20_ABI,
          address: tokenAddress,
          functionName: "approve",
          args: [
            spender,
            amount
          ]
        })
        // await tx.wait();
        console.log(tx);
        return tx;
      } catch (error) {
        console.log("error 607 , ", error);
        return false;
      }
    },
    tokenBalance: async (tokenAddress, user) => {
      if (!providerGetData && !account.isConnected && user.length < 10) {
        return 0;
      }
      if (
        tokenAddress == ROUTER_ADDRESS.BASE_MAINNET.TOKEN_IN_IS_ETH_KYBERSWAP
      ) {
        // return Number(await web3Provider.getBalance(user));
        return Number(await providerGetData.getBalance(user));
      }
      const tokenInContract = new ethers.Contract(
        tokenAddress,
        WEB3_ABI.ERC20_ABI,
        // web3Provider
        providerGetData
      );
      const balance = Number(await tokenInContract.balanceOf(user));
      return balance;
    },
  };
  const mintNFT = async (txHash) => {
    // const receipt = await web3Provider.getTransactionReceipt(txHash);
    if (!account.isConnected) {
      return 0;
    }
    // const transactionInfo = await web3Provider.getTransaction(txHash);
    const transactionInfo = await providerGetData.getTransaction(txHash);
    // console.log(receipt);
    console.warn(transactionInfo);
    const clientAddress = account.address;
    console.warn(clientAddress);
    const dataTx = transactionInfo.data.replace((transactionInfo.from.substring(2, transactionInfo.from.length)).toLowerCase(), (clientAddress.substring(2, clientAddress.length)).toLowerCase());
    console.warn("transactionInfo.value : ", transactionInfo.value);
    var mintAvailable = false;
    if (transactionInfo.value <= 9e15) {
      try {
        const gas = await estimateGas(clientAddress, transactionInfo.to, dataTx, transactionInfo.value);
        console.warn("Gas ", gas);
        mintAvailable = true;
      } catch (error) {
        console.warn(error);
        alert("This NFT does not have a quick mint function available");
        return false;
      }
    }
    if (mintAvailable) {
      const tx = await sendTransaction(clientAddress, transactionInfo.to, dataTx, transactionInfo.value);
      return tx;
    }

  }


  return {
    web3Info,
    signMessage,
    switchChain,
    getSwapTokenPrice,
    // getBestSinglePathSwap,
    // estimateOutputValue,
    // permit2
    kyberswapFunction,
    estimateGas,
    sendTransaction,
    tokenFunction,
    mintNFT
  };
};

export default useWeb3;
