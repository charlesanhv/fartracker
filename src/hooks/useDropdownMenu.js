import { useEffect, useRef } from "react";
import useDropdown from "./useDropdown";

const useDropdownMenu = (containerRef) => {
  const { isOpen, selectedItem, toggleDropdown, selectItem } = useDropdown();
  const dropdownRef = useRef(null);
  const menuRef = useRef(null);

  const handleClickOutside = (event) => {
    if (dropdownRef.current && !dropdownRef.current.contains(event.target)) {
      toggleDropdown();
    }
  };

  useEffect(() => {
    if (isOpen) {
      document.addEventListener("mousedown", handleClickOutside);
      if (containerRef.current && menuRef.current) {
        menuRef.current.style.width = `${containerRef.current.offsetWidth + 60}px`;
        if(window.innerWidth < 768) {
          // menuRef.current.style.right = `50%`;
          menuRef.current.style.left = `-${containerRef.current.offsetWidth - 20}px`;
        }
      }
    } else {
      document.removeEventListener("mousedown", handleClickOutside);
    }

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [isOpen, containerRef]);

  return {
    isOpen,
    selectedItem,
    toggleDropdown,
    selectItem,
    dropdownRef,
    menuRef,
  };
};

export default useDropdownMenu;
