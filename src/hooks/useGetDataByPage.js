import { useMemo } from "react";
import { useSelector } from "react-redux";

const useGetDataByPage = (data = [], sizePage = 20) => {
    const pageTable = useSelector((state) => state?.home?.pageNumber);

    const dataByPage = useMemo(() => {
        const start = (pageTable - 1) * sizePage;
        const end = start + sizePage;

        return data.slice(start, end);
    }, [data, pageTable, sizePage]);

    return dataByPage;
};

export default useGetDataByPage;
