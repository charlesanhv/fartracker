import { useMediaQuery } from "react-responsive";

const useScreenType = () => {
  const isMobile = useMediaQuery({ maxWidth: 767 });
  const isTablet = useMediaQuery({ minWidth: 768, maxWidth: 1024 });
  const isDesktopOrLaptop = useMediaQuery({ minWidth: 1025 });

  return {
    isMobile,
    isTablet,
    isDesktopOrLaptop,
  };
};

export default useScreenType;
