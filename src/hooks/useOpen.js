import { useState } from 'react'

const useOpen = (isShow = false) => {
    const [isShowing, setIsShowing] = useState(isShow)

    function toggle() {
        setIsShowing(!isShowing)
    }

    function open() {
        setIsShowing(true)
    }

    function close() {
        setIsShowing(false)
    }

    return {
        isShowing,
        toggle,
        open,
        close
    }
}

export default useOpen
