import React from "react";
import ReactDOM from "react-dom/client";
import "./fartracker.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "./index.css";
// import App from './App';
import reportWebVitals from "./reportWebVitals";
import Routers from "./Routers";

import { store } from "./redux";
import { Provider } from "react-redux";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import { WagmiProvider } from "wagmi";
import { ConnectKitProvider } from "connectkit";
import { config } from "./web3config";

const queryClient = new QueryClient();

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.Fragment>
    <Provider store={store}>
      <WagmiProvider config={config}>
        <QueryClientProvider client={queryClient}>
          <ConnectKitProvider theme="default" mode="dark">
            <main className="flex-shrink-0 position-relative">
              <Routers />
            </main>
            {/* <App /> */}
          </ConnectKitProvider>
        </QueryClientProvider>
      </WagmiProvider>
    </Provider>
  </React.Fragment>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
