/* eslint-disable eqeqeq */
import { configureStore } from '@reduxjs/toolkit';
import * as CONSTANTS from '../constants';
import { StorageAPI } from '../actions';
import homeReducers from './homeReducer';
import hookReducers from './hookReducer';
// import { EthereumProvider } from "@walletconnect/ethereum-provider";

// const projectId = "c33877b39c862be3d4abd8ff4dd09ce5";
// const walletConnectProvider = await EthereumProvider.init({
//     projectId,
//     chains: [1],
//     methods: ["personal_sign", "eth_sendTransaction"],
//     showQrModal: true,
//     qrModalOptions: {
//       themeMode: "dark",
//     },
// });
const web3_info_in_local = StorageAPI.load(CONSTANTS.LOCAL_STORAGE.WEB3_INFO);
const reduxStateSeeds = {
  client_auth_info: {
    bearer_token: '',
    logged: true,
    rank: CONSTANTS.ROUTE.RANK.REGISTER,
  },
  client_base_info: {},
  client_web3_info:
    web3_info_in_local != false
      ? web3_info_in_local
      : {
        address: '0x',
        type: CONSTANTS.WEB3_CONNECT_TYPE.IN_BROWSER,
        status: 0,
        session: null,
      }, //  {address, type, status, session}
  is_loading: false,
  stable_crypto_price: {
    eth: 3100,
    usd: 1,
    dai: 1
  }
};
const reducer = (state = reduxStateSeeds, action) => {
  switch (action.type) {
    case CONSTANTS.REDUX_ACTION.SET_CLIENT_INFO:
      return {
        ...state,
        client_info: action.payload,
      };
    case CONSTANTS.REDUX_ACTION.SET_LOADING_STATE:
      return {
        ...state,
        is_loading: action.payload ? true : false,
      };
    case CONSTANTS.REDUX_ACTION.SET_WEB3_INFO:
      var temp = JSON.parse(JSON.stringify(state.client_web3_info));
      action.payload = JSON.parse(action.payload);
      if (action.payload?.hasOwnProperty('address')) {
        temp['address'] = action.payload['address'];
      }
      if (action.payload?.hasOwnProperty('type')) {
        temp['type'] = action.payload['type'];
      }
      if (action.payload?.hasOwnProperty('status')) {
        temp['status'] = action.payload['status'];
      }
      if (action.payload?.hasOwnProperty('session')) {
        temp['session'] = action.payload['session'];
      }
      StorageAPI.save(CONSTANTS.LOCAL_STORAGE.WEB3_INFO, JSON.stringify(temp));

      return {
        ...state,
        client_web3_info: temp,
      };
    // case constants.REDUX_ACTION_CONSTANTS.SET_AUTH_TOKEN:
    //     return {
    //         ...state,
    //         auth_token: action.payload
    //     }
    case CONSTANTS.REDUX_ACTION.SET_STABLE_CRYPTO_PRICE:
      return {
        ...state,
        stable_crypto_price: action.payload
      }
    default:
      return state;
  }
};

export const store = configureStore({
  reducer: {
    reducer,
    home: homeReducers,
    hook: hookReducers,
  },
});
