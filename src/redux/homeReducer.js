import { createSlice } from "@reduxjs/toolkit";
import { FILTER_ACTION } from "constants";

export const homeSlice = createSlice({
  name: "home",
  initialState: {
    value: 0,
    type: FILTER_ACTION.TYPE.NORMAL,
    pageNumber: 1,
    isOpenModalFilter: false,
    trending: {
      id: 33453,
      name: "Notcoin",
      icon: "",
    },

    buy_sell_value: {
      type: "Sell",
      amount: 0,
    },
    dataSwap: {},
    dataMint: {},
    filterByUser: {},
    dataFilter: [],
    kol_options: [],
  },
  reducers: {
    increment: (state) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      state.value += 1;
    },
    decrement: (state) => {
      state.value -= 1;
    },
    incrementByAmount: (state, action) => {
      state.value += action.payload;
    },
    setFilterType: (state, action) => {
      state.type = action.payload;
      state.pageNumber = 1;
    },
    setPageNumber: (state, action) => {
      state.pageNumber = action.payload;
    },
    actionSetModalFilter: (state, action) => {
      state.isOpenModalFilter = action.payload;
    },

    actionSetTrending: (state, action) => {
      state.trending = action.payload;
    },

    actionSetBuySellValue: (state, action) => {
      state.buy_sell_value = action.payload;
    },

    actionSetDataSwap: (state, action) => {
      state.dataSwap = action.payload;
    },

    actionSetDataMint: (state, action) => {
      state.dataMint = action.payload;
    },
    actionSetFilterByUser: (state, action) => {
      state.filterByUser = action.payload;
    },
    actionSetDataFilter: (state, action) => {
      state.dataFilter = action.payload;
    },

    actionSetKolOptions: (state, action) => {
      state.kol_options = action.payload;
    },
  },
});

export const {
  increment,
  decrement,
  incrementByAmount,
  setFilterType,
  setPageNumber,
  actionSetModalFilter,
  actionSetTrending,
  actionSetBuySellValue,
  actionSetDataSwap,
  actionSetDataMint,
  actionSetFilterByUser,
  actionSetDataFilter,
  actionSetKolOptions,
} = homeSlice.actions;

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched
export const incrementAsync = (amount) => (dispatch) => {
  setTimeout(() => {
    dispatch(incrementByAmount(amount));
  }, 1000);
};

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
export const selectCount = (state) => state.home.value;
export const selectType = (state) => state.home.type;
export const getPageNumber = (state) => state.home.pageNumber;

export default homeSlice.reducer;
