import { createSlice } from "@reduxjs/toolkit";

export const hookSlice = createSlice({
    name: "hook",
    initialState: {
        showSwap: false,
    },
    reducers: {
        actionSetShowSwap: (state, action) => {
            state.showSwap = action.payload;
        }
    },
});

export const {
    actionSetShowSwap
} = hookSlice.actions;

export default hookSlice.reducer;
