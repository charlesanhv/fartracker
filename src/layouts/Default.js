function Default(props){
    const {Header, Footer, Content} = props; // props tag

    return  (
        <div>
            {Header ? <Header /> : null}
            {Content ? <Content /> : null}
            {/* {Footer ? <Footer /> : null} */}
        </div>
    )
}

export default Default;