import { lazy, memo, useEffect, useMemo, useState } from "react";
import LazyLoadWithDelay from "components/LazyLoadWithDelay";
import { useDispatch, useSelector } from "react-redux";
import useWebSocket from "hooks/useSocket";
import { LIST_FIDS_VIP_USER, LIST_TOKEN_IN } from "constants";
import useWeb3 from "hooks/useWeb3";
import {
  actionSetDataFilter,
  getPageNumber,
  selectType,
  setFilterType,
  setPageNumber,
} from "../redux/homeReducer";
import Pagination from "components/Home/Pagination/Pagination";
import ModalFilter from "components/Home/ModalFilter";
import { FilterConfirmation, LoadingTransaction } from "components/Home";
import InhouseWidget from "components/InhouseWidget";
import { useLocation, useParams } from "react-router-dom";
import { useFetch } from "hooks";
import {
  fetchTransactionsByUser,
  fetchTransactionsPowerPadge,
  fetchTransactionsWhale,
} from "services";

const Transactions = lazy(async () => {
  const module = await import("components/Home/Transactions/Transactions");
  return { default: module.Transactions };
});
const LazyTrending = lazy(async () => {
  const module = await import("components/Home/Trending");
  return { default: module.Trending };
});

const MemoizedLazyTrending = memo(LazyTrending);

export const Home = () => {
  const location = useLocation();
  const { username } = useParams();

  const pathSegments = location.pathname
    .split("/")
    .filter((segment) => segment);

  // Assuming you want the last segment
  // const value = pathSegments[pathSegments.length - 1];
  const value = pathSegments[0];
  const dispatch = useDispatch();
  const isOpenModalFilter = useSelector(
    (state) => state.home.isOpenModalFilter
  );
  const { messages, messagesMint, isConnecting } = useWebSocket(
    "https://socket.fartracker.com"
    // "http://localhost:3005"
  );

  const { getSwapTokenPrice } = useWeb3();
  const filterType = useSelector(selectType);
  const kol_options = useSelector((state) => state.home.kol_options);
  const pageNumber = useSelector(getPageNumber);
  const [convertedMessages, setConvertedMessages] = useState([]);
  const [convertedMint, setConvertedMint] = useState([]);

  const [{ data: dataWhale, loading: loadingWhale }] = useFetch(
    fetchTransactionsWhale,
    {
      args: { skip: (pageNumber - 1) * 20, take: 21 },
      skip: filterType !== "whale",
    }
  );

  const [{ data: dataPower, loading: loadingPower }] = useFetch(
    fetchTransactionsPowerPadge,
    {
      args: { skip: (pageNumber - 1) * 20, take: 21 },
      skip: filterType !== "vip",
    }
  );

  const [{ data: dataUser, loading: loadingUser }] = useFetch(
    fetchTransactionsByUser,
    {
      args: {
        skip: (pageNumber - 1) * 20,
        take: 21,
        type: value === "mint" ? "mint" : "swap",
        username: username,
      },
      skip: filterType !== "user",
    }
  );

  const isWhale = useMemo(() => {
    return (value) => value > 1000;
  }, []);

  const convertedData = (dataMap) => {
    const convertMint = dataMap?.map((mint) => {
      const action = "MINT";
      const urlpng = "/assets/images/nft.svg";
      return {
        ...mint,
        vip: LIST_FIDS_VIP_USER.includes(mint?.farcasterInfo?.fid),
        whale: false,
        value,
        action,
        maker: mint?.nft?.address,
        time: mint?.timestamp,
        address: mint?.from,
        logo_token: urlpng,
        block: mint?.blockNumber,
        type: mint?.nft?.type,
      };
    });

    return convertMint;
  };


  useEffect(() => {
    switch (filterType) {
      case "whale": dispatch(actionSetDataFilter(dataWhale)); break;
      case "vip": dispatch(actionSetDataFilter(dataPower)); break;
      case "user": dispatch(actionSetDataFilter(dataUser)); break;
      default: dispatch(actionSetDataFilter([])); break;
    }
  }, [filterType, dataWhale, dataPower, dataUser]);

  const KolTier = (numFollowers) => {
    const numFollow = Number(numFollowers);
    switch (true) {
      case numFollow >= 100000:
        return "S";
      case numFollow >= 50000:
        return "A";
      case numFollow >= 20000:
        return "B";
      case numFollow >= 5000:
        return "C";
      case numFollow >= 1000:
        return "D";
      default:
        return "";
    }
  };

  const AddKolTierToData = (data = []) => {
    if (!data) return [];
    const newData = data.map((item) => {
      const { farcasterInfo } = item;
      const { followerCount } = farcasterInfo;

      return {
        ...item,
        tier: KolTier(followerCount),
      };
    });

    return newData;
  };

  const FilterDataByKol = (data) => {
    if (kol_options.length === 0) return data;
    if (!kol_options?.includes("all")) {
      const newFilterByKol = data.filter((message) =>
        kol_options.includes(message?.tier)
      );
      return newFilterByKol;
    } else {
      return data;
    }
  };

  useEffect(() => {
    if (username) {
      dispatch(setFilterType("user"));
    }
  }, [username]);

  useEffect(() => {
    if (value === "mint") {
      // const convertMint = messagesMint.map((mint) => {
      //   const action = "MINT";
      //   const urlpng = "/assets/images/nft.svg";
      //   return {
      //     ...mint,
      //     vip: LIST_FIDS_VIP_USER.includes(mint?.farcasterInfo?.fid),
      //     whale: false,
      //     value,
      //     action,
      //     maker: mint?.nft?.address,
      //     time: mint?.timestamp,
      //     address: mint?.from,
      //     logo_token: urlpng,
      //     block: mint?.blockNumber,
      //     type: mint?.nft?.type,
      //   };
      // });
      const convertMint = convertedData(messagesMint);

      if (filterType === "vip" || filterType === "whale") {
        const filteredMessages = convertMint.filter((message) =>
          filterType === "vip" ? message.vip : message.whale
        );
        dispatch(setPageNumber(1));
        setConvertedMint(FilterDataByKol(AddKolTierToData(filteredMessages)));
      } else if (filterType === "user") {
        const dataMint = convertedData(dataUser);
       
        setConvertedMint(FilterDataByKol(AddKolTierToData(dataMint)));
      } else {
        setConvertedMint(FilterDataByKol(AddKolTierToData(convertMint)));
      }
  
    } else {
      const dataFilter =filterType === "whale"
          ? dataWhale
          : filterType === "vip"
          ? dataPower
          : filterType === "user"
          ? dataUser : [];
      const data =
          dataFilter?.length > 0 ? dataFilter.slice(0, 20)
          : messages;
      const convertMessages = data?.map((message) => {
        const swapToken = getSwapTokenPrice(message);
        const action = LIST_TOKEN_IN.includes(message?.tokenInInfo?.symbol)
          ? "PUMP"
          : "DUMP";
        const token =
          action === "PUMP"
            ? swapToken?.tokenOut?.symbol
            : swapToken?.tokenIn?.symbol;
        // const price = parseFloat(swapToken?.tokenOut?.price);
        // const amount = parseFloat(swapToken?.tokenOut?.amount);
        const price = parseFloat(
          token == swapToken?.tokenOut?.symbol
            ? swapToken?.tokenOut?.price
            : swapToken?.tokenIn?.price
        );
        const amount = parseFloat(
          token == swapToken?.tokenOut?.symbol
            ? swapToken?.tokenOut?.amount
            : swapToken?.tokenIn?.amount
        );
        const value = price * amount;
        const urlpng = `/ds-data/tokens/base/${
          action === "PUMP"
            ? swapToken?.tokenOut?.address
            : swapToken?.tokenIn?.address
        }.png?size=lg&key=e9c33`;

        const addressToken =
          action === "PUMP"
            ? swapToken?.tokenOut?.address
            : swapToken?.tokenIn?.address;
        return {
          ...message,
          vip:
            filterType === "vip"
              ? true
              : LIST_FIDS_VIP_USER.includes(message?.farcasterInfo?.fid),
          whale: filterType === "whale" ? true : isWhale(value),
          amount,
          value,
          token,
          price,
          action,
          time: message?.timestamp,
          address: message?.from,
          addressToken,
          logo_token: urlpng,
        };
      });

      // if (filterType === "vip" || filterType === "whale") {
      //   const filteredMessages = convertMessages.filter((message) =>
      //     filterType === "vip" ? message.vip : message.whale
      //   );
      //   dispatch(setPageNumber(1));
      //   setConvertedMessages(filteredMessages);
      // } else {
      //   setConvertedMessages(convertMessages);
      // }
      const dataAddTier = AddKolTierToData(convertMessages);
      const dataByKol = FilterDataByKol(dataAddTier);
      setConvertedMessages(dataByKol);
    }
  }, [
    messages,
    filterType,
    isWhale,
    value,
    messagesMint,
    dataWhale,
    dataPower,
    dataUser,
    kol_options,
  ]);

  return (
    <>
      <LazyLoadWithDelay>
        <MemoizedLazyTrending />
      </LazyLoadWithDelay>
      <FilterConfirmation dataType={value === "mint" ? "Mint" : "Default"} />
      <LazyLoadWithDelay
        loading={
          isConnecting ||
          loadingWhale ||
          loadingPower ||
          loadingUser ||
          messages.length === 0
        }
        fallback={<LoadingTransaction height={"70vh"} />}
      >
        <div
          className="container d-flex"
          style={{
            gap: "10px",
            padding: "0",
            position: "relative",
          }}
        >
          <Transactions
            DataTransactions={
              value === "mint" ? convertedMint : convertedMessages
            }
            dataType={value === "mint" ? "Mint" : "Default"}
          />
          <InhouseWidget />
        </div>
      </LazyLoadWithDelay>
      <Pagination
        size={Math.ceil(
          value === "mint"
            ? convertedMint?.length / 20
            : convertedMessages?.length / 20
        )}
        step={2}
      />
      {isOpenModalFilter && <ModalFilter />}
    </>
  );
};
