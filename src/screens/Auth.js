// import { BlockchainAPI } from "../actions"
import { useDispatch, useSelector } from 'react-redux'
// import { REDUX_ACTION } from "../constants";
import useWeb3 from "hooks/useWeb3";
import { ethers } from "ethers";
import { ROUTER_ADDRESS } from "../constants";

function Login() {
    // const dispatch = useDispatch();
    const { uniswapFunction, kyberswapFunction, estimateGas, web3Signer, tokenFunction } = useWeb3()
    const { permit2, getBestSinglePathSwap, estimateOutputValue } = uniswapFunction;
    const web3_info = useSelector((state) => state.reducer.client_web3_info);
    const estimate = async () => {
        const bestSinglePathSwap = await getBestSinglePathSwap(
            { address: "0x4200000000000000000000000000000000000006", decimals: 18 },
            { address: "0x833589fCD6eDb6E08f4c7C32D4f71b54bdA02913", decimals: 6 },
        );
        if (bestSinglePathSwap != false) {
            estimateOutputValue(
                { address: "0x4200000000000000000000000000000000000006", decimals: 18 },
                { address: "0x833589fCD6eDb6E08f4c7C32D4f71b54bdA02913", decimals: 6 },
                ethers.utils.parseEther('10'), bestSinglePathSwap.swap_version, bestSinglePathSwap.poolAddress, bestSinglePathSwap.fee).then(data => console.log(data));
        }
    }
    const querySwapRoute = async () => {
        const data = await kyberswapFunction.querySwapRoute(ROUTER_ADDRESS.BASE_MAINNET.TOKEN_IN_IS_ETH_KYBERSWAP, "0x833589fCD6eDb6E08f4c7C32D4f71b54bdA02913", ethers.utils.parseUnits('0.0001', 'ether'));
        console.log(data);
        const swapData = await kyberswapFunction.encodedSwapData(data.data.routeSummary, web3_info.address, web3_info.address, 100);
        console.log(swapData);
        const gas = await estimateGas(web3_info.address, ROUTER_ADDRESS.BASE_MAINNET.ROUTER_KYBERSWAP, swapData.data.data, ethers.utils.parseUnits('0.0001', 'ether'));
        // if(gas > 0){
        //     web3Signer.sendTransaction({
        //         data: swapData.data.data,
        //         from: web3_info.address,
        //         to: ROUTER_ADDRESS.BASE_MAINNET.ROUTER_KYBERSWAP,
        //         value: ethers.utils.parseUnits('0.0001', 'ether')
        //     });
        // }
    }
    const testApprove = async () => {
        tokenFunction.approveToken("0x833589fCD6eDb6E08f4c7C32D4f71b54bdA02913", ROUTER_ADDRESS.BASE_MAINNET.ROUTER_KYBERSWAP, ethers.utils.parseUnits('1000000', 'ether'));
    }
    return (
        <div style={{ marginTop: 100, display: 'flex', alignItems: 'center', flexDirection: 'column' }}>
            This is Login screen
            {/* <WalletConnect /> */}
            {/* <UniswapWidget /> */}
            <button onClick={estimate}>Estimate</button>
            <button onClick={querySwapRoute}>querySwapRoute</button>
            <button onClick={testApprove}>testApprove</button>
        </div>
    )
}

export {
    Login
}