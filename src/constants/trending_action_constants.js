
import {
  ChannelIcon,
  DateIcon,
  PokeChipIcon,
  UserCircleIcon,
  WarpCastIcon,
  WhaleIcon,
} from "assets/icons/icons_trending_action";
import { NewTabIcon } from "assets/icons/newTab";

export const ACTIVATE_MODAL_FILTER_TYPE = ["user", "chanel", "token"];

export const SEARCH_ITEMS = [
  {
    avatar: "dummy_avatar_modal_search.png",
    name: "Travis JBec",
    icon: "fire_icon.svg",
  },
  {
    avatar: "dummy_avatar_modal_search2.png",
    name: "Mike Melodic",
    icon: "fire_icon.svg",
  },
  { avatar: "dummy_avatar_modal_search3.png", name: "Quavo" },
  { avatar: "dummy_avatar_modal_search18.png", name: "Thebec" },
  { avatar: "dummy_avatar_modal_search4.png", name: "Don Toliver" },
  { avatar: "dummy_avatar_modal_search5.png", name: "Dang Beo" },
  { avatar: "dummy_avatar_modal_search6.png", name: "Kiuchangyie" },
  { avatar: "dummy_avatar_modal_search7.png", name: "Phantom Panda" },
  { avatar: "dummy_avatar_modal_search8.png", name: "Playboi Carti" },
  { avatar: "dummy_avatar_modal_search9.png", name: "Dan" },
  { avatar: "dummy_avatar_modal_search10.png", name: "Mike Melodic" },
  { avatar: "dummy_avatar_modal_search11.png", name: "Pinky Pinky Po" },
  { avatar: "dummy_avatar_modal_search12.png", name: "Metro Boomin" },
  { avatar: "dummy_avatar_modal_search13.png", name: "Adayroi Jean" },
  { avatar: "dummy_avatar_modal_search14.png", name: "ShopeeX" },
  { avatar: "dummy_avatar_modal_search15.png", name: "TrapLife" },
  { avatar: "dummy_avatar_modal_search16.png", name: "Nomore Trap 2024" },
  { avatar: "dummy_avatar_modal_search17.png", name: "Lancuoi Trap" },
];

export const TRENDING = [
  {
    id: 1,
    name: "BITCOIN",
    icon: "https://s2.coinmarketcap.com/static/img/coins/64x64/1.png",
    isFomo: true,
  },
  {
    id: 2,
    name: "ETHEREUM",
    icon: "https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png",
    isFomo: true,
  },
  {
    id: 3,
    name: "BINANCE COIN",
    icon: "https://s2.coinmarketcap.com/static/img/coins/64x64/1839.png",
    isFomo: true,
  },
  {
    id: 4,
    name: "SOLANA",
    icon: "https://s2.coinmarketcap.com/static/img/coins/64x64/5426.png",
    isFomo: true,
  },
  {
    id: 5,
    name: "POLKADOT",
    icon: "https://s2.coinmarketcap.com/static/img/coins/64x64/6636.png",
    token: "DOT",
    isFomo: false,
  },
  {
    id: 6,
    name: "AVAX",
    icon: "https://s2.coinmarketcap.com/static/img/coins/64x64/5805.png",
    isFomo: false,
  },
  {
    id: 7,
    name: "LUNA",
    icon: "https://s2.coinmarketcap.com/static/img/coins/64x64/20314.png",
    isFomo: false,
  },
  {
    id: 8,
    name: "XRP",
    icon: "https://s2.coinmarketcap.com/static/img/coins/64x64/52.png",
    isFomo: false,
  },
  {
    id: 9,
    name: "TRON",
    icon: "https://s2.coinmarketcap.com/static/img/coins/64x64/1958.png",
    isFomo: false,
  },
];

export const filterTypes = [
  // { type: FILTER_ACTION.TYPE.NORMAL, label: 'Normal', isMobile: true },
  {
    type: "vip",
    label: "Vip",
    position: "left",
    icon: <WarpCastIcon />,
    iconSvg:'warpcast.svg'
  },
  {
    type: "whale",
    label: "Whale",
    position: "left",
    icon: <WhaleIcon />,
  },
  {
    type: "mint",
    label: "Mint",
    position: "left",
  },
  {
    type: "user",
    label: "Users",
    position: "right",
    icon: <UserCircleIcon svgClassname={"trending_action_svg"} />,
    // disabled: true,
  },
  // {
  //   type: "chanel",
  //   label: "Channel",
  //   position: "right",
  //   icon: <ChannelIcon svgClassname={"trending_action_svg"} />,
  // },
  // {
  //   type: "token",
  //   label: "Token",
  //   position: "right",
  //   icon: <PokeChipIcon svgClassname={"trending_action_svg"} />,
  // },
  // {
  //   type: "date",
  //   label: "Date",
  //   position: "right",
  //   icon: <DateIcon svgClassname={"trending_action_svg"} />,
  // },
];

export const KOL_OPTIONS = [
  {value: 'all', label: 'All Tier'},
  {value: 'S', label: 'KOL Tier S'},
  {value: 'A', label: 'KOL Tier A'},
  {value: 'B', label: 'KOL Tier B'},
  {value: 'C', label: 'KOL Tier C'},
  {value: 'D', label: 'KOL Tier D'},
]