import { LogoA } from "assets/icons/logoA";
import { LogoB } from "assets/icons/logoB";
import { LogoC } from "assets/icons/logoC";
import { LogoD } from "assets/icons/logoD";
import { LogoS } from "assets/icons/logoS";

export const DataTransactions = [
  {
    _id: {
      $oid: "667be3c3608339ef9f9bd62d",
    },
    fid: 1,
    avatar: "https://i.imgur.com/I2rEbPF.png",
    username: "farcaster",
    displayName: "Farcaster",
    token: "DAI",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f992f27",
    },
    fid: 2,
    avatar:
      "https://i.seadn.io/gae/sYAr036bd0bRpj7OX6B-F-MqLGznVkK3--DSneL_BT5GX4NZJ3Zu91PgjpD9-xuVJtHq0qirJfPZeMKrahz8Us2Tj_X8qdNPYC-imqs?w=500&auto=format",
    username: "v",
    displayName: "Varun Srinivasan",
    token: "USDC",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f957543",
    },
    fid: 3,
    avatar:
      "https://res.cloudinary.com/merkle-manufactory/image/fetch/c_fill,f_png,w_256/https://lh3.googleusercontent.com/MyUBL0xHzMeBu7DXQAqv0bM9y6s4i4qjnhcXz5fxZKS3gwWgtamxxmxzCJX7m2cuYeGalyseCA2Y6OBKDMR06TWg2uwknnhdkDA1AA",
    username: "dwr.eth",
    displayName: "Dan Romero",
    token: "USDT",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9915f8",
    },
    fid: 4,
    avatar:
      "https://lh3.googleusercontent.com/gsOZX_CswJw_uh0KBW3WGjWlkCYV2kZfNGezAkCKml54Vm3q_Q2xrJudX6ILYkfKG9I1Q7WvTKAluAQj_yyur_XXSojP29SSp9h9",
    username: "noah",
    displayName: "Noah Jessop",
    token: "BUSD",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f95620d",
    },
    fid: 5,
    avatar:
      "https://i.seadn.io/gcs/files/1d12ec3563a9b6abac76d9636c5ed9ac.jpg?w=500&auto=format",
    username: "mircea.eth",
    displayName: "Mircea Pasoi",
    token: "WBTC",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f94865b",
    },
    fid: 6,
    avatar:
      "https://lh3.googleusercontent.com/Ikka42EZ7e7u9RZfDu7RTP2gDqh6HZd77wUzyneAL72QC1UBIUB2Bf9Zs1wPbDmToxM1lu06n0KCfQ6AoH1nio37tBDMat18FXcbunE",
    username: "libovness",
    displayName: "libovness",
    token: "WETH",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9c033f",
    },
    fid: 7,
    avatar:
      "https://lh3.googleusercontent.com/h6cxmp8kjwDzTuhZOLEsQfwi_Rfx7eJZphvKqm35zqIiAylaJXA-1x7fvRlVd8Ze6Q1NgrbApgs-HXhUrFJHwOn1SVZTbRn6bjCYkA",
    username: "kevin",
    displayName: "kevin",
    token: "WMATIC",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f9575fa",
    },
    fid: 8,
    avatar: "https://i.imgur.com/tt8uLVd.jpg",
    username: "jacob",
    displayName: "jacob",
    token: "WXDC",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9ccd9b",
    },
    fid: 9,
    avatar:
      "https://i.seadn.io/gae/oV72v-ZhWvVOtqiBlaRSQP2EMYDRrjcSYmjtDYt3ePfWN1kf7dSopMrvPZgMe9StVUWVwedlAuNpatgxc16OrtcmdZ8KZo_T3R-oIR8?w=500&auto=format",
    username: "b-rad",
    displayName: "b-rad.eth",
    token: "WBNB",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f95cea5",
    },
    fid: 10,
    avatar:
      "https://res.cloudinary.com/merkle-manufactory/image/upload/v1701386994/dragonball-tenkaichi-daibouken_400x400_if3djc.jpg",
    username: "elad",
    displayName: "Elad",
    token: "WLTC",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9b7dc0",
    },
    fid: 11,
    avatar: "http://github.com/grahamjenson.png",
    username: "graham",
    displayName: "Graham",
    token: "WINR",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9d80b1",
    },
    fid: 12,
    avatar:
      "https://i.seadn.io/gae/r6CW_kgQygQhI7-4JdWt_Nbf_bjFNnEM7dSns1nZGrijJvUMaLnpAFuBLwjsHXTkyX8zfgpRJCYibtm7ojeA2_ASQwSJgh7yKEFVMOI?w=500&auto=format",
    username: "linda",
    displayName: "Linda Xie",
    token: "DAI",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f94f783",
    },
    fid: 13,
    avatar:
      "https://lh3.googleusercontent.com/cRViHnlDceQ1rU28Puz9Ro0dE7859ABdZGxmJQrswY5YKfehz4kNDZ-fhp1Fyk1wM2tGwCmtub0CRsnS7H6TMNaUVwaQOyKD_btjRw",
    username: "jimpo",
    displayName: "jimpo",
    token: "USDC",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f978eac",
    },
    fid: 14,
    avatar: "https://avatars.githubusercontent.com/u/3130493?v=4",
    username: "rohith",
    displayName: "Rohith",
    token: "USDT",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f97cf4",
    },
    fid: 15,
    avatar: "https://i.imgur.com/JdRieqe.jpg",
    username: "pfh",
    displayName: "PFH",
    token: "BUSD",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f97cb96",
    },
    fid: 16,
    avatar: "https://github.com/zosegal.png",
    username: "zosegal",
    displayName: "Zach Segal",
    token: "WBTC",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f966240",
    },
    fid: 17,
    avatar:
      "https://i.seadn.io/gae/8gYoPP2mTxWhth4f4NZSQjaIBq0WTQWhwpJB3Cl8YvK3dUwoOLDxCSlUMQrkdM-mb3HNRmY_7xmIxARAEEjgxlXIrgj5nFp3ithB?w=500&auto=format",
    username: "sds",
    displayName: "Shane da Silva",
    token: "WETH",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9897fa",
    },
    fid: 18,
    avatar: "https://i.imgur.com/zeug0Sib.jpg",
    username: "todd",
    displayName: "Todd Goldberg",
    token: "WMATIC",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f975ac2",
    },
    fid: 19,
    avatar:
      "https://res.cloudinary.com/merkle-manufactory/image/fetch/c_fill,f_png,w_256/https://lh3.googleusercontent.com/LZbevEak-MEfi9y2cyBKUocHgyoqaVZTn4aGlqxY3qh0wDvaN6Dc1vnHFHCZQEIJ9C5wkncFtsCrTa6iQ0zJXyqaN-ayfym5aez4bVM",
    username: "julia",
    displayName: "Julia DeWahl",
    token: "WXDC",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f96b7fe",
    },
    fid: 20,
    avatar: "https://i.imgur.com/eq4Cax5.png",
    username: "omar",
    displayName: "Omar",
    token: "WBNB",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9b7b86",
    },
    fid: 21,
    avatar:
      "https://res.cloudinary.com/merkle-manufactory/image/fetch/c_fill,f_png,w_256/https://lh3.googleusercontent.com/0Hj8wu9_Lj-cxcBQKQHnSyc9nf1iTiND2FfFqvqq4boB0Vem86frSvgUjwQ_Tn8ESrPrFr9lSsYm2w3W2cQbRu4",
    username: "max",
    displayName: "Max Branzburg",
    token: "WLTC",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f995601",
    },
    fid: 22,
    avatar:
      "https://imagedelivery.net/BXluQx4ige9GuW0Ia56BHw/fa920810-fa1b-4f36-7daf-c53f837e2900/rectcrop3",
    username: "jesse",
    displayName: "Jesse Walden",
    token: "WINR",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f9570f5",
    },
    fid: 23,
    avatar: "https://i.imgur.com/ju8z8lD.jpg",
    username: "cdixon.eth",
    displayName: "Chris Dixon",
    token: "WBCH",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f967042",
    },
    fid: 24,
    avatar: "https://i.imgur.com/7inuvNa.jpg",
    username: "fehrsam",
    displayName: "Fred Ehrsam",
    token: "WXTZ",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9a7402",
    },
    fid: 25,
    avatar: "https://i.imgur.com/i6KtMfk.png",
    username: "jon",
    displayName: "itzler 🎩",
    token: "WXLM",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f946bf2",
    },
    fid: 44,
    avatar: "https://guywuollet.com/assets/images/headshot.jpg",
    username: "guy",
    displayName: "Guy",
    token: "DAI",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9a009a",
    },
    fid: 45,
    avatar: "https://i.imgur.com/hdmfyPP.jpg",
    username: "tiago",
    displayName: "tiago sada",
    token: "USDC",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9bc487",
    },
    fid: 46,
    avatar:
      "https://pbs.twimg.com/profile_images/1523689458054230016/5MYmpgh2_400x400.jpg",
    username: "darenmatsuoka",
    displayName: "Daren Matsuoka",
    token: "USDT",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f989073",
    },
    fid: 47,
    avatar:
      "https://logos-download.com/wp-content/uploads/2018/03/Buffalo_Bills_logo_blue.png",
    username: "jeff",
    displayName: "Jeff Amico",
    token: "BUSD",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9a2103",
    },
    fid: 48,
    avatar: "https://i.imgur.com/VW1eU3C.jpg",
    username: "eddy",
    displayName: "Eddy Lazzarin 🟠",
    token: "WBTC",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9d4065",
    },
    fid: 49,
    avatar:
      "https://lh3.googleusercontent.com/WqbiYqb1nPISv_jhlie3SY6tZ52HzMZ4-gQK1cr4DE4DvUXhitPC20jQ2B7xxBlHsMlPfMRSutNDpTU2cR1eoQPYcR0gxkrdrrNO_e0",
    username: "carra",
    displayName: "Carra Wu",
    token: "WETH",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f99d4b0",
    },
    fid: 50,
    avatar:
      "https://lh3.googleusercontent.com/D77ZMPBXnce1wO1yJVSNzQcxb5wB30hZ8XuNJdtyc3VBIvOxwby0u0zW7nqvuKImHuLPGZdm63Z22sJhsI_03lKic4zSDbeu-WqS",
    username: "mason",
    displayName: "Mason Hall",
    token: "WMATIC",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f96b0ae",
    },
    fid: 51,
    avatar: "https://i.imgur.com/Dsd2dz2.jpg",
    username: "pmarca",
    displayName: "Marc Andreessen",
    token: "WXDC",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9bab68",
    },
    fid: 52,
    avatar: "https://i.imgur.com/tkJCeQo.jpg",
    username: "sinahab",
    displayName: "Sina Habibian",
    token: "WBNB",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9b3de3",
    },
    fid: 53,
    avatar: "https://i.imgur.com/vUTHzMx.jpg",
    username: "antonio",
    displayName: "Antonio García Martínez",
    token: "WLTC",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9aa7e0",
    },
    fid: 54,
    avatar:
      "https://i.seadn.io/gae/JUbGP1Idb08BeW4f7PQ3hp5PVk8DRCqzlh5ygxHdoSCUWMSNplJxoZBUJkMlPXx7FacPo3V2GA0SwD9NmBzekGejaNpCr9HJ_cwUlZI?w=500&auto=format",
    username: "dcposch.eth",
    displayName: "✳️ dcposch on daimo",
    token: "WINR",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9c18b1",
    },
    fid: 55,
    avatar:
      "https://lh3.googleusercontent.com/b472whaKUZ8X130xlKG3Cnlf9Z41EcM-sp9pricmhlZE3g3x1E2rgl593lPKDitVDkYcVoeQVBs4251jjWauzRMlo2boXE--9eXxCQ",
    username: "packy",
    displayName: "packy 🎩",
    token: "WBCH",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f97f6fd",
    },
    fid: 56,
    avatar: "https://i.imgur.com/dehszLQ.gif",
    username: "literature",
    displayName: "nis.eth",
    token: "WXTZ",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f98f9a7",
    },
    fid: 57,
    avatar:
      "https://i.seadn.io/gae/hw9D90jed4cAAoQoKZZTJKUpKrYQBIjhwF-rLqlx0vKVmPVYtn7qfr1Uk9cnxkc7aFIfYRV-Mo1oErmAGsd_xwnhofjfWhdWYeQFWQ?w=500&auto=format",
    username: "henri",
    displayName: "Henri Stern Ꙫ",
    token: "WXLM",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f948448",
    },
    fid: 58,
    avatar: "https://i.imgur.com/rJcWHbv.gif",
    username: "brenner.eth",
    displayName: "Brenner",
    token: "WBAT",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f961ba1",
    },
    fid: 63,
    avatar:
      "https://lh3.googleusercontent.com/_NoELrqyEz0quvxwahN-lCuxbztYdcQE1RaAOHP531aGPGy25fd9NCPFc04R7LM_bRN2SPXYMU6rgEQlIZmghSYlM1PUBXMBQIh9pg",
    username: "lauren",
    displayName: "Lauren Farleigh",
    token: "DAI",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f97c9b",
    },
    fid: 64,
    avatar:
      "https://pbs.twimg.com/profile_images/1184946065692385281/_iRevtBm_400x400.jpg",
    username: "maksim",
    displayName: "Maksim Stepanenko",
    token: "USDC",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f9724cb",
    },
    fid: 65,
    avatar:
      "https://openseauserdata.com/files/f49a1245efef9eb53dc378f52934dd62.svg",
    username: "faraaz",
    displayName: "faraaz.eth",
    token: "USDT",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f95f454",
    },
    fid: 66,
    avatar: "https://i.imgur.com/MTXUxj5.jpg",
    username: "tayyab",
    displayName: "Tayyab - d/acc",
    token: "BUSD",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f95531d",
    },
    fid: 67,
    avatar:
      "https://lh3.googleusercontent.com/bxxY8QMWPnaHsm7VC3K8hVflIjQyBUeyhOBccaL_jUcN9aISppiuipFVb_qh2MbgmDtXBlD-SOOGvc3ZNmWgwEWMMkEdLDdf-y6XD60",
    username: "b",
    displayName: "Blake Robbins",
    token: "WBTC",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9b907b",
    },
    fid: 68,
    avatar: "https://i.imgur.com/adpD7Cv.jpg",
    username: "erik",
    displayName: "Erik Torenberg",
    token: "WETH",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f961590",
    },
    fid: 69,
    avatar:
      "https://imagedelivery.net/BXluQx4ige9GuW0Ia56BHw/56bb509e-f68b-4b5e-b8c6-c49c7d5a3d00/original",
    username: "gavi",
    displayName: "Gavi Galloway",
    token: "WMATIC",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f976c9f",
    },
    fid: 70,
    avatar: "https://i.ibb.co/D5K755z/Adam-Color-1.png",
    username: "a",
    displayName: "Adam Goldberg",
    token: "WXDC",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9adf08",
    },
    fid: 71,
    avatar: "https://i.imgur.com/p17N21E.jpg",
    username: "arjun",
    displayName: "arjun 🎩",
    token: "WBNB",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9add85",
    },
    fid: 72,
    avatar:
      "https://avatars.githubusercontent.com/u/19957?s=400&u=d95c4a92ff27aea2b16949b1a31b434c61b64453&v=4",
    username: "garrytan",
    displayName: "Garry Tan",
    token: "WLTC",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9333a",
    },
    fid: 73,
    avatar:
      "https://pbs.twimg.com/profile_images/1427678560584601604/ZUx9hjZm_400x400.jpg",
    username: "julian",
    displayName: "Julian Lehr",
    token: "WINR",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f98806e",
    },
    fid: 74,
    avatar:
      "https://i.seadn.io/gae/DlkV3xEOuHyQW0_6nadZu-dsnsB7n9O-dldHTvFBGekhwQfuB_2Ldb591zQ1sG2k5_DNmCUU5pSpvSNYNip2RnHW_qzIfSA7BSb9dA?w=500&auto=format",
    username: "osama",
    displayName: "osama",
    token: "WBCH",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f95c5f6",
    },
    fid: 75,
    avatar:
      "https://pbs.twimg.com/profile_images/1427678560584601604/ZUx9hjZm_400x400.jpg",
    username: "j",
    displayName: "j",
    token: "WXTZ",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f959f66",
    },
    fid: 77,
    avatar:
      "https://pbs.twimg.com/profile_images/1590775746493939712/FA6yBvEs_400x400.jpg",
    username: "jw",
    displayName: "Josh Williams",
    token: "DAI",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f98806e",
    },
    fid: 78,
    avatar: "https://i.imgur.com/8TlxFnm.png",
    username: "trevor",
    displayName: "Trevor McFedries",
    token: "USDC",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f98f295",
    },
    fid: 79,
    avatar:
      "https://openseauserdata.com/files/f1527c89a67f709819fe081d4a6624c3.svg",
    username: "scott",
    displayName: "Scott Sunarto",
    token: "USDT",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f95673d",
    },
    fid: 80,
    avatar: "https://i.imgur.com/3tmhZL0.jpg",
    username: "liam.eth",
    displayName: "Liam",
    token: "BUSD",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9cf316",
    },
    fid: 81,
    avatar: "https://i.imgur.com/DIBzKEi.jpg",
    username: "3lau",
    displayName: "3LAU",
    token: "WBTC",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9b6600",
    },
    fid: 82,
    avatar:
      "https://lh3.googleusercontent.com/ETyto6m9t4KodSIlW5PZixSQpllAaDxG9Yx_knSX6h4P2xf5z2uOX2JhTv0_3LdFEYYRPmKw3NZvn4JhcWdG2kiZpr7cICVXnxnv63I",
    username: "gordon",
    displayName: "gordon",
    token: "WETH",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9b930d",
    },
    fid: 83,
    avatar:
      "https://lh3.googleusercontent.com/7kZfysFMzyB_r1kJVXFXZ6S2skYNzPYlfFoVJTIt-7_dm5mJAy-xVsD1CijVUrAEBuXHDi8Ewu07qBRz8Le5NjShzvtCyHIr6tkc",
    username: "gaby",
    displayName: "Gaby Goldberg",
    token: "WMATIC",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9a542b",
    },
    fid: 84,
    avatar:
      "https://lh3.googleusercontent.com/lbF4LJ7DM9NV0V5M9hcMRii6yRF8PS0qBLxQZhPCGTIUf2Ee5gSdJG5z1-ZVPx-XEozm78sFnZX0CU3btGCAeZb1pY1_de6d6F-MrpU",
    username: "fmc",
    displayName: "fmc",
    token: "WXDC",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f95ffa3",
    },
    fid: 85,
    avatar:
      "https://lh3.googleusercontent.com/n8PvGSMNTakQkHaMpWnA1LRS5-fHBz7TqWutRfT-3ll5fie4UivygNkXUSrn3gq-Zk_l4qpzSeOU0BuPh7YK_dkaiLs1-8eHsQ4BPA",
    username: "hunter",
    displayName: "Hunter Horsley",
    token: "WBNB",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f94d8d7",
    },
    fid: 86,
    avatar:
      "https://res.cloudinary.com/merkle-manufactory/image/fetch/c_fill,f_png,w_256/https://lh3.googleusercontent.com/c96bW2ENLrYTjz8CSRiimDwWU3y386yUSQgFElNCrnlxUinXXqZUKoSvqz59GZphtP3thc4bCin6mg7ydbkuV8ADgwkyTNT99uWI",
    username: "steakbitcoin.eth",
    displayName: "Dustin Moring",
    token: "WLTC",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f959f93",
    },
    fid: 87,
    avatar:
      "https://res.cloudinary.com/merkle-manufactory/image/fetch/c_fill,f_png,w_256/https://openseauserdata.com/files/d5d652ed260e8c7891f731cad05805d4.svg",
    username: "mihai",
    displayName: "Mihai Anca",
    token: "WINR",
  },
  {
    _id: {
      $oid: "667be3c2608339ef9f958e6f",
    },
    fid: 88,
    avatar: "https://i.imgur.com/bpL5JXB.jpg",
    username: "worm.eth",
    displayName: "🪱",
    token: "WBCH",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9b16d6",
    },
    fid: 89,
    avatar: "https://github.com/vinzius.png",
    username: "vincent",
    displayName: "Vincent",
    token: "WXTZ",
  },
  {
    _id: {
      $oid: "667be3c3608339ef9f9962db",
    },
    fid: 90,
    avatar:
      "https://lh3.googleusercontent.com/Zqmo11skwQR9AmshxgVJK7muO27euyaUr9zeOpL4YmVSZgc5Xc-r7gGa-uycmrszR-kLpbokGgBkZS6YuGaSsVUEvEOxCM-RDVFA",
    username: "kyle",
    displayName: "kyle",
    token: "WXLM",
  },
];

export const TOKENDUMMY = [
  "DAI",
  "USDC",
  "USDT",
  "BUSD",
  "WBTC",
  "WETH",
  "WMATIC",
  "WXDC",
  "WBNB",
  "WLTC",
  "WINR",
  "WBCH",
  "WXTZ",
  "WXLM",
  "WBAT",
  "WLEO",
  "WCAKE",
  "WGRT",
  "WUNI",
  "WSCRT",
];

export const PRICETOKEN = {
  DAI: 1.04, // Source: https://www.coingecko.com/en/coins/dai
  USDC: 1.04, // Source: https://www.coingecko.com/en/coins/usd-coin
  USDT: 1.04, // Source: https://www.coingecko.com/en/coins/tether
  BUSD: 1.04, // Source: https://www.coingecko.com/en/coins/busd
  WBTC: 58.5, // Source: https://www.coingecko.com/en/coins/wrapped-bitcoin
  WETH: 4.17, // Source: https://www.coingecko.com/en/coins/ethereum
  WMATIC: 1.05, // Source: https://www.coingecko.com/en/coins/matic-network
  WXDC: 1.04, // Source: https://www.coingecko.com/en/coins/wrapped-xdc
  WBNB: 5.68, // Source: https://www.coingecko.com/en/coins/binance-coin
  WLTC: 77.75, // Source: https://www.coingecko.com/en/coins/wrapped-litecoin
  WINR: 0.58, // Source: https://www.coingecko.com/en/coins/win-r
  WBCH: 21.44, // Source: https://www.coingecko.com/en/coins/wrapped-chiliz
  WXTZ: 1.09, // Source: https://www.coingecko.com/en/coins/wrapped-tz
  WXLM: 1.09, // Source: https://www.coingecko.com/en/coins/wrapped-xlm
  WBAT: 1.09, // Source: https://www.coingecko.com/en/coins/wrapped-bitcoin-atlet
  WLEO: 1.09, // Source: https://www.coingecko.com/en/coins/wrapped-leo
  WCAKE: 9.84, // Source: https://www.coingecko.com/en/coins/wrapped-cake
  WGRT: 0.35, // Source: https://www.coingecko.com/en/coins/gala
  WUNI: 1.05, // Source: https://www.coingecko.com/en/coins/wrapped-uniswap
  WSCRT: 1.09, // Source: https://www.coingecko.com/en/coins/wrapped-solana
};

export const dataTokenWallet = {
  logo_token: "assets/images/logo-eth.png",
  token: "ETH",
};

export const imageTier = {
  S: <LogoS />,
  A: <LogoA />,
  B: <LogoB />,
  C: <LogoC />,
  D: <LogoD />,
};