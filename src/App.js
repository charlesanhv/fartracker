import logo from "./logo.svg";
import "./App.css";
import "./custom.css";
import { useFetch } from "hooks";
import { fetchTransactionsWhale } from "services";

function App() {
  // TODO: call api with arguments instead
  const [{ data, loading, error }] = useFetch(fetchTransactionsWhale, {
    args: { skip: 0, take: 10}
  });
  console.log("xxxxxxxxx", data, loading, error);
  // TODO: call api no arguments
  // const [{ data, loading, error }] = useFetch(fetchTransactions);
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
