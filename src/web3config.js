import { http, createConfig } from 'wagmi'
import { base } from 'wagmi/chains'
// import { injected, metaMask, safe, walletConnect } from 'wagmi/connectors'
import { WALLET_CONNECT_PROJECT_ID } from './constants'
import { getDefaultConfig } from "connectkit";


// export const config = createConfig({
//   chains: [ base],
//   connectors: [
//     injected(),
//     walletConnect({ projectId : WALLET_CONNECT_PROJECT_ID }),
//     metaMask(),
//     safe(),
//   ],
//   transports: {
//     [base.id]: http(),
//   },
// })
const connectkitDefaultConfig = getDefaultConfig({
    chains: [base],
    transports: {
      [base.id]: http(),
    },
    // Required API Keys
    walletConnectProjectId: WALLET_CONNECT_PROJECT_ID,

    // Required App Info
    appName: "Fartracker",

    // Optional App Info
    appDescription: "Your App Description",
    appUrl: "https://fartracker.com", // your app's url
    appIcon: "https://fartracker.com/logo.png", // your app's icon, no bigger than 1024x1024px (max. 1MB)
  });
export const config = createConfig(
    connectkitDefaultConfig
  );