/*!
 * Start Bootstrap - Modern Business v5.0.7 (https://startbootstrap.com/template-overviews/modern-business)
 * Copyright 2013-2024 Start Bootstrap
 * Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-modern-business/blob/master/LICENSE)
 */
// This file is intentionally blank
// Use this file to add JavaScript to your project

// click show swap

document.addEventListener("DOMContentLoaded", (event) => {
  const buttons = document.querySelectorAll(".btn-quick-action");
  const btnClose = document.querySelectorAll(".btn-close");
  const swapElement = document.getElementById("swap-element");
  const overlayElement = document.getElementById("overlay");
  const hideElement = document.querySelectorAll(".hide-swap");
  buttons.forEach((button) => {
    button.addEventListener("click", handleClick);
  });

  btnClose.forEach((button) => {
    button.addEventListener("click", function () {
      swapElement.classList.remove("show-swap");
      overlayElement.classList.remove("show-overlay");
      hideElement.forEach((element) => {
        element.style.display = "revert-layer";
      });
    });
  });

  function handleClick(event) {
    swapElement.classList.add("show-swap");
    overlayElement.classList.add("show-overlay");
    hideElement.forEach((element) => {
      element.style.display = "none";
    });
  }

  // input buy_sell_input

  const buySellInput = document.querySelector(".buy_sell_input");
  const TitleQuickActions = document.querySelectorAll(".title-quick-action");
  const valuePriceSwaps = document.querySelectorAll(".value-price-swap");
  buySellInput.addEventListener("input", function () {
    let value = this.value.replace(/\D/g, '');
    this.value = value;
    TitleQuickActions.forEach((TitleQuickAction) => {
      TitleQuickAction.textContent = `${value}$`;
    });
    valuePriceSwaps.forEach((valuePriceSwap) => {
      valuePriceSwap.textContent = `${value}`;
    });
  });

  // change swap item
  const btnChangeSwap = document.querySelector(".btn-change-swap");
  const contentSwap = document.querySelector(".content-swap");
  btnChangeSwap.addEventListener("click", function () {
    const currentDirection = getComputedStyle(contentSwap).flexDirection;
    if (currentDirection === "column") {
      contentSwap.style.flexDirection = "column-reverse";
    } else {
      contentSwap.style.flexDirection = "column";
    }
  });

  // action rendata theo token

  const parentSelector = document.querySelector('.track');

  const Trending = document.querySelectorAll(".trending_content_item");

  Trending.forEach((trend) => {
    trend.addEventListener("click", function () {
      // Lấy dataIndex của phần tử được click
      const index = trend.getAttribute('data-index');
      // add active class
      Trending.forEach((item) => {
        if (item.parentNode.classList.contains('fomo_item_container')) {
          item.parentNode.replaceWith(...item.parentNode.childNodes);
        }
        item.classList.remove('fomo_item_child_box');
      });

      const correspondingItems = parentSelector.querySelectorAll(`.trending_content_item[data-index="${index}"]`);
      correspondingItems.forEach((item) => {
        const container = document.createElement('div');
        container.classList.add('fomo_item_container');
        item.classList.add('fomo_item_child_box');
        item.parentNode.insertBefore(container, item);
        container.appendChild(item);
      });

      // get info token
      let imgSrc = 'assets/images/token_simpol.svg';
      const imgElement = trend.querySelector('img');
      if (imgElement) {
          imgSrc = imgElement.getAttribute('src');
      }
      const valueSpans = trend.querySelectorAll('.trending_action_text');
      let valueToken = valueSpans[valueSpans.length - 1].textContent || '';

      // Cập nhật thông tin token trong bảng
      const logoTokenTables = document.querySelectorAll('.logo-token');
      const nameTokenTables = document.querySelectorAll('.name-token');
      logoTokenTables.forEach((logoTokenTable) => {
          logoTokenTable.src = imgSrc;
      });
      nameTokenTables.forEach((nameTokenTable) => {
          nameTokenTable.textContent = valueToken;
      });
    });
  });


  // action copy address

  const btnCopyAddress = document.querySelectorAll('.btn-copy');

  btnCopyAddress.forEach((btnCopy) => {
    btnCopy.addEventListener('click', function () {
      const address = this.parentElement.querySelector('.address');
      console.log(address.textContent);

      const notiPoup = document.querySelector('.noti-popup');
      notiPoup.classList.add("show-noti");

      setTimeout(() => {
        notiPoup.classList.remove("show-noti");
      }, 2000);
    });
  });

  // action warning
  const notiPoup = document.querySelector('.noti-warning');

  document.getElementById("inputSearch").addEventListener("keypress", function (event) {
    if (event.key === "Enter") {
      var inputValue = this.value.trim();
      if (inputValue === "") {
        notiPoup.classList.add("show-noti-warning");
        document.querySelector('#text-warning').textContent = "Please enter number amount";
        setTimeout(() => {
          notiPoup.classList.remove("show-noti-warning");
        }, 2000);
      }
    }
  });
  // show and hide input search
  document.querySelector(".iconSearchConnectWallet").addEventListener("click", function () {
    document.querySelector(".showSearchHeader").style.display = "block";
  });

  document.getElementById("backButton").addEventListener("click", function () {

    document.querySelector(".showSearchHeader").style.display = "none";
  })
  // show and hide loading

  const loadingContainer = document.querySelector('.loading-container');

  setTimeout(() => {
    loadingContainer.style.display = 'none';
  }, 2000);

});

// scroll table
document.addEventListener("DOMContentLoaded", (event) => {
  const tableContainer = document.querySelector(".container-table");

  let isMouseDown = false;
  let startX, scrollLeft;

  tableContainer.addEventListener("mousedown", (e) => {
    isMouseDown = true;
    tableContainer.style.cursor = "grabbing";
    startX = e.pageX - tableContainer.offsetLeft;
    scrollLeft = tableContainer.scrollLeft;
  });

  tableContainer.addEventListener("mouseleave", () => {
    isMouseDown = false;
  });

  tableContainer.addEventListener("mouseup", () => {
    isMouseDown = false;
    tableContainer.style.cursor = "grab";
  });

  tableContainer.addEventListener("mousemove", (e) => {
    if (!isMouseDown) return;
    e.preventDefault();
    const x = e.pageX - tableContainer.offsetLeft;
    const walk = (x - startX) * 2; // Đặt hệ số cuộn
    tableContainer.scrollLeft = scrollLeft - walk;
  });
});

function openAction() {
  var element = document.querySelector(".action_box_mobile");
  if (element) {
    if (element.style.display === "none" || element.style.display === "") {
      element.style.display = "flex";
    } else {
      element.style.display = "none";
    }
  }
}

document.addEventListener("DOMContentLoaded", function () {
  var pageItems = document.querySelectorAll('.page-item');
  pageItems.forEach(function (item) {
    item.addEventListener('click', function () {
      pageItems.forEach(function (innerItem) {
        innerItem.classList.remove('page-item-select');
      });
      this.classList.add('page-item-select');

    });
  });
});
function handleBuy() {
  var elementBuy = document.getElementById("buy_action");
  var elementSell = document.getElementById("sell_action");

  if (elementBuy.style.color === "rgba(255, 255, 255, 0.3)") {
    elementBuy.style.color = "rgb(255, 255, 255)";
    elementSell.style.color = "rgba(255, 255, 255, 0.3)";
  }
}

function handleSell() {
  var elementBuy = document.getElementById("buy_action");
  var elementSell = document.getElementById("sell_action");

  if (elementSell.style.color === "rgba(255, 255, 255, 0.3)") {
    elementSell.style.color = "rgb(255, 255, 255)";
    elementBuy.style.color = "rgba(255, 255, 255, 0.3)";
  }
}
function handleOnchange() {
  console.log("change");
}

document.addEventListener("DOMContentLoaded", (event) => {
  const right_action_btn = document.querySelectorAll(".right_action_item");

  const modalOverlay = document.querySelector(".show-overlay_modal");
  var element = document.getElementById("modal_search");
  var inputElement = document.querySelector(".input_modal_search");
  right_action_btn.forEach((button) => {
    button.addEventListener("click", function () {
      if (element.style.display === "none" || element.style.display === "") {
        element.style.display = "block";
        modalOverlay.style.display = "block";
      }

      const spanElement = button.querySelector("span");

      if (spanElement.textContent === "Users") {
        inputElement.placeholder = "Search username";
      } else {
        inputElement.placeholder = `Search ${spanElement.textContent.toLowerCase()}`;
      }
    });
  });

  modalOverlay.addEventListener("click", function (event) {
    if (event.target === modalOverlay) {
      modalOverlay.style.display = "none";
      element.style.display = "none";
    }
  });
});

document.addEventListener("DOMContentLoaded", (event) => {
  const buttons = document.querySelectorAll(".left_action_item");

  buttons.forEach((button) => {
    button.addEventListener("click", function () {
      buttons.forEach((btn) => btn.classList.remove("action_item_active"));

      this.classList.add("action_item_active");
    });
  });
});

document.addEventListener("DOMContentLoaded", (event) => {
  const buttons = document.querySelectorAll(".action_box_mobile_item");

  buttons.forEach((button) => {
    button.addEventListener("click", function () {
      var element = document.getElementById("open_action");
      var spanElement = element.querySelector("span");
      var spanButton = button.querySelector("span");
      var action_box = document.querySelector(".action_box_mobile");
      if (spanButton.textContent === "Normal" || spanButton.textContent === "Vip" || spanButton.textContent === "Whale") {
        spanElement.textContent = spanButton.textContent;


        buttons.forEach((btn) => btn.classList.remove("hidden_btn"));

        this.classList.add("hidden_btn");
      }
      action_box.style.display = "none";
    });
  });
});